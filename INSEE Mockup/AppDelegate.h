//
//  AppDelegate.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/9/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

#define GOOGLE_MAP_API_KEY @"AIzaSyDvaIkvdEnXb6cpsQsEJfTQk9hs-5olCf4"

#define TABINDEX_PRODUCT        0
#define TABINDEX_CALCULATION    1
#define TABINDEX_SIMULATION     2
#define TABINDEX_NEWS           3
#define TABINDEX_MORE           4

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

+(BOOL)hasFourInchDisplay;

- (void)setupUiAppearance;

@end
