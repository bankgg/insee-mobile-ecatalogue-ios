//
//  AppDelegate.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/9/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "ManageData.h"
#import "AFHTTPRequestOperationManager.h"
#import "Global.h"
#import "News.h"
#import "NewsListViewController.h"
#import "LocalizeHelper.h"
#import "SIAlertView.h"
#import "TabBarViewController.h"
#import "NewsDetailViewController.h"

#import <Parse/Parse.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#warning FOR TEST PHASE ONLY
    //[Global SET_PRE_PRODUCTION_MODE];
    
    //Setup Appearance
    [self setupUiAppearance];
    
    //Google Map API
    [GMSServices provideAPIKey:GOOGLE_MAP_API_KEY];
    
    [self copyDataToDocument];
    
    //Language
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"th" forKey:@"user_language"];
    }
    LocalizationSetLanguage([[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"]);
    
    
    // Parse
    [Parse setApplicationId:@"dQBCgEOZVWQQW4sZqJqIYUSWQAHqRWarONSOjEqR"
                  clientKey:@"7d7YTzI5mDyHuDGo6ERsIUGiP3w030fLkSUnjg2T"];
    
    // Parse - Register for Push Notitications
    //iOS 8+
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    }
    else
    {
        //iOS 7
        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:notificationTypes];
    }
    
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [self syncData];
    [self checkNews];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Setup UI Appearance
- (void)setupUiAppearance
{
    //Background
    [self.window setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];
    
    //Navigation Bar
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navBackground7"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage imageNamed:@"navShadow7"]];
    
    [[UINavigationBar appearance] setTintColor:[UIColor lightGrayColor]];
    
    UIFont *barFont = [UIFont fontWithName:@"INSEE" size:18.0f];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName: [UIColor lightGrayColor],
                                                           NSFontAttributeName: barFont
                                                           } forState:UIControlStateNormal];
    
    //Navigation Font
    UIFont *navFont = [UIFont fontWithName:@"INSEE" size:24.0f];
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSForegroundColorAttributeName: [UIColor whiteColor],
                                                            NSFontAttributeName: navFont
                                                            }];
    
    //Tab Bar
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"tabBarBackground"]];
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabBarHighlight"]];
    
    //Tab Bar iPad iOS7 has different size
    if (IDIOM == IPAD)
    {
        [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"tabBarBackground_iPad7"]];
        [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabBarHighlight_iPad7"]];
        [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    }
    
    //Status Bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

+(BOOL)hasFourInchDisplay
{
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0);
}

#pragma mark - Copy Data to Document
-(void)copyDataToDocument
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"Data"];
    NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Data"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    bool isSameDatabaseVersion = YES;
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"db_version"] isEqualToString:DATABASE_VERSION]) {
        isSameDatabaseVersion = NO;
    }
    
    bool success;
    bool success2;
    if (![fileManager fileExistsAtPath:dataPath] || !isSameDatabaseVersion)
    {
        success2 = [fileManager removeItemAtPath:dataPath error:nil];
        success = [fileManager copyItemAtPath:bundlePath toPath:dataPath error:nil];
        
        if (success) {
            NSLog(@"copied");
            [[NSUserDefaults standardUserDefaults] setObject:DATABASE_VERSION forKey:@"db_version"];
        }
    }
    
    NSLog(@"Database Path : %@", dataPath);
}

#pragma mark - Sync Data
-(void)syncData
{
    id dg = nil;
    UITabBarController *tabController = (UITabBarController *)self.window.rootViewController;
    if ([tabController.selectedViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController *navCon = (UINavigationController *)tabController.selectedViewController;
        //NSLog(@"top = %@", navCon.topViewController);
        dg = navCon.topViewController;
    }
    
    
    
    ManageData *md = [[ManageData alloc] init];
    md.delegate = dg; //for calling back to reload table
    [md checkLastUpdate];
}

#pragma mark - Check News
-(void)checkNews
{
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:[Global PATH_OF:S_PATH_SERVICE_NEWS_LAST] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //NSLog(@"JSON: %@", responseObject);
         
         NSString *lastNewsValue = [responseObject objectForKey:@"last_value"];
         
         if (![[News getLastestClientNewsValue] isEqualToString:lastNewsValue])
         {
             //There are newer news
             UITabBarController *tabController = (UITabBarController *)self.window.rootViewController;
             [[tabController.tabBar.items objectAtIndex:3] setBadgeValue:@"N"];
             
             //Tell displaying view to reload if it's News tab
             if ([tabController.selectedViewController isKindOfClass:[UINavigationController class]])
             {
                 UINavigationController *navCon = (UINavigationController *)tabController.selectedViewController;
                 UIViewController *cVC = navCon.topViewController;
                 
                 if ([cVC isKindOfClass:[NewsListViewController class]])
                 {
                     if ([cVC respondsToSelector:@selector(checkNews)]) {
                         [cVC performSelector:@selector(checkNews)];
                     }
                 }
             }
             else
             {
                 [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"newer news"];
             }
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
     }];
}

#pragma mark - Parse Push Notification

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
//    currentInstallation.channels = @[ @"global" ];
#warning Change this when production
//    currentInstallation.channels = @[@"PREPROD"];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    if (userInfo)
    {
        //Get message while app is opening
        if([application applicationState] == UIApplicationStateInactive)
        {
            NSString *newsId = [userInfo objectForKey:@"news_id"];
            
            //Not show to shop's owner
            SIAlertView *alert = [[SIAlertView alloc] initWithTitle:@"New Message" andMessage:[userInfo objectForKey:@"message"]];
            [alert addButtonWithTitle:@"Cancel" type:SIAlertViewButtonTypeCancel handler:^(SIAlertView *alertView) {
                //add badge value +1
                
            }];
            [alert addButtonWithTitle:@"Show me" type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alertView) {
                [self showNewsDetailPage:newsId];
                //minus badge value -1
            }];
            [alert show];
        }
    }
}

- (void)showNewsDetailPage:(NSString *)news_id
{
    if ([self.window.rootViewController isKindOfClass:[TabBarViewController class]])
    {
        TabBarViewController *tabVC = (id)self.window.rootViewController;
        [tabVC setSelectedIndex:TABINDEX_NEWS];
        UINavigationController *newsNav = tabVC.viewControllers[TABINDEX_NEWS];
        [tabVC.delegate tabBarController:tabVC didSelectViewController:newsNav];
        
        NewsDetailViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"NewsDetailViewController"];
        vc.itemId = [news_id intValue];
        //vc.isFromMessage = YES;
        [newsNav pushViewController:vc animated:YES];
    }
}

@end
