//
//  MyUIActionSheet.m
//  BetterWeatherMini
//
//  Created by a^_^a BE on 11/01/11.
//  Copyright (c) 2012 iAppGarage. All rights reserved.
//

#import "CustomUIActionSheet.h"

@implementation CustomUIActionSheet

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

// For detecting taps outside of the alert view
-(void)tapOut:(UIGestureRecognizer *)gestureRecognizer 
{
    CGPoint p = [gestureRecognizer locationInView:self];
    if (p.y < 0) { // They tapped outside
        [self dismissWithClickedButtonIndex:0 animated:YES];
    }
}

-(void)setGestureRecognizer
{
    // Capture taps outside the bounds of this alert view
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOut:)];
    tap.cancelsTouchesInView = NO; // So that legit taps on the table bubble up to the tableview
    [self.superview addGestureRecognizer:tap];
}

-(void)showFromTabBar:(UITabBar *)view 
{
    [super showFromTabBar:view];
    [self setGestureRecognizer];
}

-(void)showFromRect:(CGRect)rect inView:(UIView *)view animated:(BOOL)animated
{
    [super showFromRect:rect inView:view animated:animated];
    [self setGestureRecognizer];
}

-(void)showInView:(UIView *)view
{
    [super showInView:view];
    [self setGestureRecognizer];
}


@end
