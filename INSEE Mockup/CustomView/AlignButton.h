//
//  AlignButton.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/14/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ButtonSideLeft,
    ButtonSideRight
} ButtonSide;

@interface AlignButton : UIButton

@property ButtonSide buttonSide;

@end
