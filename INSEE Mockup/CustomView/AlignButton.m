//
//  AlignButton.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/14/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "AlignButton.h"

@implementation AlignButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (UIEdgeInsets)alignmentRectInsets
{
    UIEdgeInsets insets = UIEdgeInsetsZero;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        if (self.buttonSide == ButtonSideLeft) {
            insets = UIEdgeInsetsMake(0, 9.0f, 0, 0);
        }
        else {
            insets = UIEdgeInsetsMake(0, 0, 0, 9.0f);
        }
    }
    
    return insets;
}

@end
