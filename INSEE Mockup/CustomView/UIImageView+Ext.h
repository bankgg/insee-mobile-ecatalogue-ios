//
//  UIImageView+Ext.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/4/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Ext)

- (void)setImageFromImagePathPhone:(NSString *)imagePathPhone andImagePathTablet:(NSString *)imagePathTablet;

- (void)setImageFromImagePathPhone:(NSString *)imagePathPhone andImagePathTablet:(NSString *)imagePathTablet scaleToImageViewWidth:(BOOL)isScale;

//- (void)setImageFromImagePathPhone:(NSString *)imagePathPhone andImagePathTablet:(NSString *)imagePathTablet scaleToImageViewWidth:(BOOL)isScale completed:(void (^)(BOOL finished))completion;

- (void)setAndScaleToFixWidthOfImage:(UIImage *)image;

@end
