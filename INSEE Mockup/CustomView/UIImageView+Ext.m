//
//  UIImageView+Ext.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/4/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "UIImageView+Ext.h"
#import "UIImageView+WebCache.h"
#import "UIImageExtension.h"

#import "Global.h"
#import "NSString+Ext.h"

@implementation UIImageView (Ext)

- (void)setImageFromImagePathPhone:(NSString *)imagePathPhone andImagePathTablet:(NSString *)imagePathTablet
{
    self.clipsToBounds = YES;
    
    NSString *path = IS_PAD?imagePathTablet:imagePathPhone;
    NSURL *imgURL = [Global imageURLFromPath:path];
    
    self.alpha = 0;
    [self sd_setImageWithURL:imgURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        if (!image)
        {
            [self setImage:[UIImage imageNamed:@"placeholder"]];
        }
        
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.alpha = 1.0;
        } completion:nil];
    }];
}

//- (void)setImageFromImagePathPhone:(NSString *)imagePathPhone andImagePathTablet:(NSString *)imagePathTablet scaleToImageViewWidth:(BOOL)isScale
//{
//    [self setImageFromImagePathPhone:imagePathPhone andImagePathTablet:imagePathTablet scaleToImageViewWidth:isScale completed:nil];
//}

- (void)setImageFromImagePathPhone:(NSString *)imagePathPhone andImagePathTablet:(NSString *)imagePathTablet scaleToImageViewWidth:(BOOL)isScale
{
    self.clipsToBounds = YES;
    
    NSString *path = IS_PAD?imagePathTablet:imagePathPhone;
    if ([NSString isNullOrEmpty:path])
    {
        [self setImage:[UIImage imageNamed:@"placeholder"]];
        return;
    }
    
    self.alpha = 0;
    NSURL *imgURL = [Global imageURLFromPath:path];
    
    [self sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        if (image)
        {
            if (isScale)
            {
                [self setAndScaleToFixWidthOfImage:image];
            }
            
//            completion(YES);
            
            [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.alpha = 1.0;
            } completion:nil];
        }
        else
        {
//            completion(NO);
        }
        
    }];
    
    [self setClipsToBounds:YES];
}

- (void)setAndScaleToFixWidthOfImage:(UIImage *)image
{
    CGSize newsize = [UIImageExtension sizeOfImage:image afterScaledToWidth:self.frame.size.width];
    CGRect newRect = self.frame;
    newRect.size.width = newsize.width;
    newRect.size.height = newsize.height;
    self.frame = newRect;
}

@end
