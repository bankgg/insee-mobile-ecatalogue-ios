//
//  Global.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/18/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPRequestOperationManager.h"

#warning Change this version if the local database structure is edited
#define DATABASE_VERSION                @"2.51"

#warning - Remove this when deploy to App Store
//#define DEBUGS

#if defined(DEBUGS)
    #define PATH_SERVER                 @"http://localhost/insee/insee/"
#else
    #define PATH_SERVER                 @"https://mobilecatalogue.siamcitycement.com/pprod/" //prod when deploy
#endif

#define PATH_SERVER_DEBUG               @"http://localhost/insee/insee/"
#define PATH_SERVER_PPROD               @"https://mobilecatalogue.siamcitycement.com/pprod/"
#define PATH_SERVER_PROD                @"https://mobilecatalogue.siamcitycement.com/prod/"


//KEYWORD -- FOR CHANGE MODE BETWEEN PRODUCTION AND PRE-PRODUCTION MODE
#define MODE_PREPROD_KEYWORD            @"INSEE PRE-PRODUCTION"
#define MODE_PROD_KEYWORD               @"INSEE PRODUCTION"


//REACHABILITY
#ifdef DEBUGS
    #define REACHABILITY_HOST           @"localhost"
#else
    #define REACHABILITY_HOST           @"mobilecatalogue.siamcitycement.com" //@"insee.unbox.co.th"
#endif


//IMAGE
#define S_PATH_IMAGE                    @"%@"

//NEWS
#define S_PATH_SERVICE_NEWS_LIST        @"appservice/newsList.php?offset=%d&row=%d"
#define S_PATH_SERVICE_NEWS_LIST2       @"appservice/newsList2.php?offset=%d&row=%d" //Phase 2
#define S_PATH_SERVICE_NEWS_DETAIL      @"appservice/newsDetail.php?id=%d"
#define S_PATH_SERVICE_NEWS_LAST        @"appservice/newsLast.php"

//PROJECT REF
#define S_PATH_SERVICE_PROJECT_LIST     @"appservice/projectList.php?offset=%d&row=%d"
#define S_PATH_SERVICE_PROJECT_DETAIL   @"appservice/projectDetail.php?id=%d"

//UPDATE DATA
#define S_PATH_SERVICE_LAST_UPDATE      @"appservice/last_update.php"
#define S_PATH_SERVICE_LAST_DATA        @"appservice/last_data.php?table_name=%@"

//LOCATION
#define S_PATH_SERVICE_LOCATION_LIST    @"appservice/locationList.php?lat=%f&lng=%f&radius=%f&cat_ids=%@"
#define S_PATH_SERVICE_LOCATION_LIST2   @"appservice/locationList2.php?lat=%f&lng=%f&radius=%f&cat_ids=%@" // Phase 2
#define S_PATH_SERVICE_LOCATION_DETAIL  @"appservice/locationDetail.php?id=%d"
#define S_PATH_SERVICE_LOCATION_SEARCH  @"appservice/locationSearch.php?lat=%f&lng=%f&offset=%d&row=%d&search=%@"
#define S_PATH_SERVICE_LOCATION_PRODUCT_CAT_LIST    @"appservice/locationProductCatList.php"

//REGISTER
#define S_PATH_SERVICE_TYPE_LIST        @"appservice/typeListRegister.php"
#define S_PATH_SERVICE_TYPE_LIST2        @"appservice/typeListRegister2.php" //Phase 2
#define S_PATH_SERVICE_REGISTER         @"appservice/register.php"

//LOGIN & USER
#define S_PATH_SERVICE_LOGIN            @"appservice/login.php"
#define S_PATH_SERVICE_USER             @"appservice/user.php?id=%d"

//FORGOT PASSWORD
#define S_PATH_SERVICE_FORGOT_PASSWORD_REQUEST @"appservice/forgot_request.php?email=%@"

//STAT
#define S_PATH_SERVICE_STAT_POST        @"appservice/stat.php"


//FACEBOOK
#define URL_FACEBOOK_FANPAGE            @"https://www.facebook.com/INSEEGroup"


//IPAD CHECKER
#define IDIOM       UI_USER_INTERFACE_IDIOM()
#define IPAD        UIUserInterfaceIdiomPad
#define IS_PAD      UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad


//INSEE CLUBCARD
#define URL_INSEE_CLUBCARD              @"appweb/clubcard.php" //@"http://www.siamcitycement.com/en/our_services/insee_club_card"

//POLICY
#define URL_INSEE_POLICY_PHONE          @"appweb/policy_phone_%@.html"
#define URL_INSEE_POLICY_TABLET         @"appweb/policy_tablet_%@.html"


@interface Global : NSObject

+(NSString *)PATH_OF:(NSString *)SUB_PATH;
+(void)SET_PRODUCTION_MODE;
+(void)SET_PRE_PRODUCTION_MODE;

+(UIStoryboard *)storyboard;
+(UIStoryboard *)storyboardIphone;
+(UIStoryboard *)storyboardIpad;

+(AFHTTPRequestOperationManager *)afManager;

+ (NSString *) appVersion;
+ (NSString *) build;





+(NSURL *)imageURLFromPath:(NSString *)path;




@end
