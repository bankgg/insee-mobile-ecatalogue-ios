//
//  Global.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/18/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "Global.h"

@implementation Global

+(NSString *)PATH_OF:(NSString *)SUB_PATH
{
    NSString *serverPath = PATH_SERVER_PROD;
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"is preproduction mode"])
    {
        serverPath = PATH_SERVER_PPROD;
//        serverPath = PATH_SERVER_DEBUG;
    }
    
    return [NSString stringWithFormat:@"%@%@", serverPath, SUB_PATH];
}

+(void)SET_PRODUCTION_MODE
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is preproduction mode"];
}

+(void)SET_PRE_PRODUCTION_MODE
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is preproduction mode"];
}

+(UIStoryboard *)storyboard
{
    if (IDIOM == IPAD)
    {
        return [self storyboardIpad];
    }
    return [self storyboardIphone];
}
+(UIStoryboard *)storyboardIphone
{
    return [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
}
+(UIStoryboard *)storyboardIpad
{
    return [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
}

+(AFHTTPRequestOperationManager *)afManager
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    //manager.securityPolicy.SSLPinningMode = AFSSLPinningModeCertificate;
    return manager;
}

+ (NSString *) appVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
}

+ (NSString *) build
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
}



+(NSURL *)imageURLFromPath:(NSString *)path
{
    NSString *imgPath = path;
    NSString *imgURLString = [NSString stringWithFormat:[Global PATH_OF:S_PATH_IMAGE], imgPath];
    //NSLog(@"image path %@", imgURLString);
    NSURL *imgURL = [NSURL URLWithString:imgURLString];
    
    return imgURL;
}

@end
