//
//  DocViewerViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/14/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "AFHTTPRequestOperationManager.h"
#import "AFURLSessionManager.h"
#import "MBProgressHUD.h"

@interface DocViewerViewController : UIViewController <UIDocumentInteractionControllerDelegate, UIWebViewDelegate, UIActionSheetDelegate>
{
    IBOutlet UIBarButtonItem *actButton;
    IBOutlet UIBarButtonItem *backButton;
    IBOutlet UIWebView *webView;
}

-(IBAction)actButtonPressed:(id)sender;
-(IBAction)backButtonPressed:(id)sender;

@property (nonatomic, retain) NSString *docURLPath;
@property (nonatomic, retain) UIDocumentInteractionController *docController;

@property (nonatomic, retain) NSString *docPath;

@property (nonatomic, retain) NSString *webURL;

@end
