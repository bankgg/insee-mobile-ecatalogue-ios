//
//  DocViewerViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/14/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "DocViewerViewController.h"
#import "ManageData.h"

@interface DocViewerViewController ()

@end

@implementation DocViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.docURLPath != nil)
    {
        [self downloadFileFromHTTP];
    }
    else if (self.docPath != nil)
    {
        [self webViewShowPath:self.docPath];
        
        NSURL *targetURL = [NSURL fileURLWithPath:self.docPath];
        self.docController = [UIDocumentInteractionController interactionControllerWithURL:targetURL];
    }
    else if (self.webURL != nil)
    {
        [self webViewOpenURL:self.webURL];
    }
}

- (void)webViewOpenURL:(NSString *)webURL
{
    NSURL *targetURL = [NSURL URLWithString:webURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    webView.delegate = self;
    [webView loadRequest:request];
}

- (void)webViewShowPath:(NSString *)docPath
{
    NSURL *targetURL = [NSURL fileURLWithPath:docPath];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    webView.delegate = self;
    [webView loadRequest:request];
}

- (void)downloadFileFromHTTP
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.dimBackground = YES;
    HUD.mode = MBProgressHUDModeDeterminate;
    HUD.labelText = @"Loading";
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.docURLPath]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    NSString *pdfName = @"pdf_file.pdf";
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:pdfName];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Successfully downloaded file to %@", path);
        self.docController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath:path]];
        [HUD hide:YES];
        [self webViewShowPath:path];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [HUD hide:YES];
        
    }];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        
        NSLog(@"Download = %f", (float)totalBytesRead / totalBytesExpectedToRead);
        HUD.progress = (float)totalBytesRead / totalBytesExpectedToRead;
        
    }];
    [operation start];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
    {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)actButtonPressed:(id)sender
{
    if (self.webURL != nil)
    {
        UIActionSheet *aSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Open in Safari" otherButtonTitles:nil];
        [aSheet showFromBarButtonItem:actButton animated:YES];
    }
    else
    {
        self.docController.delegate = self;
        [self.docController presentOptionsMenuFromBarButtonItem:actButton animated:YES];
    }
}

-(IBAction)backButtonPressed:(id)sender
{
    [self returnToParent];
}

- (void)returnToParent
{
    if ([self respondsToSelector:@selector(presentingViewController)]){
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - WebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

#pragma mark - ActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSURL *url = [NSURL URLWithString:self.webURL];
    
    if([[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Cannot open website" message:@"The website URL is not valid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

@end
