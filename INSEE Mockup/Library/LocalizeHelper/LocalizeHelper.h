//
//  LocalizeHelper.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/15/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//
//  http://stackoverflow.com/questions/9939885/manual-language-selection-in-an-ios-app-iphone-and-ipad

#import <Foundation/Foundation.h>

// some macros (optional, but makes life easy)

// Use "LocalizedString(key)" the same way you would use "NSLocalizedString(key,comment)"
#define LocalizedString(key) [[LocalizeHelper sharedLocalSystem] localizedStringForKey:(key)]

// "language" can be (for american english): "en", "en-US", "english". Analogous for other languages.
#define LocalizationSetLanguage(language) [[LocalizeHelper sharedLocalSystem] setLanguage:(language)]

@interface LocalizeHelper : NSObject

// a singleton:
+ (LocalizeHelper*) sharedLocalSystem;

// this gets the string localized:
- (NSString*) localizedStringForKey:(NSString*) key;

//set a new language:
- (void) setLanguage:(NSString*) lang;

@end