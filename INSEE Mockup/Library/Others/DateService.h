//
//  DateService.h
//  BrandBuddy
//
//  Created by The Jia on 9/18/12.
//  Copyright (c) 2012 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateService : NSObject

+(NSDate *)dateFromJsonServiceString:(NSString *)dateString withTime:(BOOL)isWithTime;

+(NSString *)stringDateFromDate:(NSDate *)date withTime:(BOOL)isWithTime;

+(NSString *)stringDateMySqlFromDate:(NSDate *)date withTime:(BOOL)isWithTime;

//Support language
+(NSString *)INSEE_stringDateLangFromServiceString:(NSString *)dateString withTime:(BOOL)isWithTime;

@end
