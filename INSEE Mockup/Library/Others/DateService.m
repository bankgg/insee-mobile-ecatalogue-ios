//
//  DateService.m
//  BrandBuddy
//
//  Created by The Jia on 9/18/12.
//  Copyright (c) 2012 iAppGarage. All rights reserved.
//

#import "DateService.h"

@implementation DateService

+(NSDate *)dateFromJsonServiceString:(NSString *)dateString withTime:(BOOL)isWithTime
{
    NSLog(@"dateString = %@", dateString);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"]; //2012-09-08
    if (isWithTime) {
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"]; //2012-09-08 15:38:00
    }
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [formatter setCalendar:gregorianCalendar];
    
    NSDate *date = [formatter dateFromString:dateString];
    
    NSLog(@"date = %@", date);
    
    return date;
}

+(NSString *)stringDateFromDate:(NSDate *)date withTime:(BOOL)isWithTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"]; //2012-09-08 15:38:00
    
    NSString *formatString = @"d MMMM yyyy";
    if (isWithTime) {
        formatString = [formatString stringByAppendingString:@" - HH:mm:ss"];
    }
    [formatter setDateFormat:formatString];
    
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

+(NSString *)stringDateMySqlFromDate:(NSDate *)date withTime:(BOOL)isWithTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSString *formatString = @"yyyy-MM-d";
    
    if (isWithTime) {
        formatString = [formatString stringByAppendingString:@" HH:mm:ss"];
    }
    [formatter setDateFormat:formatString];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}


+(NSString *)INSEE_stringDateLangFromServiceString:(NSString *)dateString withTime:(BOOL)isWithTime
{
    NSDate *date = [self dateFromJsonServiceString:dateString withTime:isWithTime];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:[[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"]];
    [dateFormat setLocale:locale];
    [dateFormat setDateStyle:NSDateFormatterMediumStyle];
    
    return [dateFormat stringFromDate:date];
}

@end
