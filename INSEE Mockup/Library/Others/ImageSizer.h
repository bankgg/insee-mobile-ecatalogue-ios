//
//  ImageSizer.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/8/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageSizer : NSObject

+(UIImage *)imageResizedWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+(CGSize)imageSizeOfImage:(UIImage *)image withShortSide:(float)shortSide;

@end
