//
//  ImageSizer.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/8/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ImageSizer.h"

@implementation ImageSizer

+(UIImage *)imageResizedWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(CGSize)imageSizeOfImage:(UIImage *)image withShortSide:(float)shortSide
{
    float imgWidth = image.size.width;
    float imgHeight = image.size.height;
    
    float newWidth = shortSide;
    float newHeight = shortSide;
    
    if (imgWidth < imgHeight)
    {
        newWidth = shortSide;
        newHeight = ceil(imgHeight * newWidth / imgWidth);
    }
    else if (imgHeight < imgWidth)
    {
        newHeight = shortSide;
        newWidth = ceil(imgWidth * newHeight / imgHeight);
    }
    
    return CGSizeMake(newWidth, newHeight);
}

@end
