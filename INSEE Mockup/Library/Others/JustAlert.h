//
//  JustAlert.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/9/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JustAlert : UIAlertView

+ (void)alertWithTitle:(NSString *)title;
+ (void)alertWithMessage:(NSString *)message;
+ (void)alertWithTitle:(NSString *)title message:(NSString *)message;

@end
