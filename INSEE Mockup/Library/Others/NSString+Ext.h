//
//  NSString+Ext.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/22/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Ext)

-(NSString *) stringByReplace:(NSArray *)array
                   withString:(NSString *)withString;
-(NSString *)htmlEncode;

-(BOOL)contains:(NSString *)text;

+(BOOL)isNullOrEmpty:(NSString *)text;
+(BOOL)isNotNullOrEmpty:(NSString *)text;

-(BOOL)isInteger;
+(BOOL)isNumeric:(NSString *)string;
+(BOOL)isNumericWithDot:(NSString *)string;
+(BOOL)isMoneyFormat:(NSString *)string;

+(BOOL)isWhiteSpace:(NSString *)string;
+(NSString *)stringValueFromObject:(id)object;

@end
