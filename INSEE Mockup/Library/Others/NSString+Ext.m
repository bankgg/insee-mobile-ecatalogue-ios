//
//  NSString+Ext.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/22/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "NSString+Ext.h"

@implementation NSString (Ext)

-(NSString *) stringByReplace:(NSArray *)array
                   withString:(NSString *)withString{
    
    NSString *result = [NSString stringWithString:self];
    
    for(NSString *each in array){
        result = [result stringByReplacingOccurrencesOfString:each withString:withString];
    }
    
    return result;
}

-(NSString *)htmlEncode
{
    if(self == nil || self.length == 0)
        return @"";
    
    NSString *encoded = [self stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    
    encoded = [encoded stringByReplacingOccurrencesOfString:@"\"" withString:@"&quot;"];
    encoded = [encoded stringByReplacingOccurrencesOfString:@"'" withString:@"&apos;"];
    
    encoded = [encoded stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
    encoded = [encoded stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];
    
    return encoded;
}

-(BOOL)contains:(NSString *)text
{
    NSRange textRange =[[self lowercaseString] rangeOfString:[text lowercaseString]];
    
    return textRange.location != NSNotFound;
}

-(BOOL)isInteger
{
    NSScanner *sc = [NSScanner scannerWithString:self];
    if ( [sc scanInteger:NULL] )
    {
        return [sc isAtEnd];
    }
    return NO;
}

+(BOOL)isNullOrEmpty:(NSString *)text
{
    return
    
    text == nil
    || [text isEqual:[NSNull null]]
    || [[NSString stringWithFormat:@"%@", text] length] == 0
    || [[NSString stringWithFormat:@"%@", text] isEqualToString:@"(null)"]
    
    ;
}

+(BOOL)isNotNullOrEmpty:(NSString *)text
{
    return ![NSString isNullOrEmpty:text];
}

+(BOOL)isNumeric:(NSString *)string
{
    return [[string stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]] isEqualToString:@""];
}

+(BOOL)isNumericWithDot:(NSString *)string
{
    return [[string stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"1234567890."]] isEqualToString:@""];
}

+(BOOL)isMoneyFormat:(NSString *)string
{
    NSNumberFormatter *numberFormat = [NSNumberFormatter new];
    [numberFormat setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *str = [[numberFormat numberFromString:[NSString stringWithFormat:@"$%@",string]] stringValue];
    return ![NSString isNullOrEmpty:str] && ![string contains:@" "];
}

+(BOOL)isWhiteSpace:(NSString *)string
{
    return [[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""];
}

+(NSString *)stringValueFromObject:(id)object
{
    if (object == nil)
    {
        return @"";
    }
    else
    {
        return object;
    }
}

@end
