//
//  UIImageExtension.h
//  Thonglor
//
//  Created by Tanut Chantarajiraporn on 9/15/2557 BE.
//  Copyright (c) 2557 Unbox. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImageExtension : NSObject

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

+ (CGSize)sizeOfImage:(UIImage *)image afterScaledToLongSide:(float)longSide;

+ (CGSize)sizeOfImage:(UIImage *)image afterScaledToWidth:(float)width;

+ (UIImage *)imageWithColor:(UIColor *)color;

+ (CGSize)sizeOfImage:(UIImage *)image afterScaledFitToFrameSize:(CGSize)size;

@end
