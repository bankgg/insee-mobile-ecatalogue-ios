//
//  UIImageExtension.m
//  Thonglor
//
//  Created by Tanut Chantarajiraporn on 9/15/2557 BE.
//  Copyright (c) 2557 Unbox. All rights reserved.
//

#import "UIImageExtension.h"

@implementation UIImageExtension

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+ (CGSize)sizeOfImage:(UIImage *)image afterScaledToLongSide:(float)longSide
{
    float imageLongSide = MAX(image.size.width, image.size.height);
    CGSize newSize;
    if (imageLongSide > longSide)
    {
        if (image.size.width > image.size.height)
        {
            newSize.width = longSide;
            newSize.height = newSize.width * image.size.height / image.size.width;
        }
        else
        {
            newSize.height = longSide;
            newSize.width = newSize.height * image.size.width / image.size.height;
        }
    }
    else
    {
        newSize = image.size;
    }
    return newSize;
}

+ (CGSize)sizeOfImage:(UIImage *)image afterScaledToWidth:(float)width
{
    CGSize newSize;
    
    newSize.width = width;
    newSize.height = image.size.height * newSize.width / image.size.width;
    
    return newSize;
}

+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (CGSize)sizeOfImage:(UIImage *)image afterScaledFitToFrameSize:(CGSize)size
{
    float ratio_image = image.size.width / image.size.height;
    float ratio_frame = size.width / size.height;
    
    CGSize r_size = CGSizeZero;
    if (ratio_image > ratio_frame) {
        
        //image is wider --> use width of the frame
        float n_width = size.width;
        float n_height = n_width / ratio_image;
        r_size = CGSizeMake(n_width, n_height);
        
    }
    else {
        
        //image is higher --> use width of the frame
        float n_height = size.height;
        float n_width = n_height * ratio_image;
        r_size = CGSizeMake(n_width, n_height);
        
    }
    
    return r_size;
}

@end
