//
//  UILabel+Boldify.h
//  Thonglor
//
//  Created by Tanut Chantarajiraporn on 9/23/2557 BE.
//  Copyright (c) 2557 Unbox. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Boldify)

- (void) boldSubstring: (NSString*) substring;
- (void) boldRange: (NSRange) range;

- (void) setSizeToFitNew;

@end
