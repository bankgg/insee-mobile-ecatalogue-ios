//
//  UILabel+Boldify.m
//  Thonglor
//
//  Created by Tanut Chantarajiraporn on 9/23/2557 BE.
//  Copyright (c) 2557 Unbox. All rights reserved.
//

#import "UILabel+Boldify.h"

@implementation UILabel (Boldify)

- (void) boldRange: (NSRange) range {
    if (![self respondsToSelector:@selector(setAttributedText:)]) {
        return;
    }
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    [attributedText setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:self.font.pointSize]} range:range];
    
    self.attributedText = attributedText;
}

- (void) boldSubstring: (NSString*) substring {
    NSRange range = [self.text rangeOfString:substring];
    [self boldRange:range];
}

- (void) setSizeToFitNew
{
    CGSize size = [self sizeThatFits:CGSizeMake(self.frame.size.width, MAXFLOAT)];
    
    CGRect labelFrame = self.frame;
    labelFrame.size.height = size.height;
    self.frame = labelFrame;
}

@end
