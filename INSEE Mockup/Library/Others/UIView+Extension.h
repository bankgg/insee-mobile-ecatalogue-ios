//
//  UIView+Extension.h
//  Thonglor
//
//  Created by Tanut Chantarajiraporn on 9/14/2557 BE.
//  Copyright (c) 2557 Unbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIView (Extension)

-(void)makeItRound;
-(void)makeItShadow;
-(void)makeItShadowRect;
-(void)makeItShadowRect2;

-(void)makeBorder;

-(void)setFramePosY:(float)posY;
//-(void)setFrameHeight:(float)height;
-(float)posOfEndY;
-(void)setFrameHeightFixWidthWithRatio:(float)ratio;
-(void)setFrameCenterY:(float)centerY;

-(void)sizeToFit2;
-(void)sizeToFit2withNumLines:(int)numOfLines;

-(void)makeRoundCorner:(float)radius;

@property (nonatomic, retain) NSString *stringTag;

- (UIView *)findSuperViewWithClass:(Class)superViewClass;

+(UIView *)blankView;

+(void)makeView:(UIView *)view1 andView:(UIView *)view2 toVerticalCenterOfHeight:(float)height;

@end
