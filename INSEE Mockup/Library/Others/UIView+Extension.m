//
//  UIView+Extension.m
//  Thonglor
//
//  Created by Tanut Chantarajiraporn on 9/14/2557 BE.
//  Copyright (c) 2557 Unbox. All rights reserved.
//

#import "UIView+Extension.h"
#import "objc/runtime.h"

@implementation UIView (Extension)

-(void)makeItRound
{
    self.layer.cornerRadius = self.frame.size.height / 2;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 0;
}

-(void)makeRoundCorner:(float)radius
{
    self.layer.cornerRadius = radius;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 0;
}

-(void)makeItShadow
{
    self.layer.shadowRadius = 3.0;
    self.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.layer.shadowOpacity = 0.3;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
//    self.layer.shouldRasterize = YES;
}

-(void)makeItShadowRect
{
    self.layer.shadowRadius = 2.0;
    self.layer.shadowOffset = CGSizeMake(0.0, 1.0);
    self.layer.shadowOpacity = 0.3;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
//    self.layer.shouldRasterize = YES;
}

-(void)makeItShadowRect2
{
    UIView *shadowView = [[UIView alloc] initWithFrame:self.frame];
    shadowView.layer.cornerRadius = self.layer.cornerRadius;
    shadowView.layer.borderWidth = self.layer.borderWidth;
    shadowView.autoresizingMask = self.autoresizingMask;
    [shadowView makeItShadowRect];
    
    [self.superview insertSubview:shadowView belowSubview:self];
}

-(void)setFramePosY:(float)posY
{
    CGRect frame = self.frame;
    frame.origin.y = posY;
    self.frame = frame;
}

-(void)setFrameHeight:(float)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

-(float)posOfEndY
{
    return self.frame.size.height + self.frame.origin.y;
}

-(void)setFrameHeightFixWidthWithRatio:(float)ratio
{
    CGRect frame = self.frame;
    frame.size.height = frame.size.width / ratio;
    self.frame = frame;
}

-(void)setFrameCenterY:(float)centerY
{
    CGPoint centerPoint = self.center;
    centerPoint.y = centerY;
    self.center = centerPoint;
}


NSString const *key = @"stringTag";

- (NSString *)stringTag
{
    return objc_getAssociatedObject(self, &key);
}

- (void)setStringTag:(NSString *)stringTag
{
    objc_setAssociatedObject(self, &key, stringTag, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


-(void)sizeToFit2
{
    [self sizeToFit2withNumLines:0];
}

-(void)sizeToFit2withNumLines:(int)numOfLines
{
    if (self.class == [UILabel class])
    {
        UILabel *label = (id)self;
        label.numberOfLines = numOfLines;
        
        CGSize size = [self sizeThatFits:CGSizeMake(self.frame.size.width, MAXFLOAT)];
        CGRect labelFrame = self.frame;
        labelFrame.size.height = size.height;
        self.frame = labelFrame;
    }
}


-(void)makeBorder
{
    UIColor *borderColor = [UIColor colorWithWhite:0.75 alpha:1.0];
    [self.layer setBorderColor:[borderColor CGColor]];
    [self.layer setBorderWidth:1.0f];
}


- (UIView *)findSuperViewWithClass:(Class)superViewClass
{
    UIView *superView = self.superview;
    UIView *foundSuperView = nil;
    
    while (nil != superView && nil == foundSuperView) {
        if ([superView isKindOfClass:superViewClass]) {
            foundSuperView = superView;
        } else {
            superView = superView.superview;
        }
    }
    return foundSuperView;
}


+(UIView *)blankView
{
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor clearColor];
    return v;
}

+(void)makeView:(UIView *)view1 andView:(UIView *)view2 toVerticalCenterOfHeight:(float)height
{
    float space = view2.frame.origin.y - view1.posOfEndY;
    
    float y1 = (height - (view1.frame.size.height + space + view2.frame.size.height))/2;
    float y2 = y1 + (view1.frame.size.height + space);
    
    [view1 setFramePosY:y1];
    [view2 setFramePosY:y2];
}


@end
