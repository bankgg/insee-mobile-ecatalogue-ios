//
//  UIView+StringTag.h
//  JCCRM
//
//  Created by Tanut Chantarajiraporn on 5/7/2557 BE.
//  Copyright (c) 2557 Nuttapol Natepinyo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (StringTag)

@property (nonatomic, retain) NSString *stringTag;

@end
