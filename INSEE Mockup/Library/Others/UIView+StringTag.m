//
//  UIView+StringTag.m
//  JCCRM
//
//  Created by Tanut Chantarajiraporn on 5/7/2557 BE.
//  Copyright (c) 2557 Nuttapol Natepinyo. All rights reserved.
//

#import "UIView+StringTag.h"
#import "objc/runtime.h"

@implementation UIView (StringTag)

@dynamic stringTag;



- (NSString *)stringTag
{
    NSString *const key = @"com.unbox.stringTag";
    return objc_getAssociatedObject(self, &key);
}

- (void)setStringTag:(NSString *)stringTag
{
    NSString *const key = @"com.unbox.stringTag";
    objc_setAssociatedObject(self, &key, stringTag, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
