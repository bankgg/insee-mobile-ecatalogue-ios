//
//  ImagesViewerViewController.h
//  PaPaiDon
//
//  Created by The Jia on 11/23/55 BE.
//  Copyright (c) 2555 Jia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface ImagesViewerViewController : UIViewController <UIScrollViewDelegate>
{
    IBOutlet UIScrollView *scrollV;
    
    int currentPage;
    
    NSMutableArray *arraySubScrollViews;
    
    UIPageControl *pageControl;
    
    IBOutlet UIButton *quitButton;
}


@property (nonatomic, retain) NSArray *arrayImages; // array of UIImage
@property int selectedIndex;

@property (nonatomic, assign) id delegate;

@property float maxZoomScale;

//Quit Button - for Guide Mode
@property BOOL isDisplayQuitButton;
-(IBAction)quitPressed:(id)sender;

@end
