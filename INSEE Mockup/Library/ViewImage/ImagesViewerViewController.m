//
//  ImagesViewerViewController.m
//  PaPaiDon
//
//  Created by The Jia on 11/23/55 BE.
//  Copyright (c) 2555 Jia. All rights reserved.
//

#import "ImagesViewerViewController.h"

@interface ImagesViewerViewController ()

@end

@implementation ImagesViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.maxZoomScale = 2.0f;
        self.isDisplayQuitButton = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.isDisplayQuitButton) {
        quitButton.hidden = YES;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupScrollView];
    [self setupPaging];
}

#pragma mark - ScrollView
-(void)setupScrollView
{
	CGFloat cx = 0;
    scrollV.tag = 100;
    currentPage = self.selectedIndex;
    [scrollV setShowsHorizontalScrollIndicator:NO];
    
    arraySubScrollViews = [[NSMutableArray alloc] init];
    
    
    for (id obj in self.arrayImages)
    {
        UIScrollView *subScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(cx, scrollV.frame.origin.y, scrollV.bounds.size.width, scrollV.bounds.size.height)];
        [subScrollView setBackgroundColor:[UIColor clearColor]];
        
        UIImageView *imageView = [[UIImageView alloc] init];
        [imageView setFrame:CGRectMake(0, 0, subScrollView.frame.size.width, subScrollView.frame.size.height)];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        [imageView setClipsToBounds:YES];
        
        if ([obj isKindOfClass:[UIImage class]])
        {
            [imageView setImage:obj];
        }
        else if ([obj isKindOfClass:[NSURL class]])
        {
            [imageView sd_setImageWithURL:obj placeholderImage:nil];
        }
        
        [subScrollView addSubview:imageView];
        [subScrollView setContentSize:CGSizeMake(imageView.frame.size.width, imageView.frame.size.height)];
        [subScrollView setClipsToBounds:YES];
        
        //UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
        //[subScrollView addGestureRecognizer:pinchGesture];
        subScrollView.tag = 200;
        subScrollView.maximumZoomScale = self.maxZoomScale;
        [subScrollView setBounces:NO];
        [subScrollView setDelegate:self];
        
        UITapGestureRecognizer *doubleTapsGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
        doubleTapsGesture.numberOfTapsRequired = 2;
        [subScrollView addGestureRecognizer:doubleTapsGesture];
        
        if (!self.isDisplayQuitButton)
        {
            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [subScrollView addGestureRecognizer:tapGesture];
            
            [tapGesture requireGestureRecognizerToFail:doubleTapsGesture];
        }
        
        [scrollV addSubview:subScrollView];
        [arraySubScrollViews addObject:subScrollView];
        
        cx += scrollV.frame.size.width;
    }
	
	[scrollV setContentSize:CGSizeMake(cx, scrollV.bounds.size.height)];
    [scrollV setContentOffset:CGPointMake(self.selectedIndex * scrollV.frame.size.width, 0)];
}

#pragma mark - UIScrollViewDelegate stuff

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView
{
    if (_scrollView == scrollV)
    {
        int newPage = _scrollView.contentOffset.x / _scrollView.frame.size.width;
        if (newPage != currentPage)
        {
            UIScrollView *subSV = (UIScrollView *)[arraySubScrollViews objectAtIndex:currentPage];
            [subSV setZoomScale:1.0 animated:YES];
            
            currentPage = newPage;
            pageControl.currentPage = currentPage;
        }
    }
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    if (scrollView.tag == 200)
    {
        for (UIView *imgView in scrollView.subviews)
        {
            if ([imgView isKindOfClass:[UIImageView class]]) {
                return imgView;
            }
        }
    }
    
    return nil;
}

#pragma mark - Button
-(void)buttonPressed
{
    //[self.presentingViewController dismissModalViewControllerAnimated:YES];
}

#pragma mark - Tap Stuffs

- (void)handleTap:(UITapGestureRecognizer *)sender
{
    NSLog(@"tap !!");
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)sender
{
    NSLog(@"DOUBLE tap !!");
    
    UIScrollView *subSV = (UIScrollView *)[arraySubScrollViews objectAtIndex:currentPage];
    if (subSV.zoomScale == 1.0) {
        [subSV setZoomScale:3.0 animated:YES];
    }
    else {
        [subSV setZoomScale:1.0 animated:YES];
    }
}

#pragma mark - Paging
-(void)setupPaging
{
    if (pageControl == nil)
    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
    pageControl.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height - 20);
    pageControl.numberOfPages = [self.arrayImages count];
    pageControl.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    pageControl.hidesForSinglePage = YES;
    
    pageControl.currentPage = self.selectedIndex;

    [self.view addSubview:pageControl];
}

#pragma mark - Quit Button
-(IBAction)quitPressed:(id)sender
{
    [self buttonPressed];
}

@end
