//
//  FormValidation.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 12/3/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ValidateObj.h"



@interface FormValidation : NSObject <UITextFieldDelegate>
{
    NSMutableArray *validateObjs;
}

@property (nonatomic, retain) UIColor *colorWarning;

- (void)addValidateTextfield:(UITextField *)textfield withLabel:(UILabel *)label withValidateType:(ValidateType)validateType withArgument:(id)arg;

- (bool)formPassValidate;

- (void)textFieldUpdate:(UITextField *)textField;

@end