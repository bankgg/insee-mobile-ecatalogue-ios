//
//  FormValidation.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 12/3/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "FormValidation.h"

@implementation FormValidation

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        validateObjs = [NSMutableArray array];
        
        self.colorWarning = [UIColor redColor];
    }
    return self;
}

- (void)addValidateTextfield:(UITextField *)textfield withLabel:(UILabel *)label withValidateType:(ValidateType)validateType withArgument:(id)arg
{
    ValidateObj *obj = [[ValidateObj alloc] init];
    obj.textfield = textfield;
    obj.label = label;
    obj.validateType = validateType;
    obj.arg = arg;
    
    obj.label.text = [self textLabelRequireFromText:obj.label.text];
    
    obj.colorDefault = obj.label.textColor;
    obj.colorWarning = self.colorWarning;
    
    textfield.delegate = self;
    
    [validateObjs addObject:obj];
    
    [textfield addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}

-(void)textFieldDidChange:(id)sender
{
    NSLog(@"tf changed");
}

-(NSString *)textLabelRequireFromText:(NSString *)text
{
    NSString *mark = @"*";
    if ([text rangeOfString:mark].location == NSNotFound) {
        text = [text stringByAppendingString:mark];
    }
    
    return text;
}

#pragma mark - TextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    for (ValidateObj *vobj in validateObjs)
    {
        if (vobj.textfield == textField)
        {
            if (vobj.validateType == ValidatePassword)
            {
                for (ValidateObj *aobj in validateObjs)
                {
                    if (aobj.validateType == ValidatePasswordConfirm)
                    {
                        aobj.textfield.text = @"";
                        aobj.label.textColor = aobj.colorDefault;
                        break;
                    }
                }
            }
            
            vobj.textfieldText = [textField.text stringByReplacingCharactersInRange:range withString:string];
            if (![vobj validate])
                break;
        }
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    for (ValidateObj *vobj in validateObjs)
    {
        if (vobj.textfield == textField)
        {
            if (![vobj validate])
                break;
        }
    }
}

- (void)textFieldUpdate:(UITextField *)textField
{
    for (ValidateObj *vobj in validateObjs)
    {
        if (vobj.textfield == textField)
        {
            if (![vobj validate])
                break;
        }
    }
}

- (bool)formPassValidate
{
    bool ret = YES;
    bool isAlert = NO;
    
    for (int i=0; i<[validateObjs count]; i++)
    {
        ValidateObj *vobj = [validateObjs objectAtIndex:i];
        bool lastObjPass = YES;
        
        for (int j=0; j<i; j++)
        {
            ValidateObj *lastObj = [validateObjs objectAtIndex:j];
            
            if (vobj != lastObj && vobj.textfield == lastObj.textfield)
            {
                if (![lastObj validate]) {
                    lastObjPass = NO;
                    ret = NO;
                }
                break;
            }
        }
        
        if (lastObjPass)
        {
            if ([vobj validate] == NO)
            {
                //alert
                if (!isAlert)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:vobj.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    
                    isAlert = YES;
                }
                ret = NO;
            }
        }
    }
    
    return ret;
}


@end