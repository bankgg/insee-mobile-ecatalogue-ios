//
//  LastUpdate.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/7/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LastUpdate : NSObject

@property (nonatomic, retain) NSString *table_name;
@property (nonatomic, retain) NSString *last_value;

+(id)objFromDict:(NSDictionary *)dict;
+(NSArray *)arrayFromResponseObject:(id)responseObject;

+(NSArray *)arrayFromLocalDatabase;

@end
