//
//  LastUpdate.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/7/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "LastUpdate.h"
#import "FMDatabase.h"
#import "ManageData.h"

@implementation LastUpdate

+(id)objFromDict:(NSDictionary *)dict
{
    LastUpdate *obj = [[LastUpdate alloc] init];
    obj.table_name = [dict objectForKey:@"table_name"];
    obj.last_value = [dict objectForKey:@"last_value"];
    
    return obj;
}

+(NSArray *)arrayFromResponseObject:(id)responseObject
{
    NSMutableArray *itemArray = [NSMutableArray arrayWithArray:responseObject];
    NSMutableArray *listArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in itemArray)
    {
        [listArray addObject:[LastUpdate objFromDict:dict]];
    }
    return [NSArray arrayWithArray:listArray];
}

+(NSArray *)arrayFromLocalDatabase
{
    NSMutableArray *arrayMut = [[NSMutableArray alloc] init];
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        NSString *sql = @"SELECT * FROM last_update";
        FMResultSet *s = [db executeQuery:sql];
        
        while ([s next])
        {
            LastUpdate *obj = [[LastUpdate alloc] init];
            obj.table_name = [s stringForColumn:@"table_name"];
            obj.last_value = [s stringForColumn:@"last_value"];
            
            [arrayMut addObject:obj];
        }
        
        [db close];
    }
    
    return [NSArray arrayWithArray:arrayMut];
}

@end
