//
//  Location.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/21/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject

@property int objId;
@property (nonatomic, retain) NSString *shop_name;
@property (nonatomic, retain) NSString *shop_address;
@property (nonatomic, retain) NSString *shop_latitude;
@property (nonatomic, retain) NSString *shop_longtitude;
@property (nonatomic, retain) NSString *shop_telephone;
@property (nonatomic, retain) NSString *shop_mobile;
@property (nonatomic, retain) NSString *shop_website;
@property (nonatomic, retain) NSString *shop_email;
@property int shop_type_id; // type1 = 1000, type2 = 2000

+(id)objFromDict:(NSDictionary *)dict;
+(NSArray *)arrayFromResponseObject:(id)responseObject;

@end
