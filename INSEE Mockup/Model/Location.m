//
//  Location.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/21/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "Location.h"

@implementation Location

+(id)objFromDict:(NSDictionary *)dict
{
    Location *obj = [[Location alloc] init];
    
    obj.objId = [[dict objectForKey:@"id"] intValue];
    obj.shop_name = [dict objectForKey:@"shop_name"];
    obj.shop_address = [dict objectForKey:@"shop_address"];
    obj.shop_latitude = [dict objectForKey:@"shop_latitude"];
    obj.shop_longtitude = [dict objectForKey:@"shop_longtitude"];
    obj.shop_mobile = [dict objectForKey:@"shop_mobile"];
    obj.shop_telephone = [dict objectForKey:@"shop_telephone"];
    obj.shop_website = [dict objectForKey:@"shop_website"];
    obj.shop_email = [dict objectForKey:@"shop_email"];
    obj.shop_type_id = [[dict objectForKey:@"shop_type_id"] intValue];
    
    return obj;
}

+(NSArray *)arrayFromResponseObject:(id)responseObject
{
    NSMutableArray *itemArray = [NSMutableArray arrayWithArray:responseObject];
    NSMutableArray *listArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in itemArray)
    {
        [listArray addObject:[Location objFromDict:dict]];
    }
    return [NSArray arrayWithArray:listArray];
}

@end
