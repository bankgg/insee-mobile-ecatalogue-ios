//
//  ManageData.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/11/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "AFHTTPRequestOperation.h"
#import "AFHTTPRequestOperationManager.h"
#import "Global.h"
#import "LastUpdate.h"
#import "MBProgressHUD.h"

@interface ManageData : NSObject
{
    bool isSyncDone;
    NSError *syncError;
}

+(NSString *)dataBundlePath;
+(NSString *)dataBundleSamplePath;

+(NSString *)databasePath;
+(FMDatabase *)database;

#pragma mark - SYNC DATA
- (void)checkLastUpdate;
@property (nonatomic, retain) id delegate;

@end
