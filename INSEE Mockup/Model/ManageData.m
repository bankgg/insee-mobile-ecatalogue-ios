//
//  ManageData.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/11/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ManageData.h"

@implementation ManageData
{
    NSMutableArray *resyncTableArray;
    int numResyncedTable;
    UIWindow *window;
    MBProgressHUD *hud;
}

+(NSString *)dataBundlePath
{
    NSString *bundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Data"];
    return bundlePath;
}

+(NSString *)dataBundleSamplePath
{
    NSString *bundlePath = [ManageData dataBundlePath];
    NSString *samplePath = [bundlePath stringByAppendingPathComponent:@"Sample"];
    return samplePath;
}

+(NSString *)databasePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"Data"];
    NSString *dbPath   = [dataPath stringByAppendingPathComponent:@"db.sqlite"];
    
    return dbPath;
}

+(FMDatabase *)database
{
    FMDatabase *db = [FMDatabase databaseWithPath:[ManageData databasePath]];
    
    return db;
}


#pragma mark - SYNC DATA

- (void)checkLastUpdate
{
    [self hudShow];
    
    NSLog(@"%@", S_PATH_SERVICE_LAST_UPDATE);
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager.requestSerializer setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [manager GET:[Global PATH_OF:S_PATH_SERVICE_LAST_UPDATE] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"responseObject = %@", responseObject);
         
         NSArray *lastUpdate_server_array = [LastUpdate arrayFromResponseObject:responseObject];
         NSArray *lastUpdate_client_array = [LastUpdate arrayFromLocalDatabase];
         resyncTableArray = [NSMutableArray array];
         
         for (int i=0; i<lastUpdate_client_array.count; i++)
         {
             LastUpdate *lastUpdate_client = [lastUpdate_client_array objectAtIndex:i];
             
             for (int j=0; j<lastUpdate_server_array.count; j++)
             {
                 LastUpdate *lastUpdate_server = [lastUpdate_server_array objectAtIndex:j];
                 
                 if ([lastUpdate_server.table_name isEqualToString:lastUpdate_client.table_name])
                 {
                     if (![lastUpdate_server.last_value isEqualToString:lastUpdate_client.last_value])
                     {
                         NSLog(@"table '%@' is not equal", lastUpdate_server.table_name);
                         NSLog(@"- server value = %@, client value = %@", lastUpdate_server.last_value, lastUpdate_client.last_value);
                         
                         NSDictionary *tableDict = [NSDictionary dictionaryWithObjectsAndKeys:lastUpdate_server.table_name, @"tableName",
                                                    lastUpdate_server.last_value, @"lastValue", nil];
                         [resyncTableArray addObject:tableDict];
                         
//                         [self syncServerDataOfTable:lastUpdate_server.table_name withLastUpdateValue:lastUpdate_server.last_value];
                     }
                     
                     break;
                 }
             }
         }
         
         
         if (resyncTableArray.count > 0)
         {
             for (NSDictionary *tableDict in resyncTableArray)
             {
                 [self syncServerDataOfTable:tableDict[@"tableName"] withLastUpdateValue:tableDict[@"lastValue"]];
             }
         }
         else
         {
             [self hudHide];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         
         NSLog(@"Error1: %@", error);
         
         if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isDatabaseEverSync"])
         {
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
             [alert show];
         }
         
         [self hudHide];
     }];
}

- (void)syncServerDataOfTable:(NSString *)tableName withLastUpdateValue:(NSString *)lastValue
{
//    [self hudShow];
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    
    //temp
    NSLog(@"S_PATH_SERVICE_LAST_DATA = %@", [NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_LAST_DATA], tableName]);
    
    [manager GET:[NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_LAST_DATA], tableName] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //NSLog(@"-------------------------------");
         NSLog(@"responseObject = %@", responseObject);
         
         NSArray *dataArray = [NSArray arrayWithArray:responseObject];
         
         FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueueWithPath:[ManageData databasePath]];
         [queue inDatabase:^(FMDatabase *db) {
             
             //1. DELETE ALL DATA
             NSString *delSqlString = [NSString stringWithFormat:@"DELETE FROM %@", tableName];
             [db executeUpdate:delSqlString];
             
             //2. INSERT TO SQL
             for (NSDictionary *dict in dataArray)
             {
                 //NSLog(@"sql ==> %@", [self insertStringFromDataDict:dict intoTable:tableName]);
                 [db executeUpdate:[self insertStringFromDataDict:dict intoTable:tableName]];
             }
             
             //3. UPDATE 'LAST_UPDATE' TABLE
             NSString *updateString = [NSString stringWithFormat:@"UPDATE last_update SET last_value = '%@' WHERE table_name = '%@'", lastValue, tableName];
             [db executeUpdate:updateString];
             
             NSLog(@"sync done");
             [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isDatabaseEverSync"];
             
             //4. TELL PARENT (DELEGATE) TO RELOAD DATA FROM SQLITE
             if (self.delegate != nil)
             {
                 if ([self.delegate canPerformAction:@selector(reloadData) withSender:nil]) {
                     [self.delegate performSelector:@selector(reloadData) withObject:nil];
                 }
             }
             
             numResyncedTable++;
             hud.progress = (float)numResyncedTable / (float)resyncTableArray.count;
             NSLog(@"sync process = %f", hud.progress);
             
             if (numResyncedTable == resyncTableArray.count) //last one
             {
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 [self hudHide];
             }
         }];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
         syncError = error;
         [self hudHide];
     }];
}

-(NSString *)insertStringFromDataDict:(NSDictionary *)dict intoTable:(NSString *)tableName
{
    NSArray *keyArray = [dict allKeys];
    NSArray *valueArray = [dict allValues];
    
    NSString *columnString = @"(";
    NSString *valueString = @"(";
    for (int i=0; i<keyArray.count; i++)
    {
        columnString = [columnString stringByAppendingString:[keyArray objectAtIndex:i]];
        
        NSString *value = [NSString stringWithFormat:@"%@", [valueArray objectAtIndex:i]];
        value = [value stringByReplacingOccurrencesOfString:@"\"" withString:@"''"];
        
        valueString = [valueString stringByAppendingString:[NSString stringWithFormat:@"\"%@\"",value]];
        
        if (i+1 < keyArray.count)
        {
            columnString = [columnString stringByAppendingString:@","];
            valueString = [valueString stringByAppendingString:@","];
        }
    }
    columnString = [columnString stringByAppendingString:@")"];
    valueString = [valueString stringByAppendingString:@")"];
    
    NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO %@ %@ VALUES %@", tableName, columnString, valueString];
    
    return sqlString;
}

-(void)hudShow
{
    window = [[[UIApplication sharedApplication] windows] lastObject];
    hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Syncing Data";
    hud.dimBackground = YES;
//    [hud show:YES];
    
//    [MBProgressHUD showHUDAddedTo:window animated:YES];
    return;
    
    if (self.delegate != nil)
    {
        UIViewController *parentVC = (UIViewController *)self.delegate;
        [MBProgressHUD showHUDAddedTo:parentVC.view animated:YES];
    }
}

-(void)hudHide
{
    hud.progress = 1.0;
//    UIWindow *window = [[[UIApplication sharedApplication] windows] lastObject];
//    [MBProgressHUD hideHUDForView:window animated:YES];
    [hud hide:YES];
    return;
    
    if (self.delegate != nil)
    {
        UIViewController *parentVC = (UIViewController *)self.delegate;
        [MBProgressHUD hideAllHUDsForView:parentVC.view animated:YES];
    }
}

@end
