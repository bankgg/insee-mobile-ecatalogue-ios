//
//  News.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface News : NSObject

@property int news_id;
@property (nonatomic, retain) NSString *news_header;
@property (nonatomic, retain) NSString *news_detail;
@property (nonatomic, retain) NSDate *news_date;
@property (nonatomic, retain) NSString *news_image_sm_phone_path;
@property (nonatomic, retain) NSString *news_image_sm_tablet_path;
@property (nonatomic, retain) NSString *news_image_lg_phone_path;
@property (nonatomic, retain) NSString *news_image_lg_tablet_path;

+(id)objFromDict:(NSDictionary *)dict;
+(NSArray *)arrayFromResponseObject:(id)responseObject;

+(void)saveLatestNewsValue:(NSString *)newsValue;
+(NSString *)getLastestClientNewsValue;

@end
