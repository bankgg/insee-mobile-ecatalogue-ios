//
//  News.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "News.h"
#import "DateService.h"

@implementation News

+(id)objFromDict:(NSDictionary *)dict
{
    News *aNews = [[News alloc] init];
    
    aNews.news_id = [[dict objectForKey:@"id"] intValue];
    aNews.news_header = [dict objectForKey:@"news_header"];
    aNews.news_detail = [dict objectForKey:@"news_detail"];
    aNews.news_date = [DateService dateFromJsonServiceString:[dict objectForKey:@"news_date"] withTime:NO];
    aNews.news_image_sm_phone_path = [dict objectForKey:@"news_image_sm_phone_path"];
    aNews.news_image_sm_tablet_path = [dict objectForKey:@"news_image_sm_tablet_path"];
    aNews.news_image_lg_phone_path = [dict objectForKey:@"news_image_lg_phone_path"];
    aNews.news_image_lg_tablet_path = [dict objectForKey:@"news_image_lg_tablet_path"];
    
    return aNews;
}

+(NSArray *)arrayFromResponseObject:(id)responseObject
{
    NSMutableArray *itemArray = [NSMutableArray arrayWithArray:responseObject];
    NSMutableArray *listArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dict in itemArray)
    {
        News *obj = [News objFromDict:dict];
        [listArray addObject:obj];
    }
    return [NSArray arrayWithArray:listArray];
}

+(void)saveLatestNewsValue:(NSString *)newsValue
{
    [[NSUserDefaults standardUserDefaults] setObject:newsValue forKey:@"LastNewsValue"];
}

+(NSString *)getLastestClientNewsValue
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"LastNewsValue"];
}

@end
