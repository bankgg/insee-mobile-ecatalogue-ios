//
//  Product.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/20/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property int prodId;
@property int product_cat_id;
@property (nonatomic, retain) NSString *product_code;
@property (nonatomic, retain) NSString *product_name_en;
@property (nonatomic, retain) NSString *product_name_th;
@property (nonatomic, retain) NSString *product_desc_en;
@property (nonatomic, retain) NSString *product_desc_th;
@property (nonatomic, retain) NSString *product_price;
@property (nonatomic, retain) NSString *product_rgb;
@property (nonatomic, retain) NSString *product_pdf_path;
@property (nonatomic, retain) NSString *product_pdf_button_en;
@property (nonatomic, retain) NSString *product_pdf_button_th;
@property (nonatomic, retain) NSString *product_keyword;

@property (nonatomic, retain) NSString *product_image_sm_phone_path;
@property (nonatomic, retain) NSString *product_image_lg_phone_path;
@property (nonatomic, retain) NSString *product_image_sm_tablet_path;
@property (nonatomic, retain) NSString *product_image_lg_tablet_path;

@property (nonatomic, retain) NSArray *prodDetails;
@property (nonatomic, retain) NSArray *prodImages;


@end
