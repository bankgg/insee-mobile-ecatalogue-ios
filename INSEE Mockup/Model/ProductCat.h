//
//  ProductCat.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/21/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductCat : NSObject

@property int catId;
@property int product_cat_parent_id;
@property (nonatomic, retain) NSString *cat_code;
@property (nonatomic, retain) NSString *cat_name;
@property (nonatomic, retain) NSString *cat_desc;
@property (nonatomic, retain) NSString *product_cat_image_phone_path;
@property (nonatomic, retain) NSString *product_cat_image_tablet_path;

@property (nonatomic, retain) NSMutableArray *prodArray;

@end
