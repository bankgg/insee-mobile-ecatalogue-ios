//
//  ProductDetail.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/25/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductDetail : NSObject

@property int prodDetailId;
@property int product_id;
@property (nonatomic, retain) NSString *header;
@property (nonatomic, retain) NSString *detail;

@end
