//
//  ProductDetailImage.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/25/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductDetailImage : NSObject

@property int prodDetailImageId;
@property int product_id;
@property (nonatomic, retain) NSString *product_image_sm_path;
@property (nonatomic, retain) NSString *product_image_lg_path;

@end
