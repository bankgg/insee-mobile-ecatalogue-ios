//
//  Stat.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/10/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "AFHTTPRequestOperationManager.h"
#import "Global.h"

typedef enum {
    pageTypeProductCat = 1100,
    pageTypeProductList = 1200,
    pageTypeProductDetail = 1500,
    pageTypeLocationMap = 2100,
    pageTypeLocationDetail = 2500,
    pageTypeFaqsCat = 3100,
    pageTypeFaqsList = 3200,
    pageTypeNewsList = 4100,
    pageTypeNewsDetail = 4500,
    pageTypeContact = 5100,

    pageTypeTabProduct = 1000,
    pageTypeTabLocation = 2000,
    pageTypeTabFaqs = 3000,
    pageTypeTabNews = 4000,
    pageTypeTabContact = 5000,
    
    
    // NEW IN PHASE 2 -------------
    
    pageTypeProductCat2         = 1150, //Cat lv2
    pageTypeCalculation         = 6100,
    pageTypeShoppingCart        = 6200,
    pageTypeSimulationMain      = 7100,
    pageTypeSimulationPortrait  = 7500,
    pageTypeProjectRefList      = 8100,
    pageTypeProjectRefDetail    = 8500,
    
    pageTypeTabCalculation      = 6000,
    pageTypeTabSimulation       = 7000,
    pageTypeTabMore             = 9000,
    
}PageType;

@interface Stat : NSObject

+(void)saveStatOfPageType:(PageType)pageType pageName:(NSString *)pageName withAction:(NSString *)action;

@end
