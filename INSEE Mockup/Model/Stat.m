//
//  Stat.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/10/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "Stat.h"

@implementation Stat

+(void)saveStatOfPageType:(PageType)pageType pageName:(NSString *)pageName withAction:(NSString *)action
{
    //Set Parameters
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSNumber numberWithInt:[User currentUserId]] forKey:@"user_id"];
    [parameters setValue:[NSNumber numberWithInt:pageType] forKey:@"page_type_id"];
    [parameters setValue:pageName forKey:@"page_name"];
    [parameters setValue:action forKey:@"action"];
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager POST:[Global PATH_OF:S_PATH_SERVICE_STAT_POST] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        if ([responseObject objectForKey:@"error"])
        {
            NSLog(@"Stat : page type id = %d, page name = %@, action = %@, status = %@", pageType, pageName, action, [responseObject objectForKey:@"error"]);
        }
        if ([responseObject objectForKey:@"success"])
        {
            NSLog(@"Stat : page type id = %d, page name = %@, action = %@, status = %@", pageType, pageName, action, [responseObject objectForKey:@"success"]);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
