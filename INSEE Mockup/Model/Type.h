//
//  Type.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/8/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Type : NSObject

@property int objId;
@property int type_group_id;
@property (nonatomic, retain) NSString *type_name;

@end
