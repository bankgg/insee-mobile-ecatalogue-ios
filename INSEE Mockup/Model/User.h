//
//  User.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/9/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property int objId;
@property (nonatomic, retain) NSString *user_email;
@property (nonatomic, retain) NSString *user_password;
@property (nonatomic, retain) NSString *user_firstname;
@property (nonatomic, retain) NSString *user_lastname;
@property (nonatomic, retain) NSDate *user_birthdate;
@property int user_sex_type_id;
@property int user_role_type_id;
@property int user_type_type_id;
@property (nonatomic, retain) NSString *user_club_card;
@property (nonatomic, retain) NSString *user_image_path;

+(id)objFromDict:(NSDictionary *)dict;

+(void)saveUserData:(User *)user;
+(void)clearUserData;

+(BOOL)isUserLogin;
+(int)currentUserId;

@end
