//
//  User.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/9/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "User.h"
#import "DateService.h"

@implementation User

+(id)objFromDict:(NSDictionary *)dict
{
    User *obj = [[User alloc] init];
    
    obj.objId = [[dict objectForKey:@"id"] intValue];
    obj.user_email = [dict objectForKey:@"user_email"];
    obj.user_password = [dict objectForKey:@"user_password"];
    obj.user_firstname = [dict objectForKey:@"user_firstname"];
    obj.user_lastname = [dict objectForKey:@"user_lastname"];
    obj.user_birthdate = [DateService dateFromJsonServiceString:[dict objectForKey:@"user_birthdate"] withTime:NO];
    obj.user_sex_type_id = [[dict objectForKey:@"user_sex_type_id"] intValue];
    obj.user_role_type_id = [[dict objectForKey:@"user_role_type_id"] intValue];
    obj.user_type_type_id = [[dict objectForKey:@"user_type_type_id"] intValue];
    obj.user_club_card = [dict objectForKey:@"user_club_card"];
    obj.user_image_path = [dict objectForKey:@"user_image_path"];
    
    return obj;
}


+(void)saveUserData:(User *)user
{
    [[NSUserDefaults standardUserDefaults] setInteger:user.objId forKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] setObject:user.user_email forKey:@"user_email"];
}

+(void)clearUserData
{
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"user_email"];
}

+(BOOL)isUserLogin
{
    bool isLg = NO;
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"user_id"] > 0) {
        isLg = YES;
    }
    return isLg;
}

+(int)currentUserId
{
    int uId = 0;
    if ([User isUserLogin])
    {
        uId = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"user_id"];
    }
    return uId;
}

@end
