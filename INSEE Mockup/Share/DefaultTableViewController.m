//
//  DefaultTableViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/14/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "DefaultTableViewController.h"

@interface DefaultTableViewController ()

@end

@implementation DefaultTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //select backgroundView
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor clearColor]; //[UIColor colorWithRed:23.0/255.0 green:132.0/255.0 blue:193.0/255.0 alpha:1.0];
    cell.selectedBackgroundView = v;
}

@end
