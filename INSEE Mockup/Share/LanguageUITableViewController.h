//
//  LanguageUITableViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/16/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalizeHelper.h"

@interface LanguageUITableViewController : UITableViewController

@property (nonatomic, retain) NSString *pageLang;

// This function will be called if the language is changed.
// - overide this to reload anything that relate to language
- (void)languageLoad;

@end
