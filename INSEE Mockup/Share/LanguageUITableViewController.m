//
//  LanguageUITableViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/16/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "LanguageUITableViewController.h"

@implementation LanguageUITableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.pageLang = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"];
    
    [self languageLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *userLang = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"];
    if (![userLang isEqualToString:self.pageLang])
    {
        [self languageLoad];
        self.pageLang = userLang;
    }
}

- (void)languageLoad
{
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //select backgroundView
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = v;
}


@end
