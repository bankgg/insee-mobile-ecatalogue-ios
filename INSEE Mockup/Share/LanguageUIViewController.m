//
//  LanguageUIViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/16/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "LanguageUIViewController.h"

@implementation LanguageUIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pageLang = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *userLang = [[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"];
    if (![userLang isEqualToString:self.pageLang])
    {
        [self languageLoad];
        self.pageLang = userLang;
    }
}

- (void)languageLoad
{
    
}

@end
