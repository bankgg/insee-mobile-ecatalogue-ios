//
//  NSString+Language.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/16/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Language)

- (NSString *)languageField;

@end
