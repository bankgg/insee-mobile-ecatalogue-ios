//
//  NSString+Language.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/16/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "NSString+Language.h"

@implementation NSString (Language)

- (NSString *)languageField
{
    NSString *fieldName = [NSString stringWithFormat:@"%@_%@", self, [[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"]];
    return fieldName;
}

@end
