//
//  RowIdentity.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    rowTypeMain,
    rowTypeSub
} RowType;

@interface RowIdentity : NSObject

@property RowType rowType;
@property int rowDataIndex;

@end
