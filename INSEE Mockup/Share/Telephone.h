//
//  Telephone.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/24/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TELEPHONE_EXT @"ext."

@interface Telephone : NSObject <UIActionSheetDelegate>
{
    //UIActionSheet *actSheet;
}

+(NSURL *)phoneUrlFromString:(NSString *)phoneString;

+(void)makeCallFromPhoneNumberString:(NSString *)phoneString;

@end
