//
//  Telephone.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/24/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "Telephone.h"

@implementation Telephone

+(NSURL *)phoneUrlFromString:(NSString *)phoneString
{
    NSMutableCharacterSet *charSet = [NSMutableCharacterSet new];
    [charSet formUnionWithCharacterSet:[NSCharacterSet whitespaceCharacterSet]];
    [charSet formUnionWithCharacterSet:[NSCharacterSet punctuationCharacterSet]];
    [charSet formUnionWithCharacterSet:[NSCharacterSet symbolCharacterSet]];
    NSArray *arrayWithNumbers = [phoneString componentsSeparatedByCharactersInSet:charSet];
    NSString *numberStr = [arrayWithNumbers componentsJoinedByString:@""];
    NSString *promptStr = [NSString stringWithFormat:@"telprompt://%@", numberStr];
    
    return [NSURL URLWithString:promptStr];
}


+(void)makeCallFromPhoneNumberString:(NSString *)phoneString
{
    NSRange extRange = [phoneString rangeOfString:TELEPHONE_EXT];
    if (extRange.location == NSNotFound)
    {
        //normal number
    }
    else
    {
        //number with ext.
        
        phoneString = [phoneString stringByReplacingOccurrencesOfString:TELEPHONE_EXT withString:@";p"];
        phoneString = [phoneString stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    
    NSURL *phoneURL = [self phoneUrlFromString:phoneString];

    if ([[UIApplication sharedApplication] canOpenURL:phoneURL])
    {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot make a call" message:@"Your device is not iPhone or the telephone number is invalid." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}

@end
