//
//  UICommon.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/9/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UICommon : NSObject

+(void)setShadow:(UIView *)_view;
+(void)setShadows:(NSArray *)_views;

+(void)setFontButton:(UIButton *)_button;
+(void)setFontButton:(UIButton *)_button withSize:(float)fontSize;
+(void)setFontButtons:(NSArray *)_buttons;

+(void)setTextField:(UITextField *)_textField;

+(void)setFakeShadow:(UIView *)_view;

//Adjust size
+(float)diffHeightFromAdjustSizeLabel:(UILabel *)label byString:(NSString *)string;
+(float)diffHeightFromAdjustSizeLabel:(UILabel *)label byString:(NSString *)string andNumOfLines:(int)numOfLines;
+(void)adjustPositionOfControl:(UIView *)control byDiffHeight:(float)diffY;
+(void)adjustSizeOfContainer:(UIView *)control byDiffHeight:(float)diffY;

//image
+ (UIImage *)imageWithColor:(UIColor *)color;

//Rotate
+(void)rotateView:(UIView *)rView withDegree:(float)degree;

+(void)setBorder:(UIView *)_view;

+(UIView *)blankHeaderViewAtHeight:(float)height;

+(NSString *)nibFileDevice:(NSString *)nibFile;

//Animation
+(void)fadeIn:(UIView *)_view;

@end
