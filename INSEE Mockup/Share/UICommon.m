//
//  UICommon.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/9/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "UICommon.h"
#import "Global.h"

@implementation UICommon

+(void)setShadow:(UIView *)_view
{
    CALayer *layer = _view.layer;
    
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOpacity = 0.5f;
    layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    layer.shadowRadius = 1.0f;
    layer.masksToBounds = NO;
}

+(void)setShadows:(NSArray *)_views
{
    for (id _view in _views)
    {
        UIView *view = (UIView *)_view;
        [self setShadow:view];
    }
}

+(void)setFontButton:(UIButton *)_button
{
    [self setFontButton:_button withSize:18.0f];
}

+(void)setFontButton:(UIButton *)_button withSize:(float)fontSize
{
    [_button.titleLabel setFont:[UIFont fontWithName:@"INSEE-Bold" size:fontSize]];
}

+(void)setFontButtons:(NSArray *)_buttons
{
    for (UIButton *_button in _buttons)
    {
        [self setFontButton:_button];
    }
}

+(void)setTextField:(UITextField *)_textField
{
    UIFont *textFieldFont = [UIFont fontWithName:@"INSEE" size:20.0f];
    _textField.font = textFieldFont;
}

+(void)setBorder:(UIView *)_view
{
    UIColor *borderColor = [UIColor colorWithWhite:0.75 alpha:1.0];
    [_view.layer setBorderColor:[borderColor CGColor]];
    [_view.layer setBorderWidth:1.0f];
    //[_view setAutoresizingMask:UIViewAutoresizingNone];
}

+(void)setFakeShadow:(UIView *)_view
{
    UIView *fakeShad = [[UIView alloc] init];
    fakeShad.frame = CGRectMake(_view.frame.origin.x +1, _view.frame.origin.y +1, _view.frame.size.width -2, _view.frame.size.height);
    fakeShad.backgroundColor = [UIColor colorWithWhite:0.6 alpha:1.0];
    
    [_view.superview addSubview:fakeShad];
    [_view.superview bringSubviewToFront:_view];
}

#pragma mark - Adjust Size

+(float)diffHeightFromAdjustSizeLabel:(UILabel *)label byString:(NSString *)string
{
    return [self diffHeightFromAdjustSizeLabel:label byString:string andNumOfLines:0];
}
+(float)diffHeightFromAdjustSizeLabel:(UILabel *)label byString:(NSString *)string andNumOfLines:(int)numOfLines
{
    label.text = string;
    
    CGSize originalSize = label.frame.size;
    CGSize maxSize = CGSizeMake(originalSize.width, 9999);
    
    CGFloat maxHeightWithLines = 0;
    if (numOfLines > 0)
    {
        NSString *dummyString = @"a";
        for (int i=1; i<numOfLines; i++)
        {
            dummyString = [dummyString stringByAppendingString:@"\na"];
        }
        CGSize maxSizeWithLines = [dummyString sizeWithFont:label.font constrainedToSize:maxSize lineBreakMode:label.lineBreakMode];
        maxHeightWithLines = maxSizeWithLines.height;
    }
    if (maxHeightWithLines > 0) {
        maxSize = CGSizeMake(originalSize.width, maxHeightWithLines);
    }
    
    CGSize newSize = [string sizeWithFont:label.font constrainedToSize:maxSize lineBreakMode:label.lineBreakMode];
    
    float diffHeight = newSize.height - originalSize.height;
    label.numberOfLines = numOfLines;
    label.frame = CGRectMake(label.frame.origin.x, label.frame.origin.y, originalSize.width, newSize.height);
    
    //NSLog(@"diffHeight = %f - %f", diffHeight, newSize.height);
    
    return diffHeight;
}
+(void)adjustPositionOfControl:(UIView *)control byDiffHeight:(float)diffY
{
    //CGSize originalSize = control.frame.size;
    if (diffY > 0) {
        control.frame = CGRectMake(control.frame.origin.x, control.frame.origin.y + diffY, control.frame.size.width, control.frame.size.height);
    }
}
+(void)adjustSizeOfContainer:(UIView *)control byDiffHeight:(float)diffY
{
    //CGSize originalSize = control.frame.size;
    if (diffY > 0)
    {
        CGRect frame = control.frame;
        frame.size.height += diffY;
        control.frame = frame;
    }
}


+ (UIImage *)imageWithColor:(UIColor *)color
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(void)rotateView:(UIView *)rView withDegree:(float)degree
{
    rView.transform = CGAffineTransformMakeRotation(degree * M_PI/180);
}

+(UIView *)blankHeaderViewAtHeight:(float)height
{
    UIView *blankView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, height)];
    blankView.backgroundColor = [UIColor clearColor];
    return blankView;
}

+(NSString *)nibFileDevice:(NSString *)nibFile
{
    if (IDIOM == IPAD) {
        nibFile = [nibFile stringByAppendingString:@"_iPad"];
    }
    return nibFile;
}

+(void)fadeIn:(UIView *)_view
{
    _view.alpha = 0.0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    
    _view.alpha = 1.0;
    
    [UIView commitAnimations];
}

@end
