//
//  ValidateObj.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 12/3/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    ValidateRequire = 100,
    ValidateEmail = 200,
    ValidatePassword = 310,
    ValidatePasswordConfirm = 320
} ValidateType;

@interface ValidateObj : NSObject
{

}

@property (nonatomic, retain) UITextField *textfield;
@property (nonatomic, retain) UILabel *label;
@property ValidateType validateType;
@property (nonatomic, retain) id arg;

@property (nonatomic, retain) NSString *message;
@property (nonatomic, retain) NSString *textfieldText;

@property (nonatomic, retain) UIColor *colorDefault;
@property (nonatomic, retain) UIColor *colorWarning;

-(bool)validate;

@end
