//
//  ValidateObj.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 12/3/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ValidateObj.h"

@implementation ValidateObj

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(bool)validate
{
    bool ret = NO;
    
    if (self.textfieldText == nil || [self.textfieldText isEqualToString:@""]) {
        self.textfieldText = self.textfield.text;
    }
    
    if (self.validateType == ValidateRequire)
    {
        if (self.textfieldText.length > 0) {
            ret = YES;
        }
        else
        {
            self.message = [NSString stringWithFormat:@"%@ is required.", self.label.text];
        }
    }
    else if (self.validateType == ValidateEmail)
    {
        if ([self NSStringIsValidEmail:self.textfieldText])
        {
            ret = YES;
        }
        else
        {
            self.message = [NSString stringWithFormat:@"%@ is invalid format.", self.label.text];
        }
    }
    else if (self.validateType == ValidatePassword)
    {
        int numChar = 6;
        if (self.textfieldText.length >= numChar || self.textfieldText.length == 0)
        {
            ret = YES;
        }
        else
        {
            self.message = [NSString stringWithFormat:@"%@ requires at least %d characters.", self.label.text, numChar];
        }
    }
    else if (self.validateType == ValidatePasswordConfirm)
    {
        //arg is first password textfield
        UITextField *firstPassTextField = self.arg;
        if ((self.textfieldText == nil || self.textfieldText.length == 0) && (firstPassTextField.text == nil || firstPassTextField.text.length == 0))
        {
            ret = YES;
        }
        else if ([self.textfieldText isEqualToString:firstPassTextField.text])
        {
            ret = YES;
        }
        else
        {
            self.message = [NSString stringWithFormat:@"Confirm Password does not match."];
            
            NSLog(@"self.textfieldText = %@", self.textfieldText);
            NSLog(@"firstPassTextField.text = %@", firstPassTextField.text);
        }
    }
    
    if (ret) {
        self.label.textColor = self.colorDefault;
    }
    else {
        self.label.textColor = self.colorWarning;
    }
    
    return ret;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end
