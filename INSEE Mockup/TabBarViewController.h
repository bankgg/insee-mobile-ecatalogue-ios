//
//  TabBarViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/20/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarViewController : UITabBarController

- (void)reloadTabItemTitles;

@end
