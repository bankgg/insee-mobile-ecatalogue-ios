//
//  TabBarViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/20/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "TabBarViewController.h"
#import "LoginViewController.h"
#import "UICommon.h"
#import "Stat.h"
#import "User.h"
#import "LocalizeHelper.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //first load
    [Stat saveStatOfPageType:pageTypeTabProduct pageName:@"" withAction:@"tab"];
    
    [self reloadTabItemTitles];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadTabItemTitles
{
    NSArray *tabTitleKeys = @[@"product_cat_title", @"cal_title", @"sim_title", @"news_title", @"options_title"];
    
    for (int i=0; i<tabTitleKeys.count; i++)
    {
        UITabBarItem *item = [self.tabBar.items objectAtIndex:i];
        item.title = LocalizedString([tabTitleKeys objectAtIndex:i]);
    }
}

#pragma mark - UITabBarDelegate

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if ([tabBar.items objectAtIndex:0] == item)
    {
        [Stat saveStatOfPageType:pageTypeTabProduct pageName:@"" withAction:@"tab"];
    }
    else if ([tabBar.items objectAtIndex:1] == item)
    {
        [Stat saveStatOfPageType:pageTypeTabCalculation pageName:@"" withAction:@"tab"];
    }
    else if ([tabBar.items objectAtIndex:2] == item)
    {
        [Stat saveStatOfPageType:pageTypeTabSimulation pageName:@"" withAction:@"tab"];
    }
    else if ([tabBar.items objectAtIndex:3] == item)
    {
        [Stat saveStatOfPageType:pageTypeTabNews pageName:@"" withAction:@"tab"];
    }
    else if ([tabBar.items objectAtIndex:4] == item)
    {
        [Stat saveStatOfPageType:pageTypeTabMore pageName:@"" withAction:@"tab"];
    }
    
}

@end
