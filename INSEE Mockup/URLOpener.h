#define BROWSER_CHROME  @"chrome"

@interface URLOpener : NSObject

@property (nonatomic, retain) NSURL * url;
@property (nonatomic, retain) NSString * browser;

- (id) initWithURL:(NSURL *)u;
- (id) initWithBrowser:(NSString *)b;
- (id) initWithURL:(NSURL *)u browser:(NSString *)b;
- (id) initWithURLString:(NSString *)us browser:(NSString *)b;

- (BOOL)openURL;

@end
