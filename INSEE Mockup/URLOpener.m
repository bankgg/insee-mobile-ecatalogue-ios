#import "URLOpener.h"

@implementation URLOpener

@synthesize url, browser;

- (id) initWithURL:(NSURL *)u
{
    self = [super init];
    if (self) {
        self.url = u;
    }
    return self;
}

- (id) initWithBrowser:(NSString *)b
{
    self = [super init];
    if (self) {
        self.browser = b;
    }
    return self;
}

- (id) initWithURL:(NSURL *)u browser:(NSString *)b
{
    self = [super init];
    if (self) {
        self.url = u;
        self.browser = b;
    }
    return self;
}

- (id) initWithURLString:(NSString *)us browser:(NSString *)b
{
    NSURL * u = [NSURL URLWithString:us];
    return [self initWithURL:u browser:b];
}


- (BOOL)openURL
{
    if ([BROWSER_CHROME compare:self.browser options:NSCaseInsensitiveSearch] == NSOrderedSame) {
        return [self openInChrome];
    }
    else if ([[UIApplication sharedApplication] canOpenURL:self.url] )
    {
        return [[UIApplication sharedApplication] openURL:self.url];
    } else {
        NSLog(@"Could not open url: %@", self.url);
        return NO;
    }
}

- (BOOL) openInChrome
{
    // is chrome installed??
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"googlechrome://"]])
    {
        NSString *scheme = self.url.scheme;
        
        // Replace the URL Scheme with the Chrome equivalent.
        NSString * chromeScheme = nil;
        if ([scheme compare:@"http" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            chromeScheme = @"googlechrome";
        } else if ([scheme compare:@"https" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            chromeScheme = @"googlechromes";
        }
        
        if (chromeScheme) {
            NSString *absoluteString = [self.url absoluteString];
            NSRange rangeForScheme = [absoluteString rangeOfString:@":"];
            NSString *urlNoScheme = [absoluteString substringFromIndex:rangeForScheme.location];
            NSString *chromeURLString = [chromeScheme stringByAppendingString:urlNoScheme];
            NSURL *chromeURL = [NSURL URLWithString:chromeURLString];
            return [[UIApplication sharedApplication] openURL:chromeURL];
        } else {
            return [[UIApplication sharedApplication] openURL:self.url];
        }
        
    } else {
        return [[UIApplication sharedApplication] openURL:self.url];
    }
}
@end
