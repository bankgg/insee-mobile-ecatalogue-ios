//
//  CalculationUsageListViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/9/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculationUsageListViewController : UITableViewController

@property int productId;
@property int selectedId;
@property (nonatomic, retain) id calculationVC;

@end
