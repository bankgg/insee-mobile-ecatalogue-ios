//
//  CalculationUsageListViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/9/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "CalculationUsageListViewController.h"

#import "Global.h"
#import "User.h"
#import "ManageData.h"
#import "UICommon.h"
#import "UIImageView+Ext.h"
#import "LocalizeHelper.h"
#import "NSString+Language.h"

#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

#import "CalculationViewController.h"

@interface CalculationUsageListViewController ()
{
    NSMutableArray *usageArray;
}
@end

@implementation CalculationUsageListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = LocalizedString(@"cal_usage_list_title");
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)loadData
{
    usageArray = [NSMutableArray array];
    
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *s = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM formula WHERE product_id = %d", self.productId]];
        while ([s next])
        {
            [usageArray addObject:[s resultDictionary]];
        }
    }
    [db close];
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return usageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary *dict = [usageArray objectAtIndex:indexPath.row];
    cell.textLabel.text = dict[[@"formula_usage" languageField]];
    
    if ([dict[@"id"] intValue] == self.selectedId) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    [self.navigationController popToViewController:self.calculationVC animated:YES];
    
    NSDictionary *dict = [usageArray objectAtIndex:indexPath.row];
    CalculationViewController *calVC = self.calculationVC;
    [calVC updateFormula:[dict[@"id"] intValue]];
}

@end
