//
//  CalculationViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 4/23/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageUITableViewController.h"

#define SECTION__NUM    2

#define SECTION_INPUT   0
#define SECTION_RESULT  1

#define SECTION__INPUT_NUMROW_MIN   2   //Total rows = min row + num of variables
#define ROW_INPUT_PRODCAT           1000
#define ROW_INPUT_PROD              0
#define ROW_INPUT_USAGE             1
#define ROW_INPUT_VARIABLE_START    2

#define SECTION__RESULT_NUMROW      2
#define ROW_RESULT_RESULT           0
#define ROW_RESULT_ADD              1

typedef enum {
    calculationModeNormal = 1,
    calculationModeEditCart = 2
}CalculationMode;

@interface CalculationViewController : LanguageUITableViewController

- (void)updateProduct:(int)productId;
- (void)updateFormula:(int)formulaId;


#pragma mark - Edit Mode for Shopping Cart
@property CalculationMode calMode;
@property int cart_id;
@property (nonatomic, retain) id shoppingCartVC;

@end
