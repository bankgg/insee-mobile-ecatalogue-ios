//
//  CalculationViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 4/23/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "CalculationViewController.h"

#import "Global.h"
#import "User.h"
#import "ManageData.h"
#import "Stat.h"

#import "UICommon.h"
#import "UIImageView+Ext.h"
#import "UIView+StringTag.h"
#import "UIView+Extension.h"
#import "DDMathParser.h"
#import "IQKeyboardManager.h"
#import "NSString+Language.h"

#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

#import "ProductL1ViewController.h"
#import "CalculationUsageListViewController.h"
#import "ShoppingCartViewController.h"

@interface CalculationViewController () <UITextFieldDelegate>
{
    NSDictionary *prodDict;
    NSDictionary *formulaDict;
    NSMutableArray *varArray;
    
    NSNumber *resultNumber;
    NSNumber *resultBase;
    NSNumber *resultSale;
}
@end

@implementation CalculationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.backgroundView = nil;
    
    [[IQKeyboardManager sharedManager] setShouldShowTextFieldPlaceholder:YES];
    
    
    //EDIT CART MODE
    if (self.calMode == calculationModeEditCart)
    {
        FMDatabase *db = [ManageData database];
        if ([db open])
        {
            FMResultSet *s = [db executeQueryWithFormat:@"SELECT p.* FROM cart c \
                              JOIN product p ON p.id = c.product_id \
                              WHERE c.id = %d", self.cart_id];
            
            while ([s next])
            {
                prodDict = [s resultDictionary];
            }
            
            //Load Formula Data
            int formula_num = 0;
            FMResultSet *s2 = [db executeQuery:[NSString stringWithFormat:@"SELECT f.* FROM cart c \
                                                JOIN formula f ON c.formula_id = f.id \
                                                WHERE c.id = %d", self.cart_id]];
            while ([s2 next])
            {
                if (formula_num == 0) {
                    formulaDict = [s2 resultDictionary];
                }
                formula_num++;
            }
            if (formulaDict)
            {
                //Load Formula Vars
                varArray = [NSMutableArray array];
                FMResultSet *s3 = [db executeQuery:[NSString stringWithFormat:@"SELECT fv.*, cv.value AS value, cv.id AS cart_var_id \
                                                    FROM cart_var cv \
                                                    JOIN formula_var fv ON cv.formula_var_id = fv.id \
                                                    WHERE cv.cart_id = %d", self.cart_id]];
                while ([s3 next])
                {
                    [varArray addObject:[[s3 resultDictionary] mutableCopy]];
                }
            }
            else
            {
                varArray = [NSMutableArray array];
            }
            [db close];
            
            [self calculate];
            [self.tableView reloadData];
        }
    }
    
    //stat
    [Stat saveStatOfPageType:pageTypeCalculation pageName:@"" withAction:@"view"];
}

- (void)languageLoad
{
    self.title = LocalizedString(@"cal_title");
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return SECTION__NUM;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    switch (section) {
        case SECTION_INPUT:
            return SECTION__INPUT_NUMROW_MIN + varArray.count;
            break;
        case SECTION_RESULT:
            return SECTION__RESULT_NUMROW;
            break;
            
        default:
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    if (indexPath.section == SECTION_INPUT)
    {
        switch (indexPath.row)
        {
            case ROW_INPUT_PRODCAT:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"cell_prodCat"];
            }
                break;
            case ROW_INPUT_PROD:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"cell_prod"];
                
                UILabel *labelHeader = (id)[cell viewWithTag:101];
                UILabel *labelProdTitle = (id)[cell viewWithTag:1000];
                UIImageView *imageView = (id)[cell viewWithTag:2000];
                UILabel *labelDesc = (id)[cell viewWithTag:3000];
                
                labelHeader.text = LocalizedString(@"cal_label_product");
                
                if (prodDict != nil)
                {
                    labelProdTitle.text = prodDict[[@"product_name" languageField]];
                    
                    [imageView setImageFromImagePathPhone:prodDict[@"product_image_sm_phone_path"] andImagePathTablet:prodDict[@"product_image_sm_tablet_path"]];
                    
                    labelDesc.text = [NSString stringWithFormat:@"[%@ %@ / 1 %@]", formulaDict[@"quantity_per_unit_sale"], formulaDict[[@"formula_unit_base" languageField]], prodDict[[@"unit_sale" languageField]]];
                }
                else
                {
                    labelProdTitle.text = LocalizedString(@"cal_product_name_ph");
                }
                
                [labelProdTitle sizeToFit2];
                [imageView setFramePosY:labelProdTitle.posOfEndY + 2];
                [labelDesc sizeToFit2];
                [labelDesc setFramePosY:imageView.posOfEndY + 2];
                
            }
                break;
            case ROW_INPUT_USAGE:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"cell_prodUsage"];
                
                UILabel *labelHeader = (id)[cell viewWithTag:101];
                UILabel *labelTitle = (id)[cell viewWithTag:1000];
                
                labelHeader.text = LocalizedString(@"cal_label_usage");
                
                if (prodDict != nil)
                {
                    labelHeader.alpha = labelTitle.alpha = 1.0;
                    
                    if (formulaDict != nil)
                    {
                        labelTitle.text = formulaDict[[@"formula_usage" languageField]];
                    }
                    else
                    {
                        labelTitle.text = @"select usage";
                    }
                }
                else
                {
                    labelHeader.alpha = labelTitle.alpha = 0.6;
                    labelTitle.text = @"";
                }
            }
                break;
                
            default:
                break;
        }
        
        if (indexPath.row >= ROW_INPUT_VARIABLE_START)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"cell_var"];
            
            int varIndex = (int)indexPath.row - ROW_INPUT_VARIABLE_START;
            NSMutableDictionary *varDict = [varArray objectAtIndex:varIndex];
            
            UILabel *labelVarName = (id)[cell viewWithTag:1000];
            labelVarName.text = varDict[[@"var_name" languageField]];
            
            UILabel *labelVarUnit = (id)[cell viewWithTag:2000];
            labelVarUnit.text = varDict[[@"var_unit" languageField]];
            
            UITextField *textField = (id)[cell viewWithTag:3000];
            textField.delegate = self;
            [textField addTarget:self action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
            
            if (varDict[@"value"])
            {
                textField.text = [NSString stringWithFormat:@"%@", varDict[@"value"]];
            }
            else
            {
                textField.text = @"";
            }
        }
    }
    else if (indexPath.section == SECTION_RESULT)
    {
        switch (indexPath.row) {
            case ROW_RESULT_RESULT:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"cell_result"];
                
                UILabel *labelHeader = (id)[cell viewWithTag:101];
                UILabel *labelResult = (id)[cell viewWithTag:2000];
                
                labelHeader.text = LocalizedString(@"cal_label_quantity");
                
                if (resultNumber)
                {
                    NSNumberFormatter *formatter = [NSNumberFormatter new];
                    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
                    
                    labelResult.text = [NSString stringWithFormat:@"%@ %@ (%@ %@)", [formatter stringFromNumber:resultBase], formulaDict[[@"formula_unit_base" languageField]], [formatter stringFromNumber:resultSale], prodDict[[@"unit_sale" languageField]]];
                }
                else
                {
                    labelResult.text = @"";
                }
            }
                break;
            case ROW_RESULT_ADD:
            {
                cell = [tableView dequeueReusableCellWithIdentifier:@"cell_add"];
                
                UILabel *label = (id)[cell viewWithTag:101];
                label.text = LocalizedString(@"cal_label_shop_add");
                
                if (self.calMode == calculationModeEditCart)
                {
                    label.text = LocalizedString(@"cal_label_shop_update");
                }
            }
                break;
                
            default:
                break;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_INPUT && indexPath.row == ROW_INPUT_PROD)
    {
        if (prodDict)
        {
            UITableViewCell *cell;
            cell = [tableView dequeueReusableCellWithIdentifier:@"cell_prod"];
            
            UILabel *labelProdTitle = (id)[cell viewWithTag:1000];
            UIImageView *imageView = (id)[cell viewWithTag:2000];
            UILabel *labelDesc = (id)[cell viewWithTag:3000];
            
            labelProdTitle.text = prodDict[[@"product_name" languageField]];
            
            [labelProdTitle sizeToFit2];
            [imageView setFramePosY:labelProdTitle.posOfEndY + 2];
            [labelDesc sizeToFit2];
            [labelDesc setFramePosY:imageView.posOfEndY + 2];
            
            return labelDesc.posOfEndY + 10;
        }
        else
        {
            return (IDIOM == IPAD)?220:140;
        }
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SECTION_INPUT)
    {
        switch (indexPath.row)
        {
            case ROW_INPUT_PRODCAT:
            {
                
            }
                break;
            case ROW_INPUT_PROD:
            {
                ProductL1ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductL1ViewController"];
                vc.listMode = productListModePickList;
                vc.calculationVC = self;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case ROW_INPUT_USAGE:
            {
                CalculationUsageListViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"CalculationUsageListViewController"];
                if (formulaDict) {
                    vc.selectedId = [formulaDict[@"id"] intValue];
                }
                vc.productId = [prodDict[@"id"] intValue];
                vc.selectedId = [formulaDict[@"id"] intValue];
                vc.calculationVC = self;
                [self.navigationController pushViewController:vc animated:YES];
            }
                break;
            case ROW_INPUT_VARIABLE_START:
            {
                
            }
                break;
                
            default:
                break;
        }
    }
    else if (indexPath.section == SECTION_RESULT)
    {
        if (indexPath.row == ROW_RESULT_ADD)
        {
            if (self.calMode == calculationModeEditCart)
            {
                if ([self updateCart])
                {
                    [self.navigationController popToViewController:self.shoppingCartVC animated:YES];
                }
            }
            else
            {
                if([self addToCart])
                {
                    ShoppingCartViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ShoppingCartViewController"];
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
        }
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self dismissKeyboard];
}

- (void)updateProduct:(int)productId
{
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        //Load Product Data
        NSString *sql = [NSString stringWithFormat:@"SELECT * FROM product WHERE id = %d", productId];
        FMResultSet *s = [db executeQuery:sql];
        
        while ([s next])
        {
            prodDict = [s resultDictionary];
        }
        
        //Load Formula Data
        int formula_num = 0;
        FMResultSet *s2 = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM formula WHERE product_id = %d", productId]];
        while ([s2 next])
        {
            if (formula_num == 0) {
                formulaDict = [s2 resultDictionary];
            }
            formula_num++;
        }
        if (formulaDict)
        {
            //Load Formula Vars
            varArray = [NSMutableArray array];
            FMResultSet *s3 = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM formula_var WHERE formula_id = %d", [formulaDict[@"id"] intValue]]];
            while ([s3 next])
            {
                [varArray addObject:[[s3 resultDictionary] mutableCopy]];
            }
        }
        else
        {
            varArray = [NSMutableArray array];
        }
    }
    [db close];
    
    if (prodDict != nil) {
        resultNumber = nil;
        [self.tableView reloadData];
    }
    
}

- (void)updateFormula:(int)formulaId
{
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *s = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM formula WHERE id = %d", formulaId]];
        while ([s next])
        {
            formulaDict = [s resultDictionary];
        }
        if (formulaDict)
        {
            //Load Formula Vars
            varArray = [NSMutableArray array];
            FMResultSet *s3 = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM formula_var WHERE formula_id = %d", [formulaDict[@"id"] intValue]]];
            while ([s3 next])
            {
                [varArray addObject:[[s3 resultDictionary] mutableCopy]];
            }
        }
        else
        {
            varArray = [NSMutableArray array];
        }
    }
    [db close];
    
    if (formulaDict) {
        resultNumber = nil;
        [self.tableView reloadData];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self dismissKeyboard];
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

#pragma mark - TextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // allow backspace
    if (!string.length)
    {
        return YES;
    }
    
    // Only Number and dot
    NSCharacterSet *charset = [NSCharacterSet characterSetWithCharactersInString:@"0123456789."];
    if ([string rangeOfCharacterFromSet:[charset invertedSet]].location != NSNotFound)
    {
        // BasicAlert(@"", @"This field accepts only numeric entries.");
        return NO;
    }
    
    // Prevent invalid character input, if keyboard is numberpad
    if (textField.keyboardType == UIKeyboardTypeNumberPad)
    {
        
    }
    
    // verify max length has not been exceeded
    NSString *updatedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (updatedText.length > 10) // 4 was chosen for SSN verification
    {
        // suppress the max length message only when the user is typing
        // easy: pasted data has a length greater than 1; who copy/pastes one character?
        if (string.length > 1)
        {
            // BasicAlert(@"", @"This field accepts a maximum of 4 characters.");
        }
        
        return NO;
    }
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *number = [f numberFromString:updatedText];
    if (number == nil) {
        return NO;
    }
    
    return YES;
}

- (void)textFieldValueChanged:(UITextField *)textField
{
    UITableViewCell *cell = (id)[textField findSuperViewWithClass:[UITableViewCell class]];
    if (cell)
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        NSLog(@"indexPath.row = %ld", (long)indexPath.row);
        
        int varIndex = (int)indexPath.row - ROW_INPUT_VARIABLE_START;
        NSMutableDictionary *varDict = [varArray objectAtIndex:varIndex];
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        
        NSNumber *number = [NSNumber numberWithFloat:0];
        if ([f numberFromString:textField.text] != nil)
        {
            number = [f numberFromString:textField.text];
        }
        [varDict setObject:number forKey:@"value"];
        
        [self calculate];
        NSIndexPath *resultIndexPath = [NSIndexPath indexPathForRow:ROW_RESULT_RESULT inSection:SECTION_RESULT];
        [self.tableView reloadRowsAtIndexPaths:@[resultIndexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)calculate
{
    if (formulaDict)
    {
        NSString *formulaText = formulaDict[@"formula_formula"];
        
        BOOL canCalculate = YES;
        for (NSMutableDictionary *var in varArray)
        {
            if (!var[@"value"])
            {
                canCalculate = NO;
            }
            else
            {
                formulaText = [formulaText stringByReplacingOccurrencesOfString:var[@"var_var"] withString:[NSString stringWithFormat:@"%@", var[@"value"]]];
            }
        }
        
        if (canCalculate)
        {
            
            @try
            {
                // do work
                resultNumber = [formulaText numberByEvaluatingString];
                
                NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                f.numberStyle = NSNumberFormatterDecimalStyle;
                
                NSNumber *quantity_per_unit_base = [f numberFromString:formulaDict[@"quantity_per_unit_base"]];
                NSNumber *quantity_per_unit_sale = [f numberFromString:formulaDict[@"quantity_per_unit_sale"]];
                
                float floatBase = [resultNumber floatValue] / [quantity_per_unit_base floatValue];
                resultBase = [NSNumber numberWithFloat:floatBase];
                
                float floatSale = floatBase / [quantity_per_unit_sale floatValue];
                resultSale = [NSNumber numberWithInt:ceil(floatSale)];
                
            }
            @catch(NSException* ex)
            {
                // handle
                resultNumber = [NSNumber numberWithInt:0];
            }
        }
    }
}

- (BOOL)addToCart
{
    BOOL isAdded = NO;
    
    if ([resultNumber floatValue] > 0)
    {
        FMDatabase *db = [ManageData database];
        if ([db open])
        {
            if([db executeUpdateWithFormat:@"INSERT INTO cart (product_id, formula_id, result_base, result_sale) VALUES (%@, %@, %@, %@)", prodDict[@"id"], formulaDict[@"id"], resultBase, resultSale])
            {
                NSLog(@"cart inserted");
                
                sqlite_int64 inserted_id = [db lastInsertRowId];
                
                [[NSUserDefaults standardUserDefaults] setInteger:(int)inserted_id forKey:@"cart_last_update_id"];
                
                for (NSDictionary *var in varArray)
                {
                    [db executeUpdateWithFormat:@"INSERT INTO cart_var (cart_id, formula_var_id, value) VALUES (%lld, %@, %@)", inserted_id, var[@"id"], var[@"value"]];
                }
                
                isAdded = YES;
            }
            [db close];
        }
    }
    
    return isAdded;
}

#pragma mark - Edit Mode for Shopping Cart

- (BOOL)updateCart
{
    BOOL isSaved = NO;
    
    if ([resultNumber floatValue] > 0)
    {
        FMDatabase *db = [ManageData database];
        if ([db open])
        {
            [db beginTransaction];
            
            if([db executeUpdateWithFormat:@"UPDATE cart SET product_id = %@, formula_id = %@, result_base = %@, result_sale = %@ WHERE id = %d",
                prodDict[@"id"], formulaDict[@"id"], resultBase, resultSale, self.cart_id])
            {
                NSLog(@"cart saved");
                
                for (NSDictionary *var in varArray)
                {
                    [db executeUpdateWithFormat:@"UPDATE cart_var SET cart_id = %d, formula_var_id = %@, value = %@ WHERE id = %@",
                     self.cart_id, var[@"id"], var[@"value"], var[@"cart_var_id"]];
                }
                
                isSaved = YES;
            }
            
            [db commit];
            [db close];
        }
    }
    
    //Trigger
    if (isSaved) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"trigger_cart_update"];
        [[NSUserDefaults standardUserDefaults] setInteger:self.cart_id forKey:@"cart_last_update_id"];
    }
    
    return isSaved;
}


@end
