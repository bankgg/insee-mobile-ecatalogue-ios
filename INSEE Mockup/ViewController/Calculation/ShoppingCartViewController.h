//
//  ShoppingCartViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/12/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "LanguageUIViewController.h"

@interface ShoppingCartViewController : LanguageUIViewController

@end
