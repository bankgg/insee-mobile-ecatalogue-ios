//
//  ShoppingCartViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/12/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "ShoppingCartViewController.h"
#import <MessageUI/MFMailComposeViewController.h>

#import "Global.h"
#import "User.h"
#import "ManageData.h"
#import "Stat.h"

#import "UICommon.h"
#import "UIImageView+Ext.h"
#import "UIView+StringTag.h"
#import "UIView+Extension.h"

#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

#import "CalculationViewController.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"
#import "NSString+Ext.h"

#import "AppDelegate.h"

typedef enum {
    listModeNormal,
    listModeEmail
} ListMode;

@interface ShoppingCartViewController () <UITableViewDataSource, UITableViewDelegate,
                            UIActionSheetDelegate, MFMailComposeViewControllerDelegate>
{
    NSMutableArray *listContent;
    
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *labelTotal;
@property (weak, nonatomic) IBOutlet UIView *viewTop;

@property (weak, nonatomic) IBOutlet UIView *viewEmailMode;
@property (weak, nonatomic) IBOutlet UIButton *buttonSendMail;
@property (weak, nonatomic) IBOutlet UILabel *labelSelectItem;
- (IBAction)buttonSendMailPressed:(id)sender;

@property ListMode listMode;

@end

@implementation ShoppingCartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupWording];
    self.listMode = listModeNormal;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
//    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.backgroundColor = [UIColor clearColor];
    
    [self.viewEmailMode setFramePosY:-self.viewEmailMode.frame.size.height];
    
    [self setupMode];
    
    //RefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    [self.viewTop makeItShadowRect];
    
    [self loadData];
    
    //stat
    [Stat saveStatOfPageType:pageTypeShoppingCart pageName:@"" withAction:@"view"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"trigger_cart_update"])
    {
        listContent = nil;
        [self loadData];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"trigger_cart_update"];
    }
}

- (void)languageLoad
{
    [self.tableView reloadData];
    [self updateTotal];
    [self setupWording];
}

- (void)setupWording
{
    self.title = LocalizedString(@"cart_title");
    self.labelSelectItem.text = LocalizedString(@"cart_select_item");
    [self.buttonSendMail setTitle:LocalizedString(@"cart_send_mail_button") forState:UIControlStateNormal];
}

#pragma mark - Load Data

- (void)loadData
{
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *s = [db executeQueryWithFormat:@"  SELECT c.id, c.product_id, c.formula_id, c.result_base, c.result_sale, \
                          p.product_name_en, p.product_name_th, p.product_rgb, p.product_image_sm_phone_path, p.product_image_sm_tablet_path, \
                          p.unit_sale_en, p.unit_sale_th, \
                          f.formula_unit_base_en, f.formula_unit_base_th \
                          FROM cart c \
                          JOIN product p ON c.product_id = p.id \
                          JOIN formula f ON c.formula_id = f.id \
                          "];
        
        while ([s next])
        {
            if (!listContent)
            {
                listContent = [NSMutableArray array];
            }
            
            // Default select
            NSMutableDictionary *dict = [[s resultDictionary] mutableCopy];
            [dict setObject:@"1" forKey:@"select"];
            
            [listContent addObject:dict];
        }
        
        [self.tableView reloadData];
        [self updateTotal];
        
        [db close];
        [refreshControl endRefreshing];
    }
    else
    {
        [refreshControl endRefreshing];
    }
}

- (void)reloadData
{
    listContent = [NSMutableArray array];
    [self loadData];
}

- (void)updateTotal
{
    self.labelTotal.text = [NSString stringWithFormat:LocalizedString(@"cart_total"), listContent.count];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listContent.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return IS_PAD?100:70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor clearColor];
    return v;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    UILabel *labelProductName = (id)[cell viewWithTag:1000];
    labelProductName.text = dict[[@"product_name" languageField]];
    [labelProductName sizeToFit2withNumLines:2];
    [labelProductName setFrameCenterY:labelProductName.superview.frame.size.height / 2];
    
    UIImageView *imageView = (id)[cell viewWithTag:2000];
    [imageView setImageFromImagePathPhone:dict[@"product_image_sm_phone_path"] andImagePathTablet:dict[@"product_image_sm_tablet_path"]];
    
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setPositiveFormat:@",###.##"];
    
    UILabel *labelBase = (id)[cell viewWithTag:3000];
    labelBase.text = [NSString stringWithFormat:@"%@ %@", [formatter stringFromNumber:dict[@"result_base"]], dict[[@"formula_unit_base" languageField]]];
    
    UILabel *labelSale = (id)[cell viewWithTag:4000];
    labelSale.text = [NSString stringWithFormat:@"%@ %@", dict[@"result_sale"], dict[[@"unit_sale" languageField]]];
    
    //highlight last update row
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"cart_last_update_id"] == [dict[@"id"] intValue])
    {
        cell.backgroundColor = [UIColor colorWithRed:254.0/255.0 green:251.0/255.0 blue:230.0/255.0 alpha:1.0];
        
        //next time, not show the color
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"cart_last_update_id"];
    }
    else
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    UIImageView *imageViewAcc = (id)[cell viewWithTag:5000];
    if (self.listMode == listModeNormal)
    {
        imageViewAcc.hidden = NO;
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else if (self.listMode == listModeEmail)
    {
        imageViewAcc.hidden = YES;
        if (![NSString isNullOrEmpty:dict[@"select"]])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSMutableDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    if (self.listMode == listModeNormal)
    {
        CalculationViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"CalculationViewController"];
        vc.calMode = calculationModeEditCart;
        vc.shoppingCartVC = self;
        vc.cart_id = [dict[@"id"] intValue];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (self.listMode == listModeEmail)
    {
        if ([NSString isNullOrEmpty:dict[@"select"]]) {
            [dict setObject:@"1" forKey:@"select"];
        } else {
            [dict removeObjectForKey:@"select"];
        }
        [listContent setObject:dict atIndexedSubscript:indexPath.row];
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

#pragma mark - Delete Row

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listMode == listModeEmail) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
        
        BOOL isDeleted = NO;
        FMDatabase *db = [ManageData database];
        if ([db open])
        {
            [db beginTransaction];
            BOOL ret = NO;
            
            ret = [db executeUpdateWithFormat:@"DELETE FROM cart_var WHERE cart_id = %@", dict[@"id"]];
            if (ret)
            {
                ret = [db executeUpdateWithFormat:@"DELETE FROM cart WHERE id = %@", dict[@"id"]];
                if (!ret)
                {
                    // error
                }
            }
            
            if (ret)
            {
                [db commit];
                isDeleted = YES;
            }
            else
            {
                [db rollback];
            }
        }
        
        if (isDeleted)
        {
            [listContent removeObjectAtIndex:indexPath.row];
            
            [tableView beginUpdates];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView endUpdates];
            
            [self updateTotal];
        }
    }
}

#pragma mark - Action Button

- (void)actionButtonPressed:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:LocalizedString(@"cart_send_mail_cancel_button")
                                         destructiveButtonTitle:LocalizedString(@"cart_send_mail_action")
                                              otherButtonTitles:nil];
    [sheet showFromBarButtonItem:sender animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        self.listMode = listModeEmail;
        [self setupMode];
    }
}

- (void)sendMail
{
    NSString *productListHtml = @"";
    for (NSDictionary *dict in listContent)
    {
        if (![NSString isNullOrEmpty:dict[@"select"]])
        {
            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
            [formatter setPositiveFormat:@",###.##"];
            
            NSString *result1 = [NSString stringWithFormat:@"%@ %@", [formatter stringFromNumber:dict[@"result_base"]], dict[[@"formula_unit_base" languageField]]];
            
            NSString *result2 = [NSString stringWithFormat:@"%@ %@", dict[@"result_sale"], dict[[@"unit_sale" languageField]]];
            
            NSString *rowHtml = [NSString stringWithFormat:@"<tr>\
                                 <td bgcolor='#EEEEEE' style='color:#333;'>%@</td>\
                                 <td bgcolor='#EEEEEE' style='color:#333;'>%@, %@</td>\
                                 </tr>",
                                 dict[[@"product_name" languageField]],
                                 result1, result2];
            
            productListHtml = [productListHtml stringByAppendingString:rowHtml];
        }
    }
    
    NSString *htmlBody = [NSString stringWithFormat:@"\
                          \
                          <table border='0' cellpadding='5' width='100%%'>\
                          <tr>\
                          <th bgcolor='#666666' style='color:#FFF;'>Product</th>\
                          <th bgcolor='#666666' style='color:#FFF;'>Quantity</th></tr>\
                          %@\
                          </table>", productListHtml];
    
    
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setSubject:@"INSEE Mobile Catalogue - Shopping Cart List"];
    [controller setMessageBody:htmlBody isHTML:YES];
    controller.modalPresentationStyle = UIModalPresentationFullScreen;
    
    controller.navigationBar.tintColor = [UIColor blackColor];
    controller.navigationBar.barTintColor = [UIColor blackColor];
    
    UIFont *navFont = [UIFont fontWithName:@"INSEE" size:14.0f];
    [[UINavigationBar appearance] setTitleTextAttributes: @{
                                                            NSForegroundColorAttributeName: [UIColor blackColor],
                                                            NSFontAttributeName: navFont
                                                            }];
    [controller.navigationBar setTitleTextAttributes: @{ NSForegroundColorAttributeName:[UIColor blackColor],
                                                         NSFontAttributeName: navFont
                                                         }];
    
    
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    AppDelegate *app = [UIApplication sharedApplication].delegate;
    [app setupUiAppearance];
    
    [self cancelButtonPressed];
}

#pragma mark - Email Mode

- (void)setupMode
{
    if (self.listMode == listModeNormal)
    {
        //Action Button
        UIBarButtonItem *actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonPressed:)];
        self.navigationItem.rightBarButtonItem = actionButton;
        
        [self.tableView reloadData];

        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.viewEmailMode setFramePosY:-self.viewEmailMode.frame.size.height];
        } completion:nil];
    }
    else if (self.listMode == listModeEmail)
    {
        //Cancel Button
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:LocalizedString(@"cart_send_mail_cancel_button") style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
        self.navigationItem.rightBarButtonItem = cancelButton;
        
        [self.tableView reloadData];
        
        [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self.viewEmailMode setFramePosY:0];
        } completion:nil];
    }
}

- (void)cancelButtonPressed
{
    [self reloadData];
    
    self.listMode = listModeNormal;
    [self setupMode];
}

- (IBAction)buttonSendMailPressed:(id)sender
{
    [self sendMail];
}
@end
