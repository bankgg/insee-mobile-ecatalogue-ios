//
//  ContactListViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/18/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "ContactListViewController.h"

#import "Stat.h"
#import "Telephone.h"
#import "LocalizeHelper.h"
#import "UIView+Extension.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "DocViewerViewController.h"
#import "SVWebViewController.h"
#import <MessageUI/MessageUI.h>

@interface ContactListViewController () <MFMailComposeViewControllerDelegate>
{
    NSArray *listContent;
}
- (IBAction)phoneButtonPressed:(id)sender;
- (IBAction)facebookButtonPressed:(id)sender;
@end

@implementation ContactListViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    self.title = LocalizedString(@"contact_title");
    
    NSDictionary *contact1 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"contact_sccc"], @"image", @"wecare@sccc.co.th", @"email" , nil];
    NSDictionary *contact2 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"contact_scco"], @"image", @"wecare@sccc.co.th", @"email" , nil];
    NSDictionary *contact3 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"contact_conwood"], @"image", @"customerservice@conwood.co.th", @"email" , nil];
    NSDictionary *contact4 = [NSDictionary dictionaryWithObjectsAndKeys:[UIImage imageNamed:@"contact_isub"], @"image", @"wecare@sccc.co.th", @"email" , nil];
    
    listContent = @[contact1, contact2, contact3, contact4];
    
    //stat
    [Stat saveStatOfPageType:pageTypeContact pageName:@"" withAction:@"view"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return listContent.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary *dict = [listContent objectAtIndex:indexPath.item];
    
    UIView *bottomFrame = (id)[cell viewWithTag:10000];
    [bottomFrame makeBorder];
    
    UIImageView *imageView = (id)[cell viewWithTag:1000];
    [imageView setImage:dict[@"image"]];
    
    UILabel *labelEmail = (id)[cell viewWithTag:2000];
    labelEmail.text = dict[@"email"];
    
    [cell makeBorder];
    
    return cell;
}

#pragma mark - Header

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return (IDIOM == IPAD)?CGSizeMake(self.view.frame.size.width, 440):CGSizeMake(320, 150);
    }
    return CGSizeZero;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                                            UICollectionElementKindSectionHeader withReuseIdentifier:@"head" forIndexPath:indexPath];
    
    UIView *frameView = (id)[headerView viewWithTag:10000];
    [frameView makeBorder];
    
    return headerView;
}

#pragma mark <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [listContent objectAtIndex:indexPath.item];
    [self sendMail:dict[@"email"]];
}

#pragma mark - Send Mail

- (void)sendMail:(NSString *)email
{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setToRecipients:[NSArray arrayWithObjects:email, nil]];
    [controller setSubject:@"Sent from INSEE App"];
    [controller setMessageBody:@"" isHTML:NO];
    controller.modalPresentationStyle = UIModalPresentationFullScreen;
    
    [controller.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName: [UIColor darkGrayColor],
                                                        }];
    
    [self presentViewController:controller animated:YES completion:nil];
    
    
//    NSURL* mailURL = [NSURL URLWithString:[NSString stringWithFormat:@"mailto://%@", email]];
//    if ([[UIApplication sharedApplication] canOpenURL:mailURL]) {
//        [[UIApplication sharedApplication] openURL:mailURL];
//    }
    
    
    
    //stat
    [Stat saveStatOfPageType:pageTypeContact pageName:@"contact" withAction:[NSString stringWithFormat:@"action mail to %@", email]];
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)phoneButtonPressed:(id)sender
{
    [Telephone makeCallFromPhoneNumberString:@"1732"];
    
    //stat
    [Stat saveStatOfPageType:pageTypeContact pageName:@"contact" withAction:@"action call"];
}

- (IBAction)facebookButtonPressed:(id)sender
{
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithAddress:URL_FACEBOOK_FANPAGE];
    [self presentViewController:webViewController animated:YES completion:NULL];
    
    //stat
    [Stat saveStatOfPageType:pageTypeContact pageName:@"contact" withAction:@"action facebook"];
}

@end
