//
//  ContactViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/14/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "Global.h"

#define CONTACT_MAIL @"wecare@sccc.co.th"
#define CONTACT_PHONE @"1732"

@interface ContactViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
    __weak IBOutlet UIView *centerView;
    __weak IBOutlet UIView *frameBox;
}

-(IBAction)button1Pressed:(id)sender;
-(IBAction)button2Pressed:(id)sender;
-(IBAction)button3Pressed:(id)sender;
-(IBAction)button4Pressed:(id)sender;

@end
