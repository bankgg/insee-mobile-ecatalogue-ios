//
//  ContactViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/14/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ContactViewController.h"
#import "UICommon.h"
#import "AlignButton.h"
#import "DocViewerViewController.h"
#import "OptionViewController.h"
#import "Telephone.h"
#import "Stat.h"

@interface ContactViewController ()

@end

@implementation ContactViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    [UICommon setBorder:frameBox];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Option Button

- (void)optionPressed
{
    OptionViewController *optionVC = [[Global storyboardIphone] instantiateViewControllerWithIdentifier:@"OptionViewController"];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:optionVC];
    [self presentViewController:navVC animated:YES completion:nil];
}

-(IBAction)button1Pressed:(id)sender
{
    [Telephone makeCallFromPhoneNumberString:CONTACT_PHONE];
    
    //stat
    [Stat saveStatOfPageType:pageTypeContact pageName:@"contact" withAction:@"action call"];
}
-(IBAction)button2Pressed:(id)sender
{
    MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
    controller.mailComposeDelegate = self;
    [controller setToRecipients:[NSArray arrayWithObjects:CONTACT_MAIL, nil]];
    [controller setSubject:@"Sent from INSEE App"];
    [controller setMessageBody:@"" isHTML:NO];
    controller.modalPresentationStyle = UIModalPresentationFormSheet;
    
    controller.navigationBar.tintColor = [UIColor whiteColor];
    
    [self presentViewController:controller animated:YES completion:nil];
    
    //stat
    [Stat saveStatOfPageType:pageTypeContact pageName:@"contact" withAction:@"action email"];
}
-(IBAction)button3Pressed:(id)sender
{
    DocViewerViewController *vc = [[DocViewerViewController alloc] initWithNibName:@"DocViewerViewController" bundle:nil];
    vc.webURL = URL_FACEBOOK_FANPAGE;
    [self presentViewController:vc animated:YES completion:nil];
    
    //stat
    [Stat saveStatOfPageType:pageTypeContact pageName:@"contact" withAction:@"action facebook"];
}
-(IBAction)button4Pressed:(id)sender
{
    
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
