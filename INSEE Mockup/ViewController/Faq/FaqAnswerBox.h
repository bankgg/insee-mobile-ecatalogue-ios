//
//  FaqAnswerBox.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#define FAQA1_HEIGHT  38
#define FAQA1_HEIGHT_IPAD  55

@interface FaqAnswerBox : UIView

@property (weak, nonatomic) IBOutlet UILabel *labAnswer;
@property (weak, nonatomic) IBOutlet UIView *frameBoxView;

-(void)initial;

-(float)diffYFromSetText:(NSString *)text;
+(float)viewHeightFromText:(NSString *)text;

@end
