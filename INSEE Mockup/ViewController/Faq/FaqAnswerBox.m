//
//  FaqAnswerBox.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "FaqAnswerBox.h"
#import "UICommon.h"

@implementation FaqAnswerBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initial
{
    [UICommon setBorder:self.frameBoxView];
    
    if (IDIOM != IPAD)
    {
        self.labAnswer.font = [UIFont fontWithName:@"INSEEText" size:16.0f];
    }
    else
    {
        self.labAnswer.font = [UIFont fontWithName:@"INSEEText" size:20.0f];
    }
}

-(float)diffYFromSetText:(NSString *)text
{
    self.labAnswer.text = text;
    
    float diffY = 0;
    diffY = [UICommon diffHeightFromAdjustSizeLabel:self.labAnswer byString:self.labAnswer.text];
    
    if (diffY > 0)
    {
        CGRect boxFrame = self.frameBoxView.frame;
        boxFrame.size.height += diffY;
        self.frameBoxView.frame = boxFrame;
        
        CGRect frame = self.frame;
        frame.size.height += diffY;
        self.frame = frame;
    }
    
    return diffY;
}

+(float)viewHeightFromText:(NSString *)text
{
    float viewHeight = FAQA1_HEIGHT;
    if (IDIOM == IPAD) {
        viewHeight = FAQA1_HEIGHT_IPAD;
    }
    
    FaqAnswerBox *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqAnswerBox"] owner:self options:nil]objectAtIndex:0];
    [box initial];
    
    float diffY = [box diffYFromSetText:text];
    
    if (diffY > 0) {
        viewHeight += diffY;
    }
    
    return viewHeight;
}

@end
