//
//  FaqHeaderBox.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/11/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"

#define FAQ_HEADER_HEIGHT  40
#define FAQ_HEADER_HEIGHT_IPAD  65

@interface FaqHeaderBox : UIView

@property (weak, nonatomic) IBOutlet UILabel *labTitle;

-(void)initial;

-(float)diffYFromSetTitle:(NSString *)title;

+(float)viewHeightFromTitle:(NSString *)title;

@end
