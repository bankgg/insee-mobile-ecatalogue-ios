//
//  FaqHeaderBox.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 11/11/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "FaqHeaderBox.h"
#import "UICommon.h"

@implementation FaqHeaderBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initial
{
    float fontSize = IS_PAD?28.0f:20.0f;
    self.labTitle.font = [UIFont fontWithName:@"INSEE" size:fontSize];
}

-(float)diffYFromSetTitle:(NSString *)title;
{
    self.labTitle.text = title;
    
    float diffY = 0;
    diffY = [UICommon diffHeightFromAdjustSizeLabel:self.labTitle byString:self.labTitle.text];
    
    if (diffY > 0)
    {
        //CGRect boxFrame = self.frameBoxView.frame;
        //boxFrame.size.height += diffY;
        //self.frameBoxView.frame = boxFrame;
        
        CGRect frame = self.frame;
        frame.size.height += diffY;
        self.frame = frame;
    }
    
    return diffY;
}

+(float)viewHeightFromTitle:(NSString *)title;
{
    float viewHeight = IS_PAD?FAQ_HEADER_HEIGHT_IPAD-1:FAQ_HEADER_HEIGHT-1;
    
    FaqHeaderBox *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqHeaderBox"] owner:self options:nil]objectAtIndex:0];
    
    float diffY = [box diffYFromSetTitle:title];
    NSLog(@"faq diffY = %f - %@", diffY, title);
    
    if (diffY > 0) {
        viewHeight += diffY;
    }
    
    return viewHeight;
}

@end
