//
//  FaqL1Box.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"

#define FAQL1_HEIGHT  40
#define FAQL1_HEIGHT_IPAD  65

typedef enum {
    arrowTypeRight,
    arrowTypeDown
} ArrowType;

@interface FaqL1Box : UIView
{
    float rotateDegree;
}
@property (weak, nonatomic) IBOutlet UIView *frameBoxView;
@property (weak, nonatomic) IBOutlet UILabel *labTitle;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UIImageView *arrowDownImageView;

-(void)initialWithArrowType:(ArrowType)arrowType;

-(void)rotateArrow;

-(void)setArrowInitialAsUp;

-(float)diffYFromSetTitle:(NSString *)title;

+(float)viewHeightFromTitle:(NSString *)title;

@end
