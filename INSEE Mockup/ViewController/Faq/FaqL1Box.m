//
//  FaqL1Box.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "FaqL1Box.h"
#import "UICommon.h"

@implementation FaqL1Box

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initialWithArrowType:(ArrowType)arrowType
{
    if (IDIOM != IPAD) {
        self.labTitle.font = [UIFont fontWithName:@"INSEE" size:20.0f];
        self.frame = CGRectMake(0, 0, 320, FAQL1_HEIGHT);
    }
    else {
        self.labTitle.font = [UIFont fontWithName:@"INSEE" size:28.0f];
        self.frame = CGRectMake(0, 0, 768, FAQL1_HEIGHT_IPAD);
    }
    
    [UICommon setBorder:self.frameBoxView];
    
    if (arrowType == arrowTypeDown) {
        self.arrowImageView.hidden = YES;
    }
    else {
        self.arrowDownImageView.hidden = YES;
    }
    
    rotateDegree = 180;
}

-(void)rotateArrow
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    self.arrowImageView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    [UICommon rotateView:self.arrowDownImageView withDegree:rotateDegree];
    
    [UIView commitAnimations];
    
    if (rotateDegree == 180)
    {
        rotateDegree = 0;
    }
    else {
        rotateDegree = 180;
    }
}

-(void)setArrowInitialAsUp
{
    rotateDegree = 180;
    self.arrowImageView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    [UICommon rotateView:self.arrowDownImageView withDegree:rotateDegree];
    rotateDegree = 0;
}

-(float)diffYFromSetTitle:(NSString *)title;
{
    self.labTitle.text = title;
    
    float diffY = 0;
    diffY = [UICommon diffHeightFromAdjustSizeLabel:self.labTitle byString:self.labTitle.text];
    
    if (diffY > 0)
    {
        CGRect boxFrame = self.frameBoxView.frame;
        boxFrame.size.height += diffY;
        self.frameBoxView.frame = boxFrame;
        
        CGRect frame = self.frame;
        frame.size.height += diffY;
        self.frame = frame;
    }
    
    self.arrowDownImageView.center = CGPointMake(self.arrowDownImageView.center.x, self.frameBoxView.frame.size.height / 2);
    self.arrowImageView.center = CGPointMake(self.arrowImageView.center.x, self.frameBoxView.frame.size.height / 2);
    
    return diffY;
}

+(float)viewHeightFromTitle:(NSString *)title;
{
    float viewHeight = FAQL1_HEIGHT - 1;
    if (IDIOM == IPAD) {
        viewHeight = FAQL1_HEIGHT_IPAD - 1;
    }
    
    FaqL1Box *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqL1Box"] owner:self options:nil]objectAtIndex:0];
    [box initialWithArrowType:arrowTypeRight];
    
    float diffY = [box diffYFromSetTitle:title];
    NSLog(@"faq diffY = %f - %@", diffY, title);
    
    if (diffY > 0) {
        viewHeight += diffY;
    }
    
    return viewHeight;
}

@end
