//
//  FaqL1ViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "DefaultTableViewController.h"
//#import "FaqCat.h"
#import "AFHTTPRequestOperationManager.h"
#import "CJSONDeserializer.h"
#import <QuartzCore/QuartzCore.h>
#import "FMDatabase.h"
#import "ManageData.h"
#import "LanguageUITableViewController.h"


@interface FaqL1ViewController : LanguageUITableViewController <UISearchBarDelegate, UISearchDisplayDelegate>
{
    NSArray *listContent;
    
    //search
    NSMutableArray *filteredListContent;
    //NSMutableArray *filterRowList;
}
@end
