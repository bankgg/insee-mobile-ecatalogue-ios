//
//  FaqL1ViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "FaqL1ViewController.h"
#import "FaqL1Box.h"
#import "FaqL2ViewController.h"
#import "AlignButton.h"
#import "UICommon.h"
#import "FaqAnswerBox.h"
#import "FaqHeaderBox.h"
#import "OptionViewController.h"
#import "NSString+Language.h"

@interface FaqL1ViewController ()

@end

@implementation FaqL1ViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.title = @"FAQs";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    float tableHeaderHeight = IS_PAD?20.0:10.0;
    
    //[self.tableView setTableHeaderView:[UICommon blankHeaderViewAtHeight:tableHeaderHeight]];
    [self.tableView setTableFooterView:[UICommon blankHeaderViewAtHeight:tableHeaderHeight]];
    
    [self loadData];
    
    if (filteredListContent == nil) {
        filteredListContent = [NSMutableArray array];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)languageLoad
{
    self.title = LocalizedString(@"faqs_title");
    [self.tableView reloadData];
}

#pragma mark - Option Button

- (void)optionPressed
{
    OptionViewController *optionVC = [[Global storyboardIphone] instantiateViewControllerWithIdentifier:@"OptionViewController"];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:optionVC];
    [self presentViewController:navVC animated:YES completion:nil];
}

#pragma mark - Load Data

- (void)loadData
{
    NSMutableArray *cats = [[NSMutableArray alloc] init];
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        NSString *sql = @"SELECT * FROM faq_cat ORDER BY code ASC";
        FMResultSet *s = [db executeQuery:sql];
        
        while ([s next])
        {
            NSDictionary *cat = [s resultDictionary];
            [cats addObject:cat];
        }
        
        listContent = [NSArray arrayWithArray:cats];
        
        [db close];
    }
}

-(void)reloadData
{
    [self loadData];
    [self.tableView reloadData];
    
    NSLog(@"reload");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int numSec = 0;
    
    if (tableView == self.tableView) {
        numSec = 1;
    }
    else
    {
        numSec = (int)[filteredListContent count];
        
        NSLog(@"filter num sec = %d", numSec);
    }
    
    return numSec;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numRow = 0;
    
    if (tableView == self.tableView)
    {
        numRow = (int)[listContent count];
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSDictionary *cat = [filteredListContent objectAtIndex:section];
        numRow = (int)[cat[@"rowList"] count];
    }
    
    return numRow;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    
    if (tableView == self.tableView)
    {
        NSDictionary *cCat = [listContent objectAtIndex:indexPath.row];
        
        FaqL1Box *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqL1Box"] owner:self options:nil]objectAtIndex:0];
        [box initialWithArrowType:arrowTypeRight];
        [box diffYFromSetTitle:cCat[[@"header" languageField]]];
        
        [cell.contentView addSubview:box];
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSDictionary *cat = [filteredListContent objectAtIndex:indexPath.section];
        
        RowIdentity *rId = (RowIdentity *)[cat[@"rowList"] objectAtIndex:indexPath.row];
        NSDictionary *aFaq = [cat[@"faqs"] objectAtIndex:rId.rowDataIndex];
        
        if (rId.rowType == rowTypeMain)
        {
            FaqL1Box *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqL1Box"] owner:self options:nil]objectAtIndex:0];
            [box initialWithArrowType:arrowTypeDown];
            [box diffYFromSetTitle:aFaq[[@"header" languageField]]];
            [cell.contentView addSubview:box];
            
            int nextRowIndex = (int)indexPath.row + 1;
            if (nextRowIndex < [cat[@"rowList"] count])
            {
                RowIdentity *nextRowId = [cat[@"rowList"] objectAtIndex:nextRowIndex];
                if (nextRowId.rowType == rowTypeSub)
                {
                    [box setArrowInitialAsUp];
                }
            }
        }
        else if (rId.rowType == rowTypeSub)
        {
            FaqAnswerBox *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqAnswerBox"] owner:self options:nil]objectAtIndex:0];
            [box initial];
            [box diffYFromSetText:aFaq[[@"detail" languageField]]];
            [cell.contentView addSubview:box];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0;
    
    if (tableView == self.tableView)
    {
        NSDictionary *cCat = [listContent objectAtIndex:indexPath.row];
        
        rowHeight = [FaqL1Box viewHeightFromTitle:cCat[@"header"]];
        
        if (indexPath.row + 1 == [listContent count]) {
            rowHeight += 2;
        }
    }
    else
    {
        NSDictionary *cat = [filteredListContent objectAtIndex:indexPath.section];
        
        RowIdentity *rId = (RowIdentity *)[cat[@"rowList"] objectAtIndex:indexPath.row];
        NSDictionary *aFaq = [cat[@"faqs"] objectAtIndex:rId.rowDataIndex];
        
        if (rId.rowType == rowTypeMain)
        {
            rowHeight = [FaqL1Box viewHeightFromTitle:aFaq[[@"header" languageField]]];
        }
        else if (rId.rowType == rowTypeSub)
        {
            rowHeight = [FaqAnswerBox viewHeightFromText:aFaq[[@"detail" languageField]]];
        }
        
        //last row
        if (indexPath.row + 1 == [cat[@"rowList"] count])
        {
            rowHeight += 1;
        }
    }
    
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == self.tableView)
    {
        NSDictionary *cat = [listContent objectAtIndex:indexPath.row];
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        FaqL2ViewController *faqL2VC = [sb instantiateViewControllerWithIdentifier:@"FaqL2ViewController"];
        faqL2VC.faqCatId = [cat[@"id"] intValue];
        faqL2VC.title = cat[[@"header" languageField]];
        
        [self.navigationController pushViewController:faqL2VC animated:YES];
    }
    else
    {
        NSDictionary *cat = [filteredListContent objectAtIndex:indexPath.section];
        
        RowIdentity *rId = (RowIdentity *)[cat[@"rowList"] objectAtIndex:indexPath.row];
        if (rId.rowType == rowTypeMain)
        {
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            FaqL1Box *box = (FaqL1Box *)[cell.contentView.subviews objectAtIndex:0];
            [box rotateArrow];
            
            //check if next row is answer or not
            int nextRowIndex = (int)indexPath.row + 1;
            
            NSLog(@"nextRowIndex = %d", nextRowIndex);
            
            bool willMakeNewRow = NO;
            
            if (nextRowIndex < [cat[@"rowList"] count])
            {
                RowIdentity *nextRowId = [cat[@"rowList"] objectAtIndex:nextRowIndex];
                if (nextRowId.rowType == rowTypeSub)
                {
                    willMakeNewRow = NO;
                }
                else if (nextRowId.rowType == rowTypeMain)
                {
                    willMakeNewRow = YES;
                }
            }
            else
            {
                willMakeNewRow = YES;
            }
            
            if (willMakeNewRow)
            {
                RowIdentity *newRowId = [[RowIdentity alloc] init];
                newRowId.rowType = rowTypeSub;
                newRowId.rowDataIndex = rId.rowDataIndex;
                
                [cat[@"rowList"] insertObject:newRowId atIndex:indexPath.row + 1];
                
                NSIndexPath *insertIndexPath = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section];
                [tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:insertIndexPath, nil] withRowAnimation:UITableViewRowAnimationMiddle];
            }
            else
            {
                [cat[@"rowList"] removeObjectAtIndex:nextRowIndex];
                NSIndexPath *delIndexPath = [NSIndexPath indexPathForRow:nextRowIndex inSection:indexPath.section];
                [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:delIndexPath, nil] withRowAnimation:UITableViewRowAnimationMiddle];
            }
            
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    float height = IS_PAD?20.0:10.0;
    
    if (tableView == self.tableView)
    {
        
    }
    else
    {
        NSDictionary *cat = [filteredListContent objectAtIndex:section];
        height = [FaqHeaderBox viewHeightFromTitle:cat[@"header"]];
        
        NSLog(@"filter header height = %f", height);
    }
    
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = nil;
    
    if (tableView == self.tableView) {
        headerView = [UICommon blankHeaderViewAtHeight:10];
    }
    else {
        
        NSDictionary *cat = [filteredListContent objectAtIndex:section];
        FaqHeaderBox *hBox = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqHeaderBox"] owner:self options:nil] objectAtIndex:0];
        [hBox initial];
        [hBox diffYFromSetTitle:cat[[@"header" languageField]]];
        headerView = hBox;
        NSLog(@"header View loaded");
    }
    
    return headerView;
}


#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSLog(@"search string = %@", searchString);
    
    NSString *searchText = [searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (searchText.length >= 2) {
        [self filterContentForSearchText:searchText];
    }
    else
    {
        [filteredListContent removeAllObjects];
    }
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    //search tableview
    tableView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:1.0];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    float searchTableHeaderHeight = IS_PAD?20.0:10.0;
    tableView.tableHeaderView = [UICommon blankHeaderViewAtHeight:searchTableHeaderHeight];
}

#pragma mark - Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText
{
    if (filteredListContent == nil) {
        filteredListContent = [NSMutableArray array];
    }
	[filteredListContent removeAllObjects]; // First clear the filtered array.
	
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        NSString *sql = [NSString stringWithFormat:@"SELECT f.id, f.faq_cat_id, f.header_th, f.header_en, f.detail_th, f.detail_en, \
                         c.header_th as cat_header_th, c.header_en as cat_header_en \
                         FROM faq f \
                         JOIN faq_cat c ON f.faq_cat_id = c.id \
                         WHERE f.header_th LIKE '%%%@%%' OR f.header_en LIKE '%%%@%%' \
                            OR f.keyword LIKE '%%%@%%' \
                            OR f.detail_th LIKE '%%%@%%' OR f.detail_th LIKE '%%%@%%' \
                         ORDER BY c.id ASC", searchText, searchText, searchText, searchText, searchText];
        FMResultSet *s = [db executeQuery:sql];
        
        NSMutableDictionary *lastRowCat = nil;
        while ([s next])
        {
            NSMutableDictionary *faq = [NSMutableDictionary dictionary];
            faq[@"id"] = [s stringForColumn:@"id"];
            faq[@"header_en"] = [s stringForColumn:@"header_en"];
            faq[@"header_th"] = [s stringForColumn:@"header_th"];
            faq[@"detail_en"] = [s stringForColumn:@"detail_en"];
            faq[@"detail_th"] = [s stringForColumn:@"detail_th"];
            
            if (![lastRowCat[@"id"] isEqualToString:[s stringForColumn:@"faq_cat_id"]])
            {
                //start new cat
                NSMutableDictionary *currentRowCat = [NSMutableDictionary dictionary];
                currentRowCat[@"id"] = [s stringForColumn:@"faq_cat_id"];
                currentRowCat[@"header_en"] = [s stringForColumn:@"cat_header_en"];
                currentRowCat[@"header_th"] = [s stringForColumn:@"cat_header_th"];
                currentRowCat[@"faqs"] = [NSMutableArray array];
                
                [currentRowCat[@"faqs"] addObject:faq];
                
                [filteredListContent addObject:currentRowCat];
                
                lastRowCat = currentRowCat;
            }
            else
            {
                [lastRowCat[@"faqs"] addObject:faq];
            }
            
            NSLog(@"search found = %@", faq[@"header_th"]);
        }
        
        [db close];
    }
    
    for (NSMutableDictionary *cat in filteredListContent)
    {
        cat[@"rowList"] = [NSMutableArray array];
        
        for (int i=0; i<[cat[@"faqs"] count]; i++)
        {
            RowIdentity *rId = [[RowIdentity alloc] init];
            rId.rowDataIndex = i;
            rId.rowType = rowTypeMain;
            [cat[@"rowList"] addObject:rId];
        }
    }
}

@end
