//
//  FaqL2ViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "Faq.h"
#import "RowIdentity.h"
#import "DefaultTableViewController.h"
#import "FMDatabase.h"
#import "ManageData.h"
#import "LanguageUITableViewController.h"

@interface FaqL2ViewController : LanguageUITableViewController
{
    NSMutableArray *rowList; //keep rowIndentity list (refer to index of faqList)
    NSMutableArray *faqList;
}

@property int faqCatId;

@end
