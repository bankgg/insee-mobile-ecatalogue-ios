//
//  FaqL2ViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "FaqL2ViewController.h"
#import "FaqL1Box.h"
#import "FaqAnswerBox.h"
#import "UICommon.h"
#import "Stat.h"
#import "NSString+Language.h"

@interface FaqL2ViewController ()

@end

@implementation FaqL2ViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.title = @"FAQs";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    float tableHeaderHeight = 10;
    if (IDIOM == IPAD) {
        tableHeaderHeight = 20;
    }
    [self.tableView setTableHeaderView:[UICommon blankHeaderViewAtHeight:tableHeaderHeight]];
    [self.tableView setTableFooterView:[UICommon blankHeaderViewAtHeight:tableHeaderHeight]];
    
    [self setData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)languageLoad
{
    [self.tableView reloadData];
}

#pragma mark - Data

-(void)setData
{
    faqList = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        NSString *sql = [NSString stringWithFormat:@"SELECT * \
                         FROM faq \
                         WHERE faq_cat_id = '%d' \
                         ORDER BY code ASC", self.faqCatId];
        FMResultSet *s = [db executeQuery:sql];
        
        while ([s next])
        {
            [faqList addObject:[s resultDictionary]];
        }

        [db close];
    }
    
    rowList = [[NSMutableArray alloc] init];
    for (int i=0; i<[faqList count]; i++)
    {
        RowIdentity *rId = [[RowIdentity alloc] init];
        rId.rowDataIndex = i;
        rId.rowType = rowTypeMain;
        [rowList addObject:rId];
    }
}

-(void)reloadData
{
    [self setData];
    [self.tableView reloadData];
    
    NSLog(@"reload");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2; //to make another section's header as footer of section 1; because there is animation issue from footer view - -''
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numRow = (int)[rowList count];
    if (section == 1) {
        numRow = 0;
    }
    
    return numRow;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if ([cell.contentView subviews]){
        for (UIView *subview in [cell.contentView subviews]) {
            [subview removeFromSuperview];
        }
    }
    
    RowIdentity *rId = (RowIdentity *)[rowList objectAtIndex:indexPath.row];
    NSDictionary *aFaq = [faqList objectAtIndex:rId.rowDataIndex];
    
    if (rId.rowType == rowTypeMain)
    {
        FaqL1Box *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqL1Box"] owner:self options:nil]objectAtIndex:0];
        [box initialWithArrowType:arrowTypeDown];
        [box diffYFromSetTitle:aFaq[[@"header" languageField]]];
        [cell.contentView addSubview:box];
        
        int nextRowIndex = (int)indexPath.row + 1;
        if (nextRowIndex < [rowList count])
        {
            RowIdentity *nextRowId = [rowList objectAtIndex:nextRowIndex];
            if (nextRowId.rowType == rowTypeSub)
            {
                [box setArrowInitialAsUp];
            }
        }
    }
    else if (rId.rowType == rowTypeSub)
    {
        FaqAnswerBox *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"FaqAnswerBox"] owner:self options:nil]objectAtIndex:0];
        [box initial];
        [box diffYFromSetText:aFaq[[@"detail" languageField]]];
        [cell.contentView addSubview:box];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.contentView.autoresizingMask = UIViewAutoresizingNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0;
    
    RowIdentity *rId = (RowIdentity *)[rowList objectAtIndex:indexPath.row];
    NSDictionary *aFaq = [faqList objectAtIndex:rId.rowDataIndex];
    
    if (rId.rowType == rowTypeMain)
    {
        //Fix iPad iOS 7, lost 1 pixel height
        float plus = 0;
        if (IDIOM == IPAD && [[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
        {
            int nextRowIndex = (int)indexPath.row + 1;
            
            if (nextRowIndex < [rowList count])
            {
                RowIdentity *nextRowId = (RowIdentity *)[rowList objectAtIndex:nextRowIndex];
                
                if (nextRowId.rowType == rowTypeSub) {
                    plus = 1;
                }
            }
        }
        
        rowHeight = [FaqL1Box viewHeightFromTitle:aFaq[[@"header" languageField]]] + plus;
    }
    else if (rId.rowType == rowTypeSub)
    {
        rowHeight = [FaqAnswerBox viewHeightFromText:aFaq[[@"detail" languageField]]];
    }
    
    //last row
    if (indexPath.row + 1 == [rowList count])
    {
        rowHeight += 1;
    }
    
    return rowHeight;
}

/*
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *blankView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 10)];
    blankView.backgroundColor = [UIColor clearColor];
    
    return blankView;
}
 */

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RowIdentity *rId = (RowIdentity *)[rowList objectAtIndex:indexPath.row];
    if (rId.rowType == rowTypeMain)
    {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        FaqL1Box *box = (FaqL1Box *)[cell.contentView.subviews objectAtIndex:0];
        [box rotateArrow];
        
        //check if next row is answer or not
        int nextRowIndex = (int)indexPath.row + 1;
        
        NSLog(@"nextRowIndex = %d", nextRowIndex);
        
        bool willMakeNewRow = NO;
        
        if (nextRowIndex < [rowList count])
        {
            RowIdentity *nextRowId = [rowList objectAtIndex:nextRowIndex];
            if (nextRowId.rowType == rowTypeSub)
            {
                willMakeNewRow = NO;
            }
            else if (nextRowId.rowType == rowTypeMain)
            {
                willMakeNewRow = YES;
            }
        }
        else
        {
            willMakeNewRow = YES;
        }
        
        if (willMakeNewRow)
        {
            RowIdentity *newRowId = [[RowIdentity alloc] init];
            newRowId.rowType = rowTypeSub;
            newRowId.rowDataIndex = rId.rowDataIndex;
            
            [rowList insertObject:newRowId atIndex:indexPath.row + 1];
            NSLog(@"--rowDataIndex = %d , numRow = %lu", rId.rowDataIndex, (unsigned long)[rowList count]);
            
            
            NSIndexPath *insertIndexPath = [NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:insertIndexPath, nil] withRowAnimation:UITableViewRowAnimationMiddle];
            
            //stat
            NSDictionary *sFaq = [faqList objectAtIndex:rId.rowDataIndex];
            [Stat saveStatOfPageType:pageTypeFaqsList pageName:self.title withAction:[NSString stringWithFormat:@"view \"%@\"", sFaq[[@"header_th" languageField]]]];
        }
        else
        {
            [rowList removeObjectAtIndex:nextRowIndex];
            NSIndexPath *delIndexPath = [NSIndexPath indexPathForRow:nextRowIndex inSection:indexPath.section];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:delIndexPath, nil] withRowAnimation:UITableViewRowAnimationMiddle];
        }
        
    }
}

@end
