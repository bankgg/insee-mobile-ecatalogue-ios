//
//  InfoWindowBox.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "InfoWindowBox.h"
#import "UICommon.h"
#import "UIView+Extension.h"

@implementation InfoWindowBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initial
{
    if (IDIOM != IPAD)
    {
//        self.labName.font = [UIFont fontWithName:@"INSEE" size:22.0f];
//        self.labDesc.font = [UIFont fontWithName:@"INSEE" size:14.0f];
    }
    else
    {
//        self.labName.font = [UIFont fontWithName:@"INSEE" size:30.0f];
//        self.labDesc.font = [UIFont fontWithName:@"INSEE" size:18.0f];
    }
    
    [self.labName sizeToFit2];
    [self.labDesc sizeToFit2];
    
    [self.labDesc setFramePosY:self.labName.posOfEndY + 0];
//    [self.frameBox setFrameHeight:self.labDesc.posOfEndY + 12];
    self.frameBox.frame = CGRectMake(self.frameBox.frame.origin.x, self.frameBox.frame.origin.y, self.frameBox.frame.size.width, self.labDesc.posOfEndY + 12);
    self.frame = self.frameBox.frame;
    
    [self.arrow setFrameCenterY:self.frame.size.height / 2];
    
    [UICommon setBorder:self.frameBox];
}

@end
