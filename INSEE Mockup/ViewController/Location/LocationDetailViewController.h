//
//  LocationDetailViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/21/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "Location.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "CoreLocation/CLLocationManager.h"
#import "CoreLocation/CLLocationManagerDelegate.h"
#import "LocationOperation.h"

@interface LocationDetailViewController : UITableViewController <MFMailComposeViewControllerDelegate>
{
    IBOutlet UILabel *labName;
    IBOutlet UILabel *labAddress;
    IBOutlet UIView *frameBoxView;
    IBOutlet UIImageView *imageViewMarker;
    
    NSMutableArray *operationArray;
}

@property (nonatomic, retain) NSDictionary *locationDict;
@property (nonatomic, retain) CLLocation *userLocation;

@end
