//
//  LocationDetailViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/21/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "LocationDetailViewController.h"
#import "UICommon.h"
#import "DocViewerViewController.h"
#import "Telephone.h"
#import "Stat.h"
#import "LocalizeHelper.h"
#import "NSString+Language.h"

@interface LocationDetailViewController ()

@end

@implementation LocationDetailViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.backgroundView = nil;
    
    //for ipad ios6 label position
    if (IS_PAD && [[[UIDevice currentDevice] systemVersion] floatValue] < 7)
    {
        float space = 35.0;
        
        labName.frame = CGRectMake(labName.frame.origin.x + space, labName.frame.origin.y, labName.frame.size.width - space, labName.frame.size.height);
        labAddress.frame = CGRectMake(labAddress.frame.origin.x + space, labAddress.frame.origin.y, labAddress.frame.size.width - space, labAddress.frame.size.height);
        CGRect markerFrame = imageViewMarker.frame;
        markerFrame.origin.x += space;
        imageViewMarker.frame = markerFrame;
    }
    
    labName.font = [UIFont fontWithName:@"INSEE" size:28.0f];
    labAddress.font = [UIFont fontWithName:@"INSEEText" size:18.0f];
    
    float diffY = 0;
    diffY += [UICommon diffHeightFromAdjustSizeLabel:labName byString:self.locationDict[[@"shop_name" languageField]]];
    [UICommon adjustPositionOfControl:labAddress byDiffHeight:diffY];
    diffY += [UICommon diffHeightFromAdjustSizeLabel:labAddress byString:self.locationDict[[@"shop_address" languageField]]];
    [UICommon adjustSizeOfContainer:frameBoxView byDiffHeight:diffY];
    [UICommon adjustSizeOfContainer:self.tableView.tableHeaderView byDiffHeight:diffY];
    [UICommon setBorder:frameBoxView];
    
    NSString *markerImageName = @"";
    switch ([self.locationDict[@"shop_type_id"] intValue]) {
        case 1000:
            markerImageName = @"locationMarkerType1";
            break;
        case 2000:
            markerImageName = @"locationMarkerType2";
            break;
        case 3000:
            markerImageName = @"locationMarkerType3";
            break;
        default:
            markerImageName = @"locationMarkerType3";
            break;
    }
    [imageViewMarker setImage:[UIImage imageNamed:markerImageName]];
    
    //Stat
    [Stat saveStatOfPageType:pageTypeLocationDetail pageName:self.locationDict[@"shop_name"] withAction:@"view"];
    
    
    operationArray = [NSMutableArray array];
    if (![self.locationDict[@"shop_telephone"] isEqualToString:@""])
    {
        //separate string if it contains comma , to multiple phone number
        
        if ([self.locationDict[@"shop_telephone"] rangeOfString:@","].location == NSNotFound)
        {
            [operationArray addObject:[LocationOperation initWithOperationType:OperationTypePhone andValue:self.locationDict[@"shop_telephone"] andLabelText:LocalizedString(@"loc_detail_tel")]];
        }
        else
        {
            NSArray *telNumbers = [self.locationDict[@"shop_telephone"] componentsSeparatedByString:@","];
            for (NSString *telNumber in telNumbers)
            {
                [operationArray addObject:[LocationOperation initWithOperationType:OperationTypePhone andValue:telNumber andLabelText:LocalizedString(@"loc_detail_tel")]];
            }
        }
    }
//    if (![self.locationDict[@"shop_mobile"] isEqualToString:@""])
//    {
//        //separate string if it contains comma , to multiple phone number
//        
//        if ([self.locationDict[@"shop_mobile"] rangeOfString:@","].location == NSNotFound)
//        {
//            [operationArray addObject:[LocationOperation initWithOperationType:OperationTypeMobile andValue:self.locationDict[@"shop_mobile"] andLabelText:LocalizedString(@"loc_detail_mobile")]];
//        }
//        else
//        {
//            NSArray *telNumbers = [self.locationDict[@"shop_mobile"] componentsSeparatedByString:@","];
//            for (NSString *telNumber in telNumbers)
//            {
//                [operationArray addObject:[LocationOperation initWithOperationType:OperationTypeMobile andValue:telNumber andLabelText:LocalizedString(@"loc_detail_mobile")]];
//            }
//        }
//    }
    if (![self.locationDict[@"shop_website"] isEqualToString:@""])
    {
        [operationArray addObject:[LocationOperation initWithOperationType:OperationTypeWeb andValue:self.locationDict[@"shop_website"] andLabelText:LocalizedString(@"loc_detail_web")]];
    }
    if (![self.locationDict[@"shop_email"] isEqualToString:@""])
    {
        [operationArray addObject:[LocationOperation initWithOperationType:OperationTypeMail andValue:self.locationDict[@"shop_email"] andLabelText:LocalizedString(@"loc_detail_email")]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 44;
    
    return rowHeight;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numRow = 0; //[super tableView:tableView numberOfRowsInSection:section];
    
    if (section == 0) {
        numRow = (int)[operationArray count];
    }
    else if (section == 1) {
        numRow = 2;
    }
    
    return numRow;
}

/*
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RightDetail";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }

    // Configure the cell...
    //UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 0)
    {
        NSLog(@"row = %ld", (long)indexPath.row);
        
        LocationOperation *opt = [operationArray objectAtIndex:indexPath.row];
        cell.detailTextLabel.text = opt.operationValue;
        cell.textLabel.text = opt.operationLabelText;
    }
    else if (indexPath.section == 1)
    {
        //cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
        if (indexPath.row == 0)
        {
            cell.textLabel.text = LocalizedString(@"loc_nav_google");
            cell.detailTextLabel.text = @"";
        }
        else if (indexPath.row == 1)
        {
            cell.textLabel.text = LocalizedString(@"loc_nav_apple");
            cell.detailTextLabel.text = @"";
        }
        
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont fontWithName:@"INSEE-Bold" size:22.0f];
    cell.detailTextLabel.font = [UIFont fontWithName:@"INSEE" size:20.0f];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    cell.indentationWidth = 0;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0)
    {
        NSString *actionName = @"";
        LocationOperation *opt = [operationArray objectAtIndex:indexPath.row];
        
        if (opt.operationType == OperationTypePhone)
        {
            //telephone
            //[Telephone makeCallFromPhoneNumberString:self.location.shop_telephone];
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [Telephone makeCallFromPhoneNumberString:cell.detailTextLabel.text];
            
            actionName = @"telephone";
        }
        else if (opt.operationType == OperationTypeMobile)
        {
            //mobile
            //[Telephone makeCallFromPhoneNumberString:self.location.shop_mobile];
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [Telephone makeCallFromPhoneNumberString:cell.detailTextLabel.text];
            
            actionName = @"mobile";
        }
        else if (opt.operationType == OperationTypeWeb)
        {
            //open website
            NSString *urlString = self.locationDict[@"shop_website"];
            //NSURL *url = [NSURL URLWithString:urlString];
            
            DocViewerViewController *vc = [[DocViewerViewController alloc] initWithNibName:@"DocViewerViewController" bundle:nil];
            vc.webURL = urlString;
            [self presentViewController:vc animated:YES completion:nil];
            
            actionName = @"website";
        }
        else if (opt.operationType == OperationTypeMail)
        {
            //email
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            [controller setToRecipients:[NSArray arrayWithObjects:self.locationDict[@"shop_email"], nil]];
            [controller setSubject:@"Sent from INSEE App"];
            [controller setMessageBody:@"" isHTML:NO];
            controller.modalPresentationStyle = UIModalPresentationFormSheet;
            [self presentViewController:controller animated:YES completion:nil];
            
            actionName = @"email";
        }
        
        //stat
        [Stat saveStatOfPageType:pageTypeLocationDetail pageName:self.locationDict[@"shop_name"] withAction:[NSString stringWithFormat:@"action %@", actionName]];
    }
    else if (indexPath.section == 1)
    {
        NSString *mapName = @"";
        if (indexPath.row == 0)
        {
            //navigate google map
            NSString *comgooglemaps = [NSString stringWithFormat:@"comgooglemaps://?daddr=%@,%@", self.locationDict[@"shop_latitude"], self.locationDict[@"shop_longtitude"]];
            NSURL *comgooglemapsURL = [NSURL URLWithString:comgooglemaps];
            if([[UIApplication sharedApplication] canOpenURL:comgooglemapsURL])
            {
                [[UIApplication sharedApplication] openURL:comgooglemapsURL];
            }
            else
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"App was not found" message:@"Please install Google Maps app to use this function." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            
            mapName = @"Google Maps";
        }
        else if (indexPath.row == 1)
        {
            //navigate apple map
            NSString *applemaps = [NSString stringWithFormat:@"http://maps.apple.com/?daddr=%@,%@", self.locationDict[@"shop_latitude"], self.locationDict[@"shop_longtitude"]];
            if (self.userLocation != nil) {
                applemaps = [NSString stringWithFormat:@"%@&saddr=%f,%f", applemaps, self.userLocation.coordinate.latitude, self.userLocation.coordinate.longitude];
            }
            NSLog(@"apple maps = %@", applemaps);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:applemaps]];
            
            mapName = @"Apple Maps";
        }
        
        //stat
        [Stat saveStatOfPageType:pageTypeLocationDetail pageName:self.locationDict[@"shop_name"] withAction:[NSString stringWithFormat:@"action navigate"]];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
