//
//  LocationMapViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "CoreLocation/CLLocationManager.h"
#import "CoreLocation/CLLocationManagerDelegate.h"
#import "MapKit/MapKit.h"
#import "AFHTTPRequestOperationManager.h"
#import "Location.h"
#import "JustAlert.h"

@interface LocationMapViewController : UIViewController <GMSMapViewDelegate, CLLocationManagerDelegate, UIActionSheetDelegate>
{
    IBOutlet UIView *gmapView;
    IBOutlet UIView *frameBox;
    
    bool isSetCameraToUserLocation;
    
    GMSMarker *selectMarker;
    
    NSMutableArray *listContent;
    
    int counter;
    int counter2;
    
    CLLocation *userLocation;
}

@property (nonatomic, retain) CLLocationManager *locationManager;

@property (nonatomic, retain) NSMutableArray *productCatSelectedIdArray;
-(void)reloadLocations;
@end
