//
//  LocationMapViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "LocationMapViewController.h"
#import "UICommon.h"
#import "InfoWindowBox.h"
#import "AlignButton.h"
#import "LocationDetailViewController.h"
#import "OptionViewController.h"
#import "LocationSearchViewController.h"
#import "LocationProductCatListViewController.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

#import "Stat.h"

// DEFINE
#define IS_DRAW_CIRCLE              NO
#define ZOOM_LEVEL_LOAD_MARKERS     10

@interface LocationMapViewController ()

@end

@implementation LocationMapViewController { 
    GMSMapView *mapView_;
    NSMutableArray *markers;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = LocalizedString(@"loc_title");
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    [UICommon setBorder:frameBox];
    
    [mapView_ setFrame:CGRectMake(0, 0, gmapView.frame.size.width, gmapView.bounds.size.height)];
    gmapView.backgroundColor = [UIColor redColor];
    
    //
    markers = [NSMutableArray array];
    
    //Location Manager
    //Starting the significant-change location service
    if (nil == self.locationManager)
        self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = 100.0;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startMonitoringSignificantLocationChanges];
    [self.locationManager startUpdatingLocation];
    
    
    
    //reload button
//    UIBarButtonItem *reloadButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconReload"] style:UIBarButtonItemStylePlain target:self action:@selector(loadLocationOfCenterMapView)];
//    
//    UIBarButtonItem *filterButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconFilter"] style:UIBarButtonItemStylePlain target:self action:nil];
//    
//    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"iconSearch"] style:UIBarButtonItemStylePlain target:self action:nil];
//    
//    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:filterButton, searchButton, nil]];
    

    
//    AlignButton *reloadBtn = [AlignButton buttonWithType:UIButtonTypeCustom];
//    reloadBtn.buttonSide = ButtonSideRight;
//    [reloadBtn setFrame:CGRectMake(0, 0, 30, 30)];
//    [reloadBtn setImage:[UIImage imageNamed:@"iconReload"] forState:UIControlStateNormal];
//    [reloadBtn addTarget:self action:@selector(loadLocationOfCenterMapView) forControlEvents:UIControlEventTouchUpInside];
    
    AlignButton *filterBtn = [AlignButton buttonWithType:UIButtonTypeCustom];
    filterBtn.buttonSide = ButtonSideRight;
    [filterBtn setFrame:CGRectMake(0, 0, 30, 30)];
    [filterBtn setImage:[UIImage imageNamed:@"iconFilter"] forState:UIControlStateNormal];
    [filterBtn addTarget:self action:@selector(filterButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    AlignButton *searchBtn = [AlignButton buttonWithType:UIButtonTypeCustom];
    searchBtn.buttonSide = ButtonSideRight;
    [searchBtn setFrame:CGRectMake(0, 0, 30, 30)];
    [searchBtn setImage:[UIImage imageNamed:@"iconSearch"] forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(navigateToSearch) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItems = @[[[UIBarButtonItem alloc] initWithCustomView:filterBtn], [[UIBarButtonItem alloc] initWithCustomView:searchBtn]];
    
    
    //Check location service enable
    if([CLLocationManager locationServicesEnabled] &&
       [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        NSLog(@"yes it's enabled");
    }
    else
    {
        [JustAlert alertWithTitle:@"Location Service is disabled" message:@"Turn on location service by \"Settings > General > Restrictions > Location Service\" to get your current location."];
    }
    
    
    //stat
    [Stat saveStatOfPageType:pageTypeLocationMap pageName:@"" withAction:@"view"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Filter Button

- (void)filterButtonPressed:(id)sender
{
    LocationProductCatListViewController *vc = [[Global storyboardIphone] instantiateViewControllerWithIdentifier:@"LocationProductCatListViewController"];
    vc.productCatSelectedIdArray = self.productCatSelectedIdArray;
    vc.LocationMapVC = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Load Data

- (void)loadView
{
    [super loadView];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:mapView_.myLocation.coordinate zoom:12];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    mapView_.myLocationEnabled = YES;
    mapView_.accessibilityElementsHidden = NO;
    mapView_.settings.compassButton = YES;
    mapView_.settings.myLocationButton = YES;
    mapView_.delegate = self;
    mapView_.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [mapView_ setContentMode:UIViewContentModeScaleAspectFill];
    
    //self.view = mapView_;
    [gmapView addSubview:mapView_];
}

#pragma mark - CLLocationManagerDelegate
// Delegate method from the CLLocationManagerDelegate protocol.
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // If it's a relatively recent event, turn off updates to save power
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    if (fabs(howRecent) < 15.0) {
        // If the event is recent, do something with it.
        NSLog(@"latitude %+.6f, longitude %+.6f\n",
              location.coordinate.latitude,
              location.coordinate.longitude);
        
        GMSCameraUpdate *camera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];
        [mapView_ animateWithCameraUpdate:camera];
        
        [self loadLocationAtCenter:CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)];
        userLocation = location;
        
        [self.locationManager stopUpdatingLocation];
    }
    else
    {
        
    }
    
    //mapView_.camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude zoom:mapView_.camera.zoom];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"location error = %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusAuthorized)
    {
        [self.locationManager startUpdatingLocation];
    }
    else if (status == kCLAuthorizationStatusDenied)
    {
        //[JustAlert alertWithTitle:@"Location Service is disabled" message:@"Turn on location service by \"Settings > General > Restrictions > Location Service\" to get your current location."];
    }
}

#pragma mark - GoogleMap API Delegate
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    if (marker.userData != nil)
    {
        NSDictionary *dict = marker.userData;
        
        InfoWindowBox *infoWindow = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"InfoWindowBox"] owner:self options:nil] objectAtIndex:0];
        infoWindow.labName.text = dict[[@"shop_name" languageField]];
        infoWindow.labDesc.text = dict[[@"shop_address" languageField]];
        [infoWindow initial];
        
        return infoWindow;
    }
    
    return nil;
}

- (void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
    selectMarker = marker;
    
    LocationDetailViewController *subVC = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LocationDetailViewController"];
    subVC.locationDict = marker.userData;
    subVC.userLocation = mapView.myLocation;
    [self.navigationController pushViewController:subVC animated:YES];
    
    /*
    UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Call 02-xxx-xxxx", @"Navigate by Google Map", @"Navigate by Map App", nil];
    [actSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actSheet showInView:[UIApplication sharedApplication].keyWindow];
    */
}

#pragma mark - LOAD LOCATION

-(void)loadLocationAtCenter:(CLLocationCoordinate2D)pointCenter
{
    if (mapView_.camera.zoom < ZOOM_LEVEL_LOAD_MARKERS)
    {
        [mapView_ clear];
        listContent = nil;
        return;
    }
    
    //CLLocationCoordinate2D pointCenter = mapView_.camera.target;
    CLLocationCoordinate2D pointA = mapView_.projection.visibleRegion.farLeft;
    CLLocationCoordinate2D pointB = mapView_.projection.visibleRegion.nearRight;
    
    //Get Radius
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:pointA.latitude longitude:pointA.longitude];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:pointB.latitude longitude:pointB.longitude];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    
    NSLog(@"distance = %f", distance);
    float radius = distance / 2.0;
    float radiusKm = radius / 1000.0;
    
    if (IS_DRAW_CIRCLE)
    {
        //Draw Circle
        GMSCircle *circ = [GMSCircle circleWithPosition:pointCenter radius:radius];
        circ.fillColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.05];
        circ.strokeColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.25];
        circ.strokeWidth = 2.0f;
        circ.map = mapView_;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *cat_ids = @"";
    for (NSString *cat_id in self.productCatSelectedIdArray)
    {
        if ([cat_ids isEqualToString:@""]) {
            cat_ids = cat_id;
        }
        else {
            cat_ids = [NSString stringWithFormat:@"%@,%@", cat_ids, cat_id];
        }
    }
    
    NSString *locListURLString = [NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_LOCATION_LIST2], pointCenter.latitude, pointCenter.longitude, radiusKm, cat_ids];
    NSLog(@"locList url = %@", locListURLString);
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:locListURLString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         if (!listContent) {
             listContent = [NSMutableArray array];
         }
         
         NSArray *newList = [NSArray arrayWithArray:responseObject];
         
         // ADD
         for (NSDictionary *nDict in newList)
         {
             BOOL isNew = YES;
             for (NSDictionary *cDict in listContent)
             {
                 if ([nDict[@"id"] intValue] == [cDict[@"id"] intValue])
                 {
                     isNew = NO;
                     break;
                 }
             }
             
             if (isNew)
             {
                 [self addMarkerFromDict:nDict];
                 [listContent addObject:nDict];
             }
         }
         
         // REMOVE
         NSMutableArray *removeList = [NSMutableArray array];
         
         for (NSDictionary *cDict in listContent)
         {
             BOOL isOut = YES;
             for (NSDictionary *nDict in newList)
             {
                 if ([cDict[@"id"] intValue] == [nDict[@"id"] intValue])
                 {
                     isOut = NO;
                     break;
                 }
             }
             
             if (isOut)
             {
                 [removeList addObject:cDict];
                 [self removeMarkerFromDict:cDict];
             }
         }
         
         [listContent removeObjectsInArray:removeList];
         
         
         
         
         
         
         
         
//         listContent = [NSMutableArray array];
//         listContent = [NSMutableArray arrayWithArray:responseObject];
//         
//         //Clear before adding anything
//         [mapView_ clear];
//         
//         [self AddMarkers];
         
         
         
         
         
         
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
     }];
    
}

-(void)loadLocationOfCenterMapView
{
    [self loadLocationAtCenter:mapView_.camera.target];
}


-(void)reloadLocations
{
    [mapView_ clear];
    listContent = nil;
    
    [self loadLocationOfCenterMapView];
}




-(void)AddMarkers
{
    if (listContent != nil)
    {
        for (NSDictionary *dict in listContent)
        {
            [self addMarkerFromDict:dict];
        }
    }
}

//

-(void)addMarkerFromDict:(NSDictionary *)dict
{
    CLLocationCoordinate2D pos = CLLocationCoordinate2DMake([dict[@"shop_latitude"] floatValue], [dict[@"shop_longtitude"] floatValue]);
    NSString *imgName = @"";
    if ([dict[@"shop_type_id"] intValue] == 1000) {
        imgName = @"locationMarkerType1";
    }
    else if ([dict[@"shop_type_id"] intValue] == 2000) {
        imgName = @"locationMarkerType2";
    }
    else { //if (loc.shop_type_id == 3000) {
        imgName = @"locationMarkerType3";
    }
    
    GMSMarker *aMarker = [GMSMarker markerWithPosition:pos];
    aMarker.icon = [UIImage imageNamed:imgName];
    aMarker.groundAnchor = CGPointMake(0.5f, 0.95f);
    aMarker.zIndex = 2;
    aMarker.infoWindowAnchor = CGPointMake(0.5f, -0.1f);
    aMarker.appearAnimation = kGMSMarkerAnimationPop;
    aMarker.map = mapView_;
    
    aMarker.userData = dict;
    
    [markers addObject:aMarker];
}

-(void)removeMarkerFromDict:(NSDictionary *)dict
{
    for (GMSMarker *marker in markers)
    {
        NSDictionary *mDict = marker.userData;
        if ([mDict[@"id"] intValue] == [dict[@"id"] intValue])
        {
            marker.map = nil;
            [markers removeObject:marker];
            break;
        }
    }
}

#pragma mark - MapView Delegate

- (void) mapView: 		(GMSMapView *)  	mapView
idleAtCameraPosition: 		(GMSCameraPosition *)  	position
{
    [self loadLocationOfCenterMapView];
    
    NSLog(@"CAMERA ZOOM LEVEL = %f", mapView_.camera.zoom);
}

#pragma mark - ActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSString *comgooglemaps = [NSString stringWithFormat:@"comgooglemaps://?daddr=%f,%f", selectMarker.position.latitude, selectMarker.position.longitude];
        NSURL *comgooglemapsURL = [NSURL URLWithString:comgooglemaps];
        if([[UIApplication sharedApplication] canOpenURL:comgooglemapsURL])
        {
            [[UIApplication sharedApplication] openURL:comgooglemapsURL];
        }
        else
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"App was not found" message:@"Please install Google Maps app to use this function." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
    }
    else if (buttonIndex == 2)
    {
        NSString *applemaps = [NSString stringWithFormat:@"http://maps.apple.com/?saddr=%f,%f&daddr=%f,%f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude, selectMarker.position.latitude, selectMarker.position.longitude];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:applemaps]];
    }
}

#pragma mark - Search

- (void)navigateToSearch
{
    LocationSearchViewController *vc = [[Global storyboardIphone] instantiateViewControllerWithIdentifier:@"LocationSearchViewController"];
    vc.userLocation = mapView_.myLocation;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
