//
//  LocationOperation.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 1/9/2557 BE.
//  Copyright (c) 2557 iAppGarage. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    OperationTypePhone   = 1000,
    OperationTypeMobile  = 1500,
    OperationTypeMail    = 2000,
    OperationTypeWeb     = 3000
} OperationType;

@interface LocationOperation : NSObject

@property OperationType operationType;
@property (nonatomic, retain) NSString *operationValue;
@property (nonatomic, retain) NSString *operationLabelText;

+(id)initWithOperationType:(OperationType)optType andValue:(NSString *)optValue andLabelText:(NSString *)labelText;

@end
