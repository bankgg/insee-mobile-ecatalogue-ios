//
//  LocationOperation.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 1/9/2557 BE.
//  Copyright (c) 2557 iAppGarage. All rights reserved.
//

#import "LocationOperation.h"

@implementation LocationOperation

+(id)initWithOperationType:(OperationType)optType andValue:(NSString *)optValue andLabelText:(NSString *)labelText
{
    LocationOperation *locOpt = [[LocationOperation alloc] init];
    locOpt.operationType = optType;
    locOpt.operationValue = optValue;
    locOpt.operationLabelText = labelText;
    
    return locOpt;
}

@end
