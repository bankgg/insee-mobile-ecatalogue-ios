//
//  LocationProductCatListViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/19/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationProductCatListViewController : UITableViewController

@property (nonatomic, retain) NSMutableArray *productCatSelectedIdArray;

@property (nonatomic, retain) id LocationMapVC;

@end
