//
//  LocationProductCatListViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/19/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "LocationProductCatListViewController.h"
#import "LocationMapViewController.h"

#import "Global.h"
#import "AFHTTPRequestOperationManager.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

@interface LocationProductCatListViewController ()
{
    NSMutableArray *listContent;
}
@end

@implementation LocationProductCatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LocalizedString(@"loc_filter_title");
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    if (!self.productCatSelectedIdArray) {
        self.productCatSelectedIdArray = [NSMutableArray array];
    }
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    LocationMapViewController *parentVC = self.LocationMapVC;
    parentVC.productCatSelectedIdArray = self.productCatSelectedIdArray;
    [parentVC reloadLocations];
}

- (void)loadData
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *servicePath = [Global PATH_OF:S_PATH_SERVICE_LOCATION_PRODUCT_CAT_LIST];
    NSLog(@"servicePath = %@", servicePath);
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:servicePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         if (listContent == nil) {
             listContent = [NSMutableArray array];
         }
         
         listContent = [NSMutableArray arrayWithArray:responseObject];
         
         [self.tableView reloadData];
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return listContent.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (indexPath.section == 0)
    {
        cell.textLabel.text = LocalizedString(@"loc_filter_any_cat");
        
        if (self.productCatSelectedIdArray.count == 0)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    else
    {
        NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
        cell.textLabel.text = dict[[@"cat_name" languageField]];
        
        BOOL isInList = NO;
        for (NSString *s in self.productCatSelectedIdArray)
        {
            if ([s isEqualToString:dict[@"id"]])
            {
                isInList = YES;
                break;
            }
        }
        
        if (isInList) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    if (indexPath.section == 0)
    {
        // Any Item
        self.productCatSelectedIdArray = [NSMutableArray array];
        [self.tableView reloadData];
    }
    else
    {
        BOOL isInList = NO;
        for (NSString *s in self.productCatSelectedIdArray)
        {
            if ([s isEqualToString:dict[@"id"]])
            {
                isInList = YES;
                [self.productCatSelectedIdArray removeObject:s];
                break;
            }
        }
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        if (!isInList)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [self.productCatSelectedIdArray addObject:dict[@"id"]];
            
            //uncheck the first row
            UITableViewCell *firstCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            firstCell.accessoryType = UITableViewCellAccessoryNone;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
