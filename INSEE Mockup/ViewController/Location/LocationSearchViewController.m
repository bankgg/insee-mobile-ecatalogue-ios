//
//  LocationSearchViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/12/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "LocationSearchViewController.h"

#import "CoreLocation/CLLocationManager.h"
#import "CoreLocation/CLLocationManagerDelegate.h"
#import "AFHTTPRequestOperationManager.h"
#import "CJSONDeserializer.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "JustAlert.h"
#import "UIView+Extension.h"

#import "Global.h"
#import "LocalizeHelper.h"
#import "NSString+Language.h"

#import "LocationDetailViewController.h"

#define NUM_ROW_LOAD 10

@interface LocationSearchViewController ()
{
    NSMutableArray *listContent;
    
    int offset;
    bool isLoading;
    bool isMoreData;
    
    Reachability *reach;
    
    UIRefreshControl *refreshControl;
    
    NSString *searchText;
}
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@end

@implementation LocationSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LocalizedString(@"loc_search_title");
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    listContent = [NSMutableArray array];
    
    [self.searchBar becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load Data

-(void)loadData
{
    if (isLoading) {
        return;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    isLoading = YES;
    
    NSString *servicePath = [NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_LOCATION_SEARCH], self.userLocation.coordinate.latitude, self.userLocation.coordinate.longitude, offset, NUM_ROW_LOAD, searchText];
    NSString *escapedUrlString = [servicePath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"servicePath = %@", escapedUrlString);
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:escapedUrlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSArray *responseArray = (NSArray *)responseObject;
         for (NSDictionary *dict in responseArray)
         {
             NSMutableDictionary *dict_m = [dict mutableCopy];
             
             //Row Height
             UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
             UILabel *labelTitle = (id)[cell viewWithTag:1000];
             labelTitle.text = dict[[@"shop_name" languageField]];
             [labelTitle sizeToFit2];
             
             UILabel *labelAddress = (id)[cell viewWithTag:2000];
             labelAddress.text = dict[[@"shop_address" languageField]];
             [labelAddress sizeToFit2];
             [labelAddress setFramePosY:labelTitle.posOfEndY + 0];
             
             //Distance
             UILabel *labelDistance = (id)[cell viewWithTag:3000];
             [labelDistance setFramePosY:labelAddress.posOfEndY + 0];
             
             float row_height = labelDistance.posOfEndY + 5;
             
             [dict_m setObject:[NSNumber numberWithFloat:row_height] forKey:@"row_height"];
             
             [listContent addObject:dict_m];
         }
         if ([responseArray count] == NUM_ROW_LOAD) {
             isMoreData = YES;
         }
         else {
             isMoreData = NO;
         }
         if ([responseArray count] > 0) {
             offset += NUM_ROW_LOAD;
         }
         
         [self.tableView reloadData];
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [refreshControl endRefreshing];
         isLoading = NO;
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
         
         listContent = [NSMutableArray array];
         [self.tableView reloadData];
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [refreshControl endRefreshing];
         isLoading = NO;
     }];
}

-(void)reloadData
{
    //clear index and array
    offset = 0;
    listContent = nil;
    listContent = [NSMutableArray array];
    
    [self loadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return listContent.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary *dict = [listContent objectAtIndex:indexPath.section];
    
    UIView *viewFrame = (id)[cell viewWithTag:10000];
    [viewFrame makeBorder];
    
    UILabel *labelTitle = (id)[cell viewWithTag:1000];
    labelTitle.text = dict[[@"shop_name" languageField]];
    [labelTitle sizeToFit2];
    
    UILabel *labelAddress = (id)[cell viewWithTag:2000];
    labelAddress.text = dict[[@"shop_address" languageField]];
    [labelAddress sizeToFit2];
    [labelAddress setFramePosY:labelTitle.posOfEndY + 0];
    
    //Distance
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *nDistance = [f numberFromString:dict[@"distance"]];
    
    UILabel *labelDistance = (id)[cell viewWithTag:3000];
    labelDistance.text = [NSString stringWithFormat:@"%.02f %@", [nDistance floatValue], LocalizedString(@"loc_search_distance_unit")];
    [labelDistance setFramePosY:labelAddress.posOfEndY + 0];
    
//    [viewFrame setFrameHeight:labelDistance.posOfEndY + 5];
    viewFrame.frame = CGRectMake(viewFrame.frame.origin.x, viewFrame.frame.origin.y, viewFrame.frame.size.width, labelDistance.posOfEndY + 5);
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [listContent objectAtIndex:indexPath.section];
    
    return [dict[@"row_height"] floatValue];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor clearColor];
    return v;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [listContent objectAtIndex:indexPath.section];
    
    LocationDetailViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LocationDetailViewController"];
    vc.locationDict = dict;
    vc.userLocation = self.userLocation;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Search Bar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)_searchText
{
    NSLog(@"searchText = %@", _searchText);
    
    searchText = [_searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (searchText.length >= 3)
    {
        [self reloadData];
    }
    else
    {
        if (listContent.count > 0)
        {
            listContent = [NSMutableArray array];
            [self.tableView reloadData];
        }
    }
}

//check position of scroll of tableview to load more data
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= 40)
    {
        if (!isLoading && isMoreData)
        {
            //offset += NUM_ROW_LOAD;
            [self loadData];
            NSLog(@"load more news");
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self dismissKeyboard];
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
}

@end
