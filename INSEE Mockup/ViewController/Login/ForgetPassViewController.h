//
//  ForgetPassViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/10/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "User.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "AFHTTPRequestOperationManager.h"
#import "JustAlert.h"

@interface ForgetPassViewController : UITableViewController <UIAlertViewDelegate>
{
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UILabel *labEmail;
    __weak IBOutlet UIButton *btnSend;
}

-(IBAction)buttonSendPressed:(id)sender;

@end
