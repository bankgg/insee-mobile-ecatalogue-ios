//
//  ForgetPassViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/10/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ForgetPassViewController.h"
#import "UICommon.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

@interface ForgetPassViewController ()

@end

@implementation ForgetPassViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = LocalizedString(@"forgot_title");
    labEmail.text = LocalizedString(@"forgot_lab_email");
    [btnSend setTitle:LocalizedString(@"forgot_btn_send") forState:UIControlStateNormal];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 10.0f)];

    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backToParent)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    [UICommon setFontButton:btnSend];
    [UICommon setShadow:btnSend];
    [UICommon setTextField:tfEmail];
    
    UIFont *labelFont = [UIFont fontWithName:@"INSEE-Bold" size:20.0f];
    labEmail.font = labelFont;
    labEmail.textColor = [UIColor colorWithWhite:0.15 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)backToParent
{
    if ([self respondsToSelector:@selector(presentingViewController)]){
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(IBAction)buttonSendPressed:(id)sender
{
    tfEmail.text = [tfEmail.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    [tfEmail resignFirstResponder];
    
    if ([self isSwitchingWord]) {
        return;
    }
    
    if ([self validatePass])
    {
        //SHOW LOADING
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading";
        hud.dimBackground = YES;
        
        NSString *serviceString = [NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_FORGOT_PASSWORD_REQUEST], tfEmail.text];
        
        AFHTTPRequestOperationManager *manager = [Global afManager];
        [manager GET:serviceString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             [hud hide:YES];
             NSLog(@"JSON: %@", responseObject);
             
             if ([responseObject objectForKey:@"error"])
             {
                 [JustAlert alertWithMessage:[responseObject objectForKey:@"error"]];
             }
             else if ([responseObject objectForKey:@"success"])
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[responseObject objectForKey:@"success"] message:[responseObject objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                 [alert show];
                 //[JustAlert alertWithTitle:[responseObject objectForKey:@"success"] message:[responseObject objectForKey:@"message"]];
             }
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             [hud hide:YES];
             NSLog(@"Error1: %@", error);
             [JustAlert alertWithMessage:[NSString stringWithFormat:@"%@",error]];
         }];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self backToParent];
    }
}

#pragma mark - Validate

-(bool) validatePass
{
    bool isPass = NO;
    
    if ([tfEmail.text length] > 0)
    {
        isPass = YES;
    }
    else
    {
        [JustAlert alertWithMessage:LocalizedString(@"forgot_alert_email")];
    }
    
    return isPass;
}

#pragma mark - PRODUCTION - PREPRODUCTION MODE SWITCHER

-(bool) isSwitchingWord
{
    bool ret = NO;
    
    if ([tfEmail.text isEqualToString:MODE_PREPROD_KEYWORD])
    {
        [Global SET_PRE_PRODUCTION_MODE];
        ret = YES;
        [JustAlert alertWithTitle:@"PRE-PRODUCTION MODE ACTIVATED" message:@"Please close the application and open again to reload data."];
    }
    else if ([tfEmail.text isEqualToString:MODE_PROD_KEYWORD])
    {
        [Global SET_PRODUCTION_MODE];
        ret = YES;
        [JustAlert alertWithTitle:@"PRODUCTION MODE ACTIVATED" message:@"Please close the application and open again to reload data."];
    }
    
    return ret;
}


@end
