//
//  LoginViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/9/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "AFHTTPRequestOperationManager.h"
#import "User.h"
#import "MBProgressHUD.h"
#import "URLOpener.h"

@interface LoginViewController : UIViewController <UIScrollViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate>
{
    IBOutlet UIButton *btnLogin;
    IBOutlet UIButton *btnRegister;
    IBOutlet UIImageView *imageViewFillBox;
    IBOutlet UIScrollView *scrollView;
    IBOutlet UITextField *tfUser, *tfPass;
    
    IBOutlet UIButton *btnForget;
    IBOutlet UIButton *btnSkip;
    IBOutlet UILabel *labNewUser;
    IBOutlet UIButton *btnClubCard;
    
    CGPoint scrollViewOffset;
}

- (IBAction)loginButtonPressed:(id)sender;
- (IBAction)skipButtonPressed:(id)sender;
- (IBAction)forgetButtonPressed:(id)sender;
- (IBAction)registerButtonPressed:(id)sender;
- (IBAction)clubcardButtonPressed:(id)sender;

@end
