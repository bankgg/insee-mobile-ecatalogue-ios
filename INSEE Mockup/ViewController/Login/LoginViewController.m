//
//  LoginViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/9/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "LoginViewController.h"
#import "UICommon.h"
#import "AppDelegate.h"
#import "ForgetPassViewController.h"
#import "RegisterViewController.h"
#import "SVWebViewController.h"

#import "LocalizeHelper.h"

#import <Parse/Parse.h>

@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    tfUser.delegate = self;
    tfPass.delegate = self;
	
    //Shadow
    [UICommon setShadows:[NSArray arrayWithObjects:btnLogin, btnRegister, imageViewFillBox, nil]];
    
    scrollViewOffset = scrollView.contentOffset;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    
    labNewUser.text = LocalizedString(@"login_new_user");
    [btnLogin setTitle:LocalizedString(@"login_sign_in") forState:UIControlStateNormal];
    [btnRegister setTitle:LocalizedString(@"login_register") forState:UIControlStateNormal];
    [btnForget setTitle:LocalizedString(@"login_forgot_password") forState:UIControlStateNormal];
    [btnSkip setTitle:LocalizedString(@"login_skip_sign_in") forState:UIControlStateNormal];
    [btnClubCard setTitle:LocalizedString(@"login_club_card") forState:UIControlStateNormal];
    
    btnSkip.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnForget.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    
    //INSEE CLUB CARD BUTTON
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"login_club_card")];
    
    NSRange rangeOfRed = [LocalizedString(@"login_club_card") rangeOfString:LocalizedString(@"login_club_card_red")];
    NSRange rangeOfGray = NSMakeRange(0, rangeOfRed.location);
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:rangeOfGray];
    [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:rangeOfRed];
    [btnClubCard setAttributedTitle:attrStr forState:UIControlStateNormal];
    
    
    NSMutableAttributedString *attrStrHighlight = [[NSMutableAttributedString alloc] initWithString:LocalizedString(@"login_club_card")];
    [attrStrHighlight addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [LocalizedString(@"login_club_card") length])];
    [btnClubCard setAttributedTitle:attrStrHighlight forState:UIControlStateHighlighted];
    
    
    
    
    
    //Test
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    NSLog(@"PARSE currentInstallation = %@", currentInstallation.channels);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)returnToParent
{
    if ([self respondsToSelector:@selector(presentingViewController)]){
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)login
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading";
    hud.dimBackground = YES;
    
    //Set Parameters
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:tfUser.text forKey:@"user_email"];
    [parameters setValue:tfPass.text forKey:@"user_password"];
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager POST:[Global PATH_OF:S_PATH_SERVICE_LOGIN] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        if ([responseObject objectForKey:@"error"])
        {
            [hud hide:YES];
            
            //login failed
            NSString *erMessage = [responseObject objectForKey:@"error"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:erMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            //login finished
            User *sUser = [User objFromDict:responseObject];
            [User saveUserData:sUser];
            
            hud.mode = MBProgressHUDModeText;
            hud.labelText = @"Login Successful";
            [hud hide:YES afterDelay:1.5];
            
            [self performSelector:@selector(returnToParent) withObject:nil afterDelay:1.8];
            //[self returnToParent];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Fail to make a database connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [hud hide:YES];
    }];
}

#pragma mark - Buttons
-(IBAction)loginButtonPressed:(id)sender
{
    if ([self validatePass])
    {
        [self dismissKeyboard];
        [self login];
    }
}

-(IBAction)skipButtonPressed:(id)sender
{
    [self returnToParent];
}

-(IBAction)forgetButtonPressed:(id)sender
{
    ForgetPassViewController *vc = [[Global storyboardIphone] instantiateViewControllerWithIdentifier:@"ForgetPassViewController"];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:nil];
}

-(IBAction)registerButtonPressed:(id)sender
{
    RegisterViewController *vc = [[Global storyboardIphone] instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:vc];
    [self presentViewController:navVC animated:YES completion:nil];
}

- (IBAction)clubcardButtonPressed:(id)sender
{
    NSString *urlPath = [Global PATH_OF:URL_INSEE_CLUBCARD];
    NSString *userAgent = BROWSER_CHROME;
    URLOpener *opener = [[URLOpener alloc] initWithURLString:urlPath browser:userAgent];
    [opener openURL];
}

#pragma mark - Keyboard

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [scrollView setContentOffset:scrollViewOffset];
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    CGPoint nPoint = CGPointMake(0, 100);
    if ([AppDelegate hasFourInchDisplay])
    {
        nPoint = CGPointMake(0, 30);
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [scrollView setContentOffset:nPoint];
    [UIView commitAnimations];
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == tfUser) {
        [tfPass becomeFirstResponder];
    }
    else {
        [self dismissKeyboard];
        [self login];
    }
    
    return YES;
}

#pragma mark - Validate

-(bool) validatePass
{
    bool isPass = NO;
    
    if ([tfUser.text length] > 0 && [tfPass.text length] > 0)
    {
        isPass = YES;
    }
    else
    {
        [JustAlert alertWithMessage:@"Please fill email and password"];
    }
    
    return isPass;
}

@end
