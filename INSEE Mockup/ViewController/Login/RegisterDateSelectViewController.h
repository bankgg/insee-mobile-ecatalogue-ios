//
//  RegisterDateSelectViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#define YEAR_BACKWARD 18

@interface RegisterDateSelectViewController : UITableViewController
{
    UIDatePicker *datePicker;
    
    IBOutlet UILabel *lab1;
    IBOutlet UITextField *tf1;
}

@property (nonatomic, retain) NSDate *selectDate;
@property id delegate;

@end
