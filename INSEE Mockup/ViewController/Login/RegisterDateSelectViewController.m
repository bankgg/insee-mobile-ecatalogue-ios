//
//  RegisterDateSelectViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "RegisterDateSelectViewController.h"
#import "UICommon.h"
#import "RegisterViewController.h"

@interface RegisterDateSelectViewController ()

@end

@implementation RegisterDateSelectViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    UIFont *labelFont = [UIFont fontWithName:@"INSEE-Bold" size:20.0f];
    lab1.font = labelFont;
    [UICommon setTextField:tf1];

    [self setupDatePicker];
    [self updateTextFieldByDate:self.selectDate];
    
    self.tableView.scrollEnabled = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillDisappear:(BOOL)animated
{
    RegisterViewController *rVC = (RegisterViewController *)self.delegate;
    [rVC updateSelectDateValue:self.selectDate];
    
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    float datePickerHeight = 260;
    if (datePicker) {
        [datePicker setFrame:CGRectMake(0, self.view.frame.size.height - datePickerHeight, self.view.frame.size.width, datePickerHeight)];
    }
}

-(void)setupDatePicker
{
    float datePickerHeight = 260;
    datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - datePickerHeight, self.view.frame.size.width, datePickerHeight)];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.backgroundColor = [UIColor whiteColor];
    [UICommon setBorder:datePicker];
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:datePicker];
    
    if (self.selectDate != nil) {
        datePicker.date = self.selectDate;
    }
    else {
        NSDate *currentDate = [NSDate date];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
        [components setYear:[components year] - YEAR_BACKWARD];
        datePicker.date = [calendar dateFromComponents:components];
    }
}

-(void)datePickerValueChanged:(id)sender
{
    self.selectDate = datePicker.date;
    [self updateTextFieldByDate:self.selectDate];
}

-(void)updateTextFieldByDate:(NSDate *)_date
{
    if (_date != nil)
    {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        df.dateStyle = NSDateFormatterMediumStyle;
//        [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        tf1.text = [df stringFromDate:_date];
    }
    else
    {
        self.selectDate = datePicker.date;
        [self updateTextFieldByDate:datePicker.date];
    }
}


@end
