//
//  RegisterSelectViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/10/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Type.h"

typedef enum {
    selectTypeSex,
    selectTypeRole,
    selectTypeType
} SelectType;

@interface RegisterSelectViewController : UITableViewController
{

}

@property (nonatomic, retain) NSArray *selectArray;
@property int selectId;
@property id delegate;
@property SelectType selectType;

@end
