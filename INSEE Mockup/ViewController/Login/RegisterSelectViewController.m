//
//  RegisterSelectViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/10/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "RegisterSelectViewController.h"
#import "RegisterViewController.h"

@interface RegisterSelectViewController ()

@end

@implementation RegisterSelectViewController

@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    self.tableView.backgroundView = nil;
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableView.bounds.size.width, 10.0f)];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.selectArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    Type *_type = [self.selectArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = _type.type_name;
    cell.textLabel.font = [UIFont fontWithName:@"INSEE" size:20.0f];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    if (_type.objId == self.selectId) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //clear cell acc
    for (int i=0; i<[tableView numberOfRowsInSection:indexPath.section]; i++)
    {
        NSIndexPath *cindexPath = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
        UITableViewCell *cCell = [tableView cellForRowAtIndexPath:cindexPath];
        [cCell setAccessoryType:UITableViewCellAccessoryNone];
    }
    
    [[tableView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Type *_type = [self.selectArray objectAtIndex:indexPath.row];
    
    RegisterViewController *parentVC = (RegisterViewController *)delegate;
    [parentVC updateSelectValue:_type.objId bySelectType:self.selectType];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
