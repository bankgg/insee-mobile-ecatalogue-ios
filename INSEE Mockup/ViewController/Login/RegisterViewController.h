//
//  RegisterViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/10/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterSelectViewController.h"
#import "RegisterDateSelectViewController.h"
#import "Global.h"
#import "AFHTTPRequestOperationManager.h"
#import "ImageSizer.h"
#import "User.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "FormValidation.h"

typedef enum {
    registerModeEdit = 200,
    registerModeAdd = 100
}RegisterMode;

@interface RegisterViewController : UITableViewController <UIGestureRecognizerDelegate, UITextFieldDelegate, UIActionSheetDelegate,
                                                            UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *lab1, *lab2, *lab3, *lab4, *lab5, *lab6, *lab7, *lab8, *lab9, *lab10, *lab3_2;
    IBOutlet UITextField *tf1, *tf2, *tf3, *tf4, *tf5, *tf6, *tf7, *tf8, *tf9, *tf10;
    NSArray *textFields;
    IBOutlet UIButton *btn1;
    IBOutlet UILabel *labRemark;
    
    IBOutlet UIButton *photoButton;
    IBOutlet UIImageView *photoImageView;
    
    IBOutlet UIView *activeField;
    
    NSMutableArray *typeArr;
    int typeSelectId;
    
    NSMutableArray *roleArr;
    int roleSelectId;
    
    NSMutableArray *sexArr;
    int sexSelectId;
    
    NSMutableArray *allTypeArr;
    
    NSDate *birthDate;
    
    UITableView *tableV;
    
    UIPopoverController *popVC;
    
    IBOutlet UIButton *submitButton;
    
    UIImage *pickerImage;
    
    Reachability *reach;
    
    FormValidation *form;
}

-(void)updateSelectValue:(int)selectIndex bySelectType:(SelectType)selectType;
-(void)updateSelectDateValue:(NSDate *)selectDate;

-(IBAction)photoButtonPressed:(id)sender;
-(IBAction)submitButtonPressed:(id)sender;

@property RegisterMode regMode;

@end
