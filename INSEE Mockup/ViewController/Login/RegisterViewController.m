//
//  RegisterViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/10/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "RegisterViewController.h"
#import "UICommon.h"
#import "CustomUIActionSheet.h"
#import "Type.h"
#import "DateService.h"
#import "LoginViewController.h"
#import "JustAlert.h"

#import "IQKeyboardManager.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = LocalizedString(@"reg_title_create");
    [btn1 setTitle:LocalizedString(@"reg_btn_save_create") forState:UIControlStateNormal];
    
    tableV = (UITableView *)self.view;
    tableV.backgroundView = nil;
    tableV.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    tableV.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tableV.bounds.size.width, 10.0f)];
    
    [self setLabels];
    [self setTextFields];
    [UICommon setFontButton:btn1];
    [UICommon setShadow:btn1];
    labRemark.font = [UIFont fontWithName:@"INSEE" size:16.0f];
    
    [UICommon setBorder:photoButton];
    
    if (self == [self.navigationController.viewControllers objectAtIndex:0])
    {
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backToParent)];
        self.navigationItem.leftBarButtonItem = backButton;
    }
    
    [self loadTypeList];
    
    /*
    //Resign when tap outside
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
     */
    
    if ([User isUserLogin])
    {
        self.title = LocalizedString(@"reg_title_edit");
        [btn1 setTitle:LocalizedString(@"reg_btn_save_edit") forState:UIControlStateNormal];
        [tf5 setUserInteractionEnabled:NO];
        tf5.textColor = [UIColor grayColor];
    }
    
    //Reachability
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    reach = [Reachability reachabilityWithHostname:REACHABILITY_HOST];
    [reach startNotifier];
    
    
    //Form Validation -- TESTING
    form = [[FormValidation alloc] init];
    form.colorWarning = [UIColor redColor];
    
    [form addValidateTextfield:tf1 withLabel:lab1 withValidateType:ValidateRequire withArgument:nil];
    [form addValidateTextfield:tf2 withLabel:lab2 withValidateType:ValidateRequire withArgument:nil];
    [form addValidateTextfield:tf3 withLabel:lab3 withValidateType:ValidateRequire withArgument:nil];
    
    [form addValidateTextfield:tf5 withLabel:lab5 withValidateType:ValidateRequire withArgument:nil];
    [form addValidateTextfield:tf5 withLabel:lab5 withValidateType:ValidateEmail withArgument:nil];
    
    if (![User isUserLogin])
    {
        [form addValidateTextfield:tf6 withLabel:lab6 withValidateType:ValidateRequire withArgument:tf7];
        [form addValidateTextfield:tf7 withLabel:lab7 withValidateType:ValidateRequire withArgument:nil];
    }
    [form addValidateTextfield:tf6 withLabel:lab6 withValidateType:ValidatePassword withArgument:tf7];
    [form addValidateTextfield:tf7 withLabel:lab7 withValidateType:ValidatePasswordConfirm withArgument:tf6];
    
    
    [[IQKeyboardManager sharedManager] setShouldShowTextFieldPlaceholder:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)backToParent
{
    if (self != [self.navigationController.viewControllers objectAtIndex:0])
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    if ([self respondsToSelector:@selector(presentingViewController)]){
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)backToRoot
{
    [[[self presentingViewController] presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissKeyboard
{
    [self.view endEditing:YES];
    //[self pickerHide];
}

- (void)setLabels
{
    UIFont *labelFont = [UIFont fontWithName:@"INSEE-Bold" size:20.0f];
    NSArray *labels = [NSArray arrayWithObjects:lab1, lab2, lab3, lab4, lab5, lab6, lab7, lab8, lab9, lab10, lab3_2, nil];
    for (UILabel *lab in labels)
    {
        lab.font = labelFont;
        lab.textColor = [UIColor colorWithWhite:0.15 alpha:1.0];
    }
    
    lab1.text = LocalizedString(@"reg_lab_firstname");
    lab2.text = LocalizedString(@"reg_lab_lastname");
    lab3.text = LocalizedString(@"reg_lab_birthdate");
    lab3_2.text = LocalizedString(@"reg_lab_photo");
    lab4.text = LocalizedString(@"reg_lab_gender");
    lab5.text = LocalizedString(@"reg_lab_email");
    lab6.text = LocalizedString(@"reg_lab_password");
    lab7.text = LocalizedString(@"reg_lab_confirm");
    lab8.text = LocalizedString(@"reg_lab_role");
    lab9.text = LocalizedString(@"reg_lab_type");
    lab10.text = LocalizedString(@"reg_lab_inseeclub");
    
    lab10.font = [UIFont fontWithName:lab10.font.fontName size:17.0f];
    lab10.numberOfLines = 0;
}

- (void)setTextFields
{
    if (textFields == nil) {
        textFields = [NSArray arrayWithObjects:tf1, tf2, tf3, tf4, tf5, tf6, tf7, tf8, tf9, tf10, nil];
    }
    for (UITextField *textField in textFields)
    {
        [UICommon setTextField:textField];
        textField.textColor = [UIColor colorWithWhite:0.25 alpha:1.0];
        textField.delegate = self;
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == tf2 || textField == tf7 || textField == tf10)
    {
        [self dismissKeyboard];
        return YES;
    }
    
    for (int i=0; i<[textFields count]-1; i++)
    {
        UITextField *tf = [textFields objectAtIndex:i];
        UITextField *tfNext = [textFields objectAtIndex:i+1];
        
        if (textField == tf)
        {
            [tfNext becomeFirstResponder];
            [tableV selectRowAtIndexPath:[NSIndexPath indexPathForRow:i+1 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
            
            break;
        }
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyboard];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 4 || indexPath.row == 8 || indexPath.row == 9)
    {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        RegisterSelectViewController *selectVC = [sb instantiateViewControllerWithIdentifier:@"RegisterSelectViewController"];
        
        switch (indexPath.row) {
            case 4:
                selectVC.selectArray = sexArr;
                selectVC.selectId = sexSelectId;
                selectVC.selectType = selectTypeSex;
                selectVC.title = @"Gender";
                break;
            case 8:
                selectVC.selectArray = roleArr;
                selectVC.selectId = roleSelectId;
                selectVC.selectType = selectTypeRole;
                selectVC.title = @"Role";
                break;
            case 9:
                selectVC.selectArray = typeArr;
                selectVC.selectId = typeSelectId;
                selectVC.selectType = selectTypeType;
                selectVC.title = @"Type";
                break;
                
            default:
                break;
        }
        
        selectVC.delegate = self;
        [self.navigationController pushViewController:selectVC animated:YES];
    }
    else if (indexPath.row == 3)
    {
        //date
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
        RegisterDateSelectViewController *selectVC = [sb instantiateViewControllerWithIdentifier:@"RegisterDateSelectViewController"];
        selectVC.delegate = self;
        selectVC.selectDate = birthDate;
        [self.navigationController pushViewController:selectVC animated:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //[searchBar resignFirstResponder];
    [self dismissKeyboard];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 2) {
        return 88;
    }
    else if (indexPath.row == 10) {
        return 50;
    }
//    else if (self.regMode == registerModeEdit && indexPath.row == 7) {
//        return 0;
//    }
    
    return [tableView rowHeight];
}

#pragma mark - Load List Data

-(void)loadTypeList
{
    typeArr = [NSMutableArray array];
    roleArr = [NSMutableArray array];
    sexArr = [NSMutableArray array];
    allTypeArr = [NSMutableArray array];
    
    //LOAD TYPE LIST
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:[Global PATH_OF:S_PATH_SERVICE_TYPE_LIST2] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         //NSLog(@"JSON: %@", responseObject);
         
         for (NSDictionary *dict in responseObject)
         {
             Type *_type = [[Type alloc] init];
             _type.objId = [[dict objectForKey:@"id"] intValue];
             _type.type_name = [dict objectForKey:[@"type_name" languageField]];
             _type.type_group_id = [[dict objectForKey:@"type_group_id"] intValue];
             
             switch (_type.type_group_id) {
                 case 1000:
                     [sexArr addObject:_type];
                     break;
                 case 2000:
                     [roleArr addObject:_type];
                     break;
                 case 3000:
                     [typeArr addObject:_type];
                     break;
                     
                 default:
                     break;
             }
             
             [allTypeArr addObject:_type];
         }
         
         if ([User isUserLogin]) {
             [self loadUserData];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
     }];
}

#pragma mark - Update Value From Select Page

-(void)updateSelectValue:(int)selectId bySelectType:(SelectType)selectType
{
    if (selectId <= 0) {
        return;
    }
    
    Type *aType = [[Type alloc] init];
    for (Type *_type in allTypeArr)
    {
        if (_type.objId == selectId)
        {
            aType = _type;
            break;
        }
    }
    
    if (selectType == selectTypeSex)
    {
        sexSelectId = selectId;
        tf4.text = aType.type_name;
    }
    else if (selectType == selectTypeRole)
    {
        roleSelectId = selectId;
        tf8.text = aType.type_name;
    }
    else if (selectType == selectTypeType)
    {
        typeSelectId = selectId;
        tf9.text = aType.type_name;
    }
}

-(void)updateSelectDateValue:(NSDate *)selectDate
{
    birthDate = selectDate;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
	df.dateStyle = NSDateFormatterMediumStyle;
    
//    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    tf3.text = [df stringFromDate:selectDate];
    
    [form textFieldUpdate:tf3];
}

#pragma mark - Photo

-(IBAction)photoButtonPressed:(id)sender
{
    CustomUIActionSheet *actSheet = [[CustomUIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo", @"Choose Photo", nil];
    actSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    //[actSheet showInView:self.view];
    
    [actSheet showFromRect:photoButton.frame inView:photoButton.superview animated:YES];
    
    //[popoverController presentPopoverFromRect:[self.myDetailViewController.tableView convertRect:aFrame toView:self.view] inView:self.view permittedArrowDirections:UIPopoverArrowDirectionRight animated:YES];
    
    //[actSheet showFromRect:photoButton.bounds inView:self.view animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController *imPicker = [[UIImagePickerController alloc] init];
	imPicker.delegate = self;
    
    if (buttonIndex == 0)
    {
        imPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
    }
    else if(buttonIndex == 1)
    {
        imPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    if (buttonIndex == 0 || buttonIndex == 1)
    {
        if (IDIOM == IPAD)
        {
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:imPicker];
            [popover presentPopoverFromRect:photoButton.frame inView:photoButton.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            popVC = popover;
        }
        else
        {
            [self presentViewController:imPicker animated:YES completion:nil];
        }
    }
}

#pragma mark - ImagePicker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	[picker dismissViewControllerAnimated:YES completion:NULL];
	
    pickerImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [photoImageView setImage:pickerImage];
    [photoImageView setContentMode:UIViewContentModeScaleAspectFill];
    [photoImageView setClipsToBounds:YES];
    
    if (popVC != nil) {
        [popVC dismissPopoverAnimated:YES];
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - Submit
-(IBAction)submitButtonPressed:(id)sender
{
    //Trim all textfields
    for (UITextField *_tf in textFields)
    {
        _tf.text = [_tf.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    //Check Validate
    if (![form formPassValidate]) {
        return;
    }
    
    /*if (![self validatesRequiredFields])
    {
        return;
    }
    */
    
    //SHOW LOADING
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading";
    hud.dimBackground = YES;
    
    //PREPARE DATA ===================================================
    
    //Set Parameters
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSNumber numberWithInt:[User currentUserId]] forKey:@"id"];
    [parameters setValue:tf1.text forKey:@"user_firstname"];
    [parameters setValue:tf2.text forKey:@"user_lastname"];
    [parameters setValue:tf5.text forKey:@"user_email"];
    [parameters setValue:tf6.text forKey:@"user_password"];
    [parameters setValue:tf10.text forKey:@"user_club_card"];
    [parameters setValue:[DateService stringDateMySqlFromDate:birthDate withTime:NO] forKey:@"user_birthdate"];
    
    [parameters setValue:[NSNumber numberWithInt:typeSelectId] forKey:@"user_type_type_id"];
    [parameters setValue:[NSNumber numberWithInt:sexSelectId] forKey:@"user_sex_type_id"];
    [parameters setValue:[NSNumber numberWithInt:roleSelectId] forKey:@"user_role_type_id"];
    
    
    //SUBMIT DATA ===================================================
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager POST:[Global PATH_OF:S_PATH_SERVICE_REGISTER] parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        //PREPARE IMAGE ===================================================
        if (pickerImage != nil)
        {
            pickerImage = [ImageSizer imageResizedWithImage:pickerImage scaledToSize:[ImageSizer imageSizeOfImage:pickerImage withShortSide:200.0]];
            NSData *imageData = UIImageJPEGRepresentation(pickerImage, 0.7);
            
            [formData appendPartWithFileData:imageData name:@"image_file" fileName:@"image.jpg" mimeType:@"image/jpeg"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"JSON: %@", responseObject);
        
        //DB ERROR ===================================================
        if ([responseObject objectForKey:@"error"])
        {
            //register failed ===================================================
            [hud hide:YES];
            
            NSString *erMessage = [responseObject objectForKey:@"error"];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:erMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            //register finished ===================================================
            hud.mode = MBProgressHUDModeText;
            hud.labelText = [User isUserLogin]?@"Profile Edited":@"Registration Successful";
            [hud hide:YES afterDelay:1.5];
            
            User *sUser = [User objFromDict:responseObject];
            [User saveUserData:sUser];
            
            if (self.presentingViewController.class == [LoginViewController class])
            {
                [self performSelector:@selector(backToRoot) withObject:nil afterDelay:1.8];
            }
            else
            {
                [self performSelector:@selector(backToParent) withObject:nil afterDelay:1.8];
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [hud hide:YES];
        //CONNECTION ERROR ===================================================
        NSLog(@"Error: %@", error);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Fail to make a database connection" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }];
}

#pragma mark - LOAD USER PROFILE DATA

-(void)loadUserData
{
    NSString *pathUrl = [NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_USER], [User currentUserId]];
    
    //SHOW LOADING
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading";
    hud.dimBackground = YES;
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:pathUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         [hud hide:YES];
         NSLog(@"JSON: %@", responseObject);
         
         User *_user = [User objFromDict:responseObject];
         
         tf1.text = _user.user_firstname;
         tf2.text = _user.user_lastname;
         tf5.text = _user.user_email;
         tf10.text = _user.user_club_card;
         [self updateSelectDateValue:_user.user_birthdate];
         [self updateSelectValue:_user.user_sex_type_id bySelectType:selectTypeSex];
         [self updateSelectValue:_user.user_role_type_id bySelectType:selectTypeRole];
         [self updateSelectValue:_user.user_type_type_id bySelectType:selectTypeType];
         
         [photoImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:[Global PATH_OF:S_PATH_IMAGE], _user.user_image_path]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
         [photoImageView setClipsToBounds:YES];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
         [hud hide:YES];
     }];
}

#pragma mark - VALIDATION

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)validatesRequiredFields
{
    if (tf1.text.length == 0)
    {
        [JustAlert alertWithMessage:@"First Name is required"];
        return NO;
    }
    if (tf2.text.length == 0)
    {
        [JustAlert alertWithMessage:@"Last Name is required"];
        return NO;
    }
    if (tf3.text.length == 0)
    {
        [JustAlert alertWithMessage:@"Birthdate is required"];
        return NO;
    }
    if (tf5.text.length == 0)
    {
        [JustAlert alertWithMessage:@"Email is required"];
        return NO;
    }
    if (![self NSStringIsValidEmail:tf5.text])
    {
        [JustAlert alertWithMessage:@"Email is invalid"];
        return NO;
    }
    
    if (![User isUserLogin])
    {
        if (tf6.text.length == 0)
        {
            [JustAlert alertWithMessage:@"Password is required"];
            return NO;
        }
        if (tf6.text.length < 6)
        {
            [JustAlert alertWithTitle:@"Password is too short" message:@"Require at least 6 characters"];
            return NO;
        }
        if (![tf6.text isEqualToString:tf7.text])
        {
            [JustAlert alertWithMessage:@"Password does not match"];
            return NO;
        }
    }
    
    if ([User isUserLogin])
    {
        if (tf6.text.length > 0)
        {
            if (tf6.text.length < 6)
            {
                [JustAlert alertWithTitle:@"Password is too short" message:@"Require at least 6 characters"];
                return NO;
            }
            if (![tf6.text isEqualToString:tf7.text])
            {
                [JustAlert alertWithMessage:@"Password does not match"];
                return NO;
            }
        }
    }
    
    return YES;
}

#pragma mark - Reachability

-(void)reachabilityChanged:(id)sender
{
    if (!reach.isReachable)
    {
        [JustAlert alertWithMessage:@"No internet connection !!"];
        [submitButton setUserInteractionEnabled:NO];
        submitButton.alpha = 0.5;
    }
    else
    {
        if (allTypeArr == nil) {
            [self loadTypeList];
        }
        [submitButton setUserInteractionEnabled:YES];
        submitButton.alpha = 1.0;
    }
}



@end
