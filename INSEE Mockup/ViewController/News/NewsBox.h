//
//  NewsBox.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"
#import "Global.h"
#import "UIImageView+WebCache.h"

@interface NewsBox : UIView
{
    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet UILabel *labTitle;
    //__weak IBOutlet UILabel *labDesc;
    __weak IBOutlet UILabel *labDate;
    //__weak IBOutlet UIView *boxDate;
    __weak IBOutlet UIView *frameBoxView;
    
    
    UILabel *labDescClone;
}

-(void)initial;
-(void)setNews:(News *)news;
+(float)viewHeight;

@end
