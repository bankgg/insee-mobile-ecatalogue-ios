//
//  NewsBox.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "NewsBox.h"
#import "UICommon.h"
#import "DateService.h"

#import "UIImageView+Ext.h"
#import "LocalizeHelper.h"
#import "NSString+Language.h"

@implementation NewsBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initial
{
    if (IDIOM != IPAD)
    {
        labTitle.font = [UIFont fontWithName:@"INSEE" size:20.0f];
        labDate.font = [UIFont fontWithName:@"INSEE" size:14.0f];
        self.frame = CGRectMake(0, 0, 320, 80);
    }
    else
    {
        labTitle.font = [UIFont fontWithName:@"INSEE" size:30.0f];
        labDate.font = [UIFont fontWithName:@"INSEE" size:22.0f];
        self.frame = CGRectMake(0, 0, 768, 120);
    }
    
    [UICommon setBorder:frameBoxView];
}

-(void)setNews:(News *)news
{
    labTitle.text = news.news_header;
    labDate.text = [DateService stringDateFromDate:news.news_date withTime:NO];
    
    //NSLog(@"labDate.text = %@", labDate.text);
    
    float diffY = [UICommon diffHeightFromAdjustSizeLabel:labTitle byString:news.news_header andNumOfLines:2];
    [UICommon adjustPositionOfControl:labDate byDiffHeight:diffY];
    
    if (diffY > 0)
    {
        CGRect frame1 = labTitle.frame;
        frame1.origin.y -= diffY/2;
        labTitle.frame = frame1;
        
        CGRect frame2 = labDate.frame;
        frame2.origin.y -= diffY/2;
        labDate.frame = frame2;
    }
    
    NSString *imgPath = IS_PAD?news.news_image_sm_tablet_path:news.news_image_sm_phone_path;
    NSString *imgURLString = [NSString stringWithFormat:[Global PATH_OF:S_PATH_IMAGE], imgPath];
    //NSLog(@"image path %@", imgURLString);
    NSURL *imgURL = [NSURL URLWithString:imgURLString];
    [imageView sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
    [imageView setClipsToBounds:YES];
}

+(float)viewHeight
{
    float height = 80;
    if (IDIOM == IPAD)
    {
        height = 120;
    }
    return height;
}

@end
