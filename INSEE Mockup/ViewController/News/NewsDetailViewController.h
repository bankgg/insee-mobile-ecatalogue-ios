//
//  NewsDetailViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "News.h"
#import "Global.h"
#import "AFHTTPRequestOperationManager.h"
#import "CJSONDeserializer.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "LanguageUIViewController.h"

@interface NewsDetailViewController : LanguageUIViewController
{
    __weak IBOutlet UIImageView *imageView;
    __weak IBOutlet UILabel *labTitle;
    __weak IBOutlet UILabel *labDate;
    __weak IBOutlet UILabel *labContent;
    
    __weak IBOutlet UIView *viewBox;
    __weak IBOutlet UIScrollView *scrollView;
    
    float space;
}

@property int itemId;

@end
