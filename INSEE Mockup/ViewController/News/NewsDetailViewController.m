//
//  NewsDetailViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "NewsDetailViewController.h"
#import "UICommon.h"
#import "DateService.h"
#import "Stat.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"
#import "UIView+Extension.h"
#import "UIImageView+Ext.h"

@interface NewsDetailViewController ()
{
    NSDictionary *newsDict;
}
@end

@implementation NewsDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    space = IS_PAD?30.0:20.0;
    
    labTitle.text = labContent.text = labDate.text = @"";
    
    [UICommon setBorder:viewBox];
    
    imageView.alpha = 0;
    
    [self loadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(void)loadData
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:[NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_NEWS_DETAIL], self.itemId] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         newsDict = responseObject;
         [self setUI];
         
         //stat
         [Stat saveStatOfPageType:pageTypeNewsDetail pageName:newsDict[@"news_header"] withAction:@"view"];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
     }];
}

-(void)setupContent
{
    labTitle.text = newsDict[[@"news_header" languageField]];
    labDate.text = [DateService INSEE_stringDateLangFromServiceString:newsDict[@"news_date"] withTime:NO];
    labContent.text = newsDict[[@"news_detail" languageField]];
    
    [labTitle sizeToFit2];
    [labContent sizeToFit2];
    
    [labTitle setFramePosY:imageView.posOfEndY + 15];
    [labDate setFramePosY:labTitle.posOfEndY + 5];
    [labContent setFramePosY:labDate.posOfEndY + 15];
//    [viewBox setFrameHeight:labContent.posOfEndY + space];
    viewBox.frame = CGRectMake(viewBox.frame.origin.x, viewBox.frame.origin.y, viewBox.frame.size.width, labContent.posOfEndY + space);
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, viewBox.posOfEndY + space)];
}

-(void)setUI
{
    [self setupContent];
    
    NSURL *imgUrl = [Global imageURLFromPath:IS_PAD?newsDict[@"news_image_lg_tablet_path"]:newsDict[@"news_image_lg_phone_path"]];
    
    imageView.alpha = 0;
    [imageView sd_setImageWithURL:imgUrl placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (image)
        {
            [imageView setAndScaleToFixWidthOfImage:image];
            [self setupContent];
            
            [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                imageView.alpha = 1.0;
            } completion:nil];
        }
        else
        {
            
        }
        
    }];
    
    return;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)languageLoad
{
    self.title = LocalizedString(@"news_title");
    [self setupContent];
}

@end
