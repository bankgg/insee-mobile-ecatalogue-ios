//
//  NewsListViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultTableViewController.h"
#import "Global.h"
#import "News.h"
#import "AFHTTPRequestOperationManager.h"
#import "CJSONDeserializer.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "JustAlert.h"
#import "LanguageUITableViewController.h"

#define NUM_ROW_LOAD 10

@interface NewsListViewController : LanguageUITableViewController
{
    NSMutableArray *listContent;
    int offset;
    bool isLoading;
    bool isMoreData;
    
    Reachability *reach;
    
    UIRefreshControl *refreshControl;
}
@end
