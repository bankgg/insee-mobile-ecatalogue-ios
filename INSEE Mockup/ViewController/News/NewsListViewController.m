//
//  NewsListViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/13/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "NewsListViewController.h"
#import "NewsDetailViewController.h"
#import "NewsBox.h"
#import "UICommon.h"
#import "AlignButton.h"
#import "DateService.h"
#import "OptionViewController.h"
#import "UIView+Extension.h"
#import "LocalizeHelper.h"
#import "NSString+Language.h"
#import "UIImageView+Ext.h"

@interface NewsListViewController ()

@end

@implementation NewsListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    float tableHeaderHeight = IS_PAD?20.0:10.0;
    [self.tableView setTableHeaderView:[UICommon blankHeaderViewAtHeight:tableHeaderHeight]];
    [self.tableView setTableFooterView:[UICommon blankHeaderViewAtHeight:tableHeaderHeight]];
    
    //Reachability
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    reach = [Reachability reachabilityWithHostname:REACHABILITY_HOST];
    [reach startNotifier];
    
    offset = 0;
    if (listContent == nil) {
        listContent = [NSMutableArray array];
    }
    [self loadData];
    
    //RefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    [self setRefreshControl:refreshControl];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self checkNews];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)languageLoad
{
    self.title = LocalizedString(@"news_title");
    [self.tableView reloadData];
}

#pragma mark - Load Data

-(void)loadData
{
    if (isLoading) {
        return;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    isLoading = YES;
    
    NSString *servicePath = [NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_NEWS_LIST2], offset, NUM_ROW_LOAD];
    NSLog(@"servicePath = %@", servicePath);
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:servicePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray *objArray = (id)responseObject;
        for (id obj in objArray)
        {
            [listContent addObject:obj];
        }
        if ([objArray count] == NUM_ROW_LOAD) {
            isMoreData = YES;
        }
        else {
            isMoreData = NO;
        }
        if ([objArray count] > 0) {
            offset += NUM_ROW_LOAD;
        }
        
        [self.tableView reloadData];
        
        //Clear badge value
        [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:nil];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [refreshControl endRefreshing];
        isLoading = NO;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error1: %@", error);
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        [refreshControl endRefreshing];
        isLoading = NO;
    }];
}

-(void)reloadData
{
    //clear index and array
    offset = 0;
    listContent = nil;
    listContent = [NSMutableArray array];
    
    [self loadData];
    //[self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listContent count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//    cell.backgroundColor = [UIColor clearColor];
    
    UILabel *labelTitle = (id)[cell viewWithTag:1000];
    UILabel *labelDate = (id)[cell viewWithTag:2000];
    UIImageView *imageView = (id)[cell viewWithTag:3000];
//    UIView *frameView = (id)[cell viewWithTag:10000];
    
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    labelTitle.text = dict[[@"news_header" languageField]];
    [labelTitle sizeToFit2withNumLines:2];
    
    labelDate.text = [DateService INSEE_stringDateLangFromServiceString:dict[@"news_date"] withTime:NO];
    [labelDate sizeToFit2withNumLines:1];
    
    float labSpace = 0;
    [labelDate setFramePosY:labelTitle.posOfEndY + labSpace];
    
    float labelsHeight = labelDate.posOfEndY - labelTitle.frame.origin.y;
    float labelTitleY = (cell.frame.size.height - labelsHeight)/2;
    
    [labelTitle setFramePosY:labelTitleY];
    [labelDate setFramePosY:labelTitle.posOfEndY + labSpace];
    
    [imageView setImageFromImagePathPhone:dict[@"news_image_sm_phone_path"] andImagePathTablet:dict[@"news_image_sm_tablet_path"]];
    
//    [frameView makeBorder];
    
//    [cell makeBorder];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float height = 100;
    if (IDIOM == IPAD)
    {
        height = 160;
    }
    return height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    NewsDetailViewController *detailVC = [[Global storyboard] instantiateViewControllerWithIdentifier:@"NewsDetailViewController"];
    detailVC.itemId = [dict[@"id"] intValue];
    
    [self.navigationController pushViewController:detailVC animated:YES];
}

//check position of scroll of tableview to load more data
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= 40)
    {
        if (!isLoading && isMoreData)
        {
            //offset += NUM_ROW_LOAD;
            [self loadData];
            NSLog(@"load more news");
        }
    }
}

#pragma mark - Check News
-(void)checkNews
{
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:[Global PATH_OF:S_PATH_SERVICE_NEWS_LAST] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSString *lastNewsValue = [responseObject objectForKey:@"last_value"];
         
         if (![[News getLastestClientNewsValue] isEqualToString:lastNewsValue])
         {
             //There are newer news
             [News saveLatestNewsValue:lastNewsValue];
             [self reloadData];
         }
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
     }];
}

#pragma mark - Reachability

-(void)reachabilityChanged:(id)sender
{
    if (!reach.isReachable) {
//        [JustAlert alertWithMessage:@"No internet connection !!"];
    }
    else {
        [self reloadData];
    }
}

@end
