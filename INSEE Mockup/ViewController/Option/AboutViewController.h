//
//  AboutViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 12/9/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"

@interface AboutViewController : UIViewController
{
    IBOutlet UILabel *label1;
    IBOutlet UILabel *label2;
    
    IBOutlet UILabel *labelProd;
}
@end
