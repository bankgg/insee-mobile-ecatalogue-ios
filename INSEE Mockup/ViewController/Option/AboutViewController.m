//
//  AboutViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 12/9/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    label1.font = [UIFont fontWithName:@"INSEE-Bold" size:IS_PAD?40:30];
    label2.font = [UIFont fontWithName:@"INSEE" size:IS_PAD?30:20];
    
    //label1.text = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
    label2.text = [NSString stringWithFormat:@"Version %@ Build %@", [Global appVersion], [Global build]];
    
    //
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"is preproduction mode"])
    {
        labelProd.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
