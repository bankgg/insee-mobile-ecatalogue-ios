//
//  GoogleMapLegalViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/27/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"

@interface GoogleMapLegalViewController : UIViewController
{
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UILabel *labContent;
}
@end
