//
//  GoogleMapLegalViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/27/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "GoogleMapLegalViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "UICommon.h"

@interface GoogleMapLegalViewController ()

@end

@implementation GoogleMapLegalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    labContent.text = [GMSServices openSourceLicenseInfo];
    [UICommon diffHeightFromAdjustSizeLabel:labContent byString:labContent.text];
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, labContent.frame.size.height + labContent.frame.origin.y *2)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
