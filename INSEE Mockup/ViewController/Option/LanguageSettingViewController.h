//
//  LanguageSettingViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/15/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LanguageUITableViewController.h"

@interface LanguageSettingViewController : LanguageUITableViewController //UITableViewController

@end
