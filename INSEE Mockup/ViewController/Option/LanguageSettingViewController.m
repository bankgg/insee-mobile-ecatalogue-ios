//
//  LanguageSettingViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/15/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "LanguageSettingViewController.h"
#import "LocalizeHelper.h"
#import "TabBarViewController.h"

@interface LanguageSettingViewController ()
{
    NSArray *listContent;
}
@end

@implementation LanguageSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSDictionary *eng = [NSDictionary dictionaryWithObjectsAndKeys:
                         @"English", @"lang_name",
                         @"en", @"lang_key", nil];
    
    NSDictionary *thai = [NSDictionary dictionaryWithObjectsAndKeys:
                         @"ภาษาไทย", @"lang_name",
                         @"th", @"lang_key", nil];
    
    listContent = @[eng, thai];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)languageLoad
{
    self.title = LocalizedString(@"language");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listContent.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    cell.textLabel.text = dict[@"lang_name"];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"] isEqualToString:dict[@"lang_key"]])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    //Save
    LocalizationSetLanguage(dict[@"lang_key"]);
    [[NSUserDefaults standardUserDefaults] setObject:dict[@"lang_key"] forKey:@"user_language"];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
    //Reload TabBarItems Title
    if (self.tabBarController)
    {
        TabBarViewController *tabVC = (id)self.tabBarController;
        [tabVC reloadTabItemTitles];
    }
}

@end
