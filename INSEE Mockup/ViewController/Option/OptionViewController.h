//
//  OptionViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/21/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DefaultTableViewController.h"
#import "Global.h"
#import "LanguageUITableViewController.h"

#define SECTION_USER        0
#define SECTION_FEATURE     1
#define SECTION_SETTING     2
#define SECTION_OTHER       3

#define SECTION_USER__NUMROW        2
#define ROW_USER_EDITPROFILE        0
#define ROW_USER_SIGNOUT            1

#define SECTION_FEATURE__NUMROW     5
#define ROW_FEATURE_PROJECT         0
#define ROW_FEATURE_CART            1
#define ROW_FEATURE_LOCATION        2
#define ROW_FEATURE_FAQS            3
#define ROW_FEATURE_INSEECONNECT    4

#define SECTION_SETTING__NUMROW     1
#define ROW_SETTING_LANGUAGE        0

#define SECTION_OTHER__NUMROW       4
#define ROW_OTHER_ABOUT             0
#define ROW_OTHER_CONTACT           1
#define ROW_OTHER_POLICY            2
#define ROW_OTHER_GOOGLEMAP         3


@interface OptionViewController : LanguageUITableViewController <UIAlertViewDelegate>

@end
