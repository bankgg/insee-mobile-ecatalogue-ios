//
//  OptionViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/21/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "OptionViewController.h"
#import "User.h"

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "AboutViewController.h"
#import "ProjectRefListViewController.h"
#import "FaqL1ViewController.h"
#import "LocationMapViewController.h"
#import "ShoppingCartViewController.h"
#import "LanguageSettingViewController.h"
#import "SVWebViewController.h"

@interface OptionViewController ()

@end

@implementation OptionViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

//    self.title = @"Options";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.backgroundView = nil;
    
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(backToParent)];
//    self.navigationItem.leftBarButtonItem = backButton;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)languageLoad
{
    self.title = LocalizedString(@"options_title");
}

- (void)backToParent
{
    if ([self respondsToSelector:@selector(presentingViewController)]){
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }
    else {
        [self.parentViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"INSEE-Bold" size:22.0f];
    cell.detailTextLabel.font = [UIFont fontWithName:@"INSEE" size:20.0f];
    
    if (indexPath.section == SECTION_USER)
    {
        switch (indexPath.row) {
            case ROW_USER_EDITPROFILE:
                cell.textLabel.text = [User isUserLogin]?LocalizedString(@"options_edit_profile"):LocalizedString(@"options_sign_up");
                break;
            case ROW_USER_SIGNOUT:
                cell.textLabel.text = [User isUserLogin]?LocalizedString(@"options_sign_out"):LocalizedString(@"options_sign_in");
                break;
                
            default:
                break;
        }
    }
    else if (indexPath.section == SECTION_FEATURE)
    {
        switch (indexPath.row) {
            case ROW_FEATURE_PROJECT:
                cell.textLabel.text = LocalizedString(@"options_proj");
                break;
            case ROW_FEATURE_CART:
                cell.textLabel.text = LocalizedString(@"options_cart");
                break;
            case ROW_FEATURE_LOCATION:
                cell.textLabel.text = LocalizedString(@"options_location");
                break;
            case ROW_FEATURE_FAQS:
                cell.textLabel.text = LocalizedString(@"options_faqs");
                break;
            case ROW_FEATURE_INSEECONNECT:
                cell.textLabel.text = LocalizedString(@"options_insee_connect");
                break;
                
            default:
                break;
        }
    }
    else if (indexPath.section == SECTION_SETTING)
    {
        cell.textLabel.text = LocalizedString(@"language");
    }
    else if (indexPath.section == SECTION_OTHER)
    {
        switch (indexPath.row) {
            case ROW_OTHER_ABOUT:
                cell.textLabel.text = LocalizedString(@"options_about");
                break;
            case ROW_OTHER_CONTACT:
                cell.textLabel.text = LocalizedString(@"options_contact");
                break;
            case ROW_OTHER_POLICY:
                cell.textLabel.text = LocalizedString(@"options_policy");
                break;
            case ROW_OTHER_GOOGLEMAP:
                cell.textLabel.text = LocalizedString(@"options_google_map");
                break;
                
            default:
                break;
        }
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case SECTION_USER:
            return LocalizedString(@"options_section_user");
            break;
        case SECTION_FEATURE:
            return LocalizedString(@"options_section_feature");
            break;
        case SECTION_SETTING:
            return LocalizedString(@"options_section_settings");
            break;
        case SECTION_OTHER:
            return LocalizedString(@"options_section_others");
            break;
            
        default:
            break;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == SECTION_USER)
    {
        if (indexPath.row == ROW_USER_EDITPROFILE)
        {
            RegisterViewController *vc = [[Global storyboardIphone] instantiateViewControllerWithIdentifier:@"RegisterViewController"];
            if ([User isUserLogin]) {
                vc.regMode = registerModeEdit;
            }
            else {
                vc.regMode = registerModeAdd;
            }
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == ROW_USER_SIGNOUT)
        {
            if ([User isUserLogin])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sign out ?" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
                [alert show];
            }
            else
            {
                LoginViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LoginViewController"];
                [self presentViewController:vc animated:YES completion:nil];
            }
        }
    }
    
    else if (indexPath.section == SECTION_OTHER)
    {
        if (indexPath.row == ROW_OTHER_ABOUT)
        {
//            AboutViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"AboutViewController"];
//            [self.navigationController pushViewController:vc animated:YES];
        }
        
    }
    
    else if (indexPath.section == SECTION_FEATURE)
    {
        if (indexPath.row == ROW_FEATURE_PROJECT)
        {
            ProjectRefListViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProjectRefListViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == ROW_FEATURE_FAQS)
        {
            FaqL1ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"FaqL1ViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == ROW_FEATURE_LOCATION)
        {
            LocationMapViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LocationMapViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == ROW_FEATURE_CART)
        {
            ShoppingCartViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ShoppingCartViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == ROW_FEATURE_INSEECONNECT)
        {
            NSString *urlPath = [Global PATH_OF:URL_INSEE_CLUBCARD];
            SVWebViewController *webViewController = [[SVWebViewController alloc] initWithAddress:urlPath];
            [self.navigationController pushViewController:webViewController animated:YES];
        }
    }
    
    else if (indexPath.section == SECTION_SETTING)
    {
        if (indexPath.row == ROW_SETTING_LANGUAGE)
        {
            LanguageSettingViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LanguageSettingViewController"];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        [User clearUserData];
        [self.tableView reloadData];
        
        LoginViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}

@end
