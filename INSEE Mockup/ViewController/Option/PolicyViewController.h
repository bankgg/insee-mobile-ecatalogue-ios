//
//  PolicyViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 12/18/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICommon.h"
#import "MBProgressHUD.h"
#import "Global.h"

@interface PolicyViewController : UIViewController <UIWebViewDelegate>
{
    //__weak IBOutlet UIScrollView *scrollView;
    //__weak IBOutlet UILabel *labContent;
    IBOutlet UIWebView *webview;
    
    MBProgressHUD *hud;
}
@end
