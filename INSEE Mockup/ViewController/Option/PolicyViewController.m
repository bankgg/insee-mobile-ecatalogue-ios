//
//  PolicyViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 12/18/2556 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "PolicyViewController.h"

@interface PolicyViewController ()

@end

@implementation PolicyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    //labContent.text = @"abc";
    //[UICommon diffHeightFromAdjustSizeLabel:labContent byString:labContent.text];
    //[scrollView setContentSize:CGSizeMake(scrollView.frame.size.width, labContent.frame.size.height + labContent.frame.origin.y *2)];
    
    webview.delegate = self;
    webview.backgroundColor = [UIColor clearColor];
    [webview setOpaque:NO];
    
//    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:IS_PAD?@"policy_tablet":@"policy_phone" ofType:@"html"];
//    NSString *htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
//    [webview loadHTMLString:htmlString baseURL:nil];
    
    NSString *pathString = [NSString stringWithFormat:IS_PAD?URL_INSEE_POLICY_TABLET:URL_INSEE_POLICY_PHONE, [[NSUserDefaults standardUserDefaults] objectForKey:@"user_language"]];
    NSString *urlString = [Global PATH_OF:pathString];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *req = [[NSURLRequest alloc] initWithURL:url];
    [webview loadRequest:req];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - WebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [hud hide:YES];
}

@end
