//
//  ProjectRefDetailViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/4/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "ProjectRefDetailViewController.h"
#import "ProductDetail2ViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "UICommon.h"
#import "UIImageView+Ext.h"
#import "UIView+Extension.h"
#import "UIImageExtension.h"

#import "Global.h"
#import "Stat.h"

#import "AFHTTPRequestOperationManager.h"
#import "CJSONDeserializer.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "JustAlert.h"
#import "MWPhotoBrowser.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

@interface ProjectRefDetailViewController () < UICollectionViewDataSource, UICollectionViewDelegate, MWPhotoBrowserDelegate >
{
    NSMutableArray *listProduct;
    NSMutableArray *listImage;
    NSDictionary *obj;
    
    NSMutableArray *photos;
}
@property (weak, nonatomic) IBOutlet UIView *viewTableTop;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewMain;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDesc;
@property (weak, nonatomic) IBOutlet UILabel *labelContent;
@property (weak, nonatomic) IBOutlet UIView *viewTopContent;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation ProjectRefDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    listProduct = [NSMutableArray array];
    listImage = [NSMutableArray array];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load Data

-(void)loadData
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *servicePath = [NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_PROJECT_DETAIL], [self.obj_id intValue]];
    NSLog(@"servicePath = %@", servicePath);
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:servicePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSDictionary *responseDict = (id)responseObject;
         obj = responseDict[@"project"];
         listProduct = responseDict[@"project_product"];
         listImage = responseDict[@"project_image"];
         
         if ([listProduct isEqual:[NSNull null]]) {
             listProduct = [NSMutableArray array];
         }
         else
         {
             NSMutableArray *listProduct_n = [NSMutableArray array];
             for (NSDictionary *prodDict in listProduct)
             {
                 NSMutableDictionary *prodDictMutable = [prodDict mutableCopy];
                 
                 //row height
                 UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
                 
                 UILabel *labelTitle = (id)[cell viewWithTag:2000];
                 labelTitle.text = prodDict[[@"product_name" languageField]];
                 [labelTitle sizeToFit2];
                 
                 UILabel *labelDesc = (id)[cell viewWithTag:3000];
                 labelDesc.text = prodDict[[@"product_desc" languageField]];
                 [labelDesc sizeToFit2];
                 [labelDesc setFramePosY:labelTitle.posOfEndY + 0];
                 
                 float row_height = MAX(labelDesc.posOfEndY + 10, IS_PAD?110:100);
                 [prodDictMutable setObject:[NSNumber numberWithFloat:row_height] forKey:@"row_height"];
                 
                 [listProduct_n addObject:prodDictMutable];
             }
             listProduct = listProduct_n;
         }
         
         if ([listImage isEqual:[NSNull null]]) {
             listImage = [NSMutableArray array];
         }
         
         self.labelTitle.text = obj[[@"project_title" languageField]];
         self.labelDesc.text = obj[[@"project_desc" languageField]];
         self.labelContent.text = obj[[@"project_content" languageField]];
         
         NSURL *imgURL = [Global imageURLFromPath:IS_PAD?obj[@"project_image_tablet_path"]:obj[@"project_image_phone_path"]];
         
         self.imageViewMain.alpha = 0;
         [self.imageViewMain sd_setImageWithURL:imgURL placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
             
             if (image)
             {
                 [self.imageViewMain setAndScaleToFixWidthOfImage:image];
                 [self setLayoutHeaderView];
                 
                 [UIView animateWithDuration:0.3 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                     self.imageViewMain.alpha = 1.0;
                 } completion:nil];
             }
             
         }];
         
//         [self.imageViewMain setImageFromImagePathPhone:obj[@"project_image_phone_path"] andImagePathTablet:obj[@"project_image_tablet_path"] scaleToImageViewWidth:YES];
         [self setLayoutHeaderView];
         
         //Full size photo set for MWPhotoBrowser
         photos = [NSMutableArray array];
         if (listImage)
         {
             for (int i=0;i<listImage.count;i++)
             {
                 NSDictionary *imageDict = [listImage objectAtIndex:i];
                 MWPhoto *photo = [MWPhoto photoWithURL:[Global imageURLFromPath:imageDict[@"project_image_lg_path"]]];
                 [photos addObject:photo];
             }
             [self.collectionView reloadData];
         }
         
         [self.tableView reloadData];
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         
         //stat
         [Stat saveStatOfPageType:pageTypeProjectRefDetail pageName:obj[[@"project_title" languageField]] withAction:@"view"];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
     }];
}

-(void)setLayoutHeaderView
{
    [self.labelTitle sizeToFit2];
    [self.labelTitle setFramePosY:self.imageViewMain.posOfEndY + 15];
    
    [self.labelDesc sizeToFit2];
    [self.labelDesc setFramePosY:self.labelTitle.posOfEndY + 5];
    
    [self.labelContent sizeToFit2];
    [self.labelContent setFramePosY:self.labelDesc.posOfEndY + 10];
    
//    [self.viewTopContent setFrameHeight:self.labelContent.posOfEndY + 15];
    self.viewTopContent.frame = CGRectMake(self.viewTopContent.frame.origin.x, self.viewTopContent.frame.origin.y, self.viewTopContent.frame.size.width, self.labelContent.posOfEndY + 15);
    
    if (listImage)
    {
        [self.collectionView setFramePosY:self.viewTopContent.posOfEndY + 10];
        
        int collectionNumColumns = (IDIOM == IPAD)?5:3;
        int collectionNumRows = ((int)listImage.count + collectionNumColumns - 1)/collectionNumColumns;
        
        UICollectionViewFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
        float collectionViewHeight = layout.sectionInset.top + (layout.itemSize.height * collectionNumRows) + layout.sectionInset.bottom + (layout.minimumLineSpacing * (collectionNumRows - 1));
//        [self.collectionView setFrameHeight:collectionViewHeight];
        self.collectionView.frame = CGRectMake(self.collectionView.frame.origin.x, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, collectionViewHeight);
        
//        [self.viewTableTop setFrameHeight:self.collectionView.posOfEndY + 15];
        self.viewTableTop.frame = CGRectMake(self.viewTableTop.frame.origin.x, self.viewTableTop.frame.origin.y, self.viewTableTop.frame.size.width, self.collectionView.posOfEndY + 15);
        self.tableView.tableHeaderView = self.viewTableTop;
    }
    else
    {
//        [self.viewTableTop setFrameHeight:self.viewTopContent.posOfEndY + 15];
        self.viewTableTop.frame = CGRectMake(self.viewTableTop.frame.origin.x, self.viewTableTop.frame.origin.y, self.viewTableTop.frame.size.width, self.viewTopContent.posOfEndY + 15);
        self.tableView.tableHeaderView = self.viewTableTop;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return (listProduct.count > 0)?1:0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [listProduct count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary *dict = [listProduct objectAtIndex:indexPath.item];
    
    UILabel *labelTitle = (id)[cell viewWithTag:2000];
    labelTitle.text = dict[[@"product_name" languageField]];
    [labelTitle sizeToFit2];
    
    UILabel *labelDesc = (id)[cell viewWithTag:3000];
    labelDesc.text = dict[[@"product_desc" languageField]];
    [labelDesc sizeToFit2];
    [labelDesc setFramePosY:labelTitle.posOfEndY + 0];
    
    UIImageView *imageView = (id)[cell viewWithTag:1000];
    [imageView setImageFromImagePathPhone:dict[@"product_image_sm_phone_path"] andImagePathTablet:dict[@"product_image_sm_tablet_path"]];
    [imageView setFrameCenterY:imageView.superview.center.y];
    
    UIView *frameView = (id)[cell viewWithTag:10000];
//    [frameView setFrameHeight:[dict[@"row_height"] floatValue] - 1];
    frameView.frame = CGRectMake(frameView.frame.origin.x, frameView.frame.origin.y, frameView.frame.size.width, [dict[@"row_height"] floatValue] - 1);
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier = @"sectionHeader";
    UITableViewCell *headerView = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    UILabel *labelHeader = (id)[headerView viewWithTag:1000];
    labelHeader.text = LocalizedString(@"proj_related_product");
    
    headerView.backgroundColor = [UIColor clearColor];

    return headerView;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 45;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [listProduct objectAtIndex:indexPath.item];
    
    return [dict[@"row_height"] floatValue];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dict = [listProduct objectAtIndex:indexPath.row];
    
    ProductDetail2ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductDetail2ViewController"];
    vc.product_id = [dict[@"product_id"] intValue];
    
    [self.navigationController pushViewController:vc animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - CollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return listImage.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary *dict = [listImage objectAtIndex:indexPath.item];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1000];
    [imageView setImageFromImagePathPhone:dict[@"project_image_sm_path"] andImagePathTablet:dict[@"project_image_sm_path"]];
    
    [UICommon setBorder:cell];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    [browser setCurrentPhotoIndex:indexPath.item];
    
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:browser];
//    nav.navigationBar.barStyle = UIBarStyleBlack;
//    [self presentViewController:nav animated:YES completion:nil];
    
    [self.navigationController pushViewController:browser animated:YES];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return listImage.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

@end
