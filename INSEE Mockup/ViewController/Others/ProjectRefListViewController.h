//
//  ProjectRefListViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/4/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DefaultTableViewController.h"
#import "Global.h"
#import "AFHTTPRequestOperationManager.h"
#import "CJSONDeserializer.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "JustAlert.h"

#define NUM_ROW_LOAD 10

@interface ProjectRefListViewController : UITableViewController
{
    NSMutableArray *listContent;
    int offset;
    bool isLoading;
    bool isMoreData;
    
    Reachability *reach;
    
    UIRefreshControl *refreshControl;
}
@end
