//
//  ProjectRefListViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/4/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "ProjectRefListViewController.h"
#import "Stat.h"

#import "UICommon.h"
#import "ProjectRefDetailViewController.h"

#import "UIImageView+Ext.h"
#import "UIView+Extension.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

@interface ProjectRefListViewController ()

@end

@implementation ProjectRefListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.tableView.backgroundView = nil;
    
    self.title = LocalizedString(@"proj_title");
    
    offset = 0;
    if (listContent == nil) {
        listContent = [NSMutableArray array];
    }
    [self loadData];
    
    //RefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    [self setRefreshControl:refreshControl];
    
    //stat
    [Stat saveStatOfPageType:pageTypeProjectRefList pageName:@"" withAction:@"view"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load Data

-(void)loadData
{
    if (isLoading) {
        return;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    isLoading = YES;
    
    NSString *servicePath = [NSString stringWithFormat:[Global PATH_OF:S_PATH_SERVICE_PROJECT_LIST], offset, NUM_ROW_LOAD];
    NSLog(@"servicePath = %@", servicePath);
    
    AFHTTPRequestOperationManager *manager = [Global afManager];
    [manager GET:servicePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         NSArray *responseArray = (NSArray *)responseObject;
         for (id obj in responseArray)
         {
             NSMutableDictionary *dict = (id)[obj mutableCopy];
             
             //row height
             UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
             
             UIView *viewTitleFrame = (UIView *)[cell viewWithTag:15000];
             
             UILabel *labelTitle = (UILabel *)[cell viewWithTag:2000];
             labelTitle.text = obj[[@"project_title" languageField]];
             [labelTitle sizeToFit2];
             
             UILabel *labelDesc = (UILabel *)[cell viewWithTag:3000];
             labelDesc.text = obj[[@"project_desc" languageField]];
             [labelDesc sizeToFit2];
             [labelDesc setFramePosY:labelTitle.posOfEndY + 0];
             
//             [viewTitleFrame setFrameHeight:labelDesc.posOfEndY + 10];
             viewTitleFrame.frame = CGRectMake(viewTitleFrame.frame.origin.x, viewTitleFrame.frame.origin.y, viewTitleFrame.frame.size.width, labelDesc.posOfEndY + 10);
             
             float row_height = viewTitleFrame.posOfEndY + 8;
             [dict setObject:[NSNumber numberWithFloat:row_height] forKey:@"row_height"];
             
             
             [listContent addObject:dict];
         }
         if ([responseArray count] == NUM_ROW_LOAD) {
             isMoreData = YES;
         }
         else {
             isMoreData = NO;
         }
         if ([responseArray count] > 0) {
             offset += NUM_ROW_LOAD;
         }
         
         [self.tableView reloadData];
         
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [refreshControl endRefreshing];
         isLoading = NO;
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"Error1: %@", error);
         [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
         [refreshControl endRefreshing];
         isLoading = NO;
     }];
}

-(void)reloadData
{
    //clear index and array
    offset = 0;
    listContent = nil;
    listContent = [NSMutableArray array];
    
    [self loadData];
    //[self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [listContent count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary *obj = [listContent objectAtIndex:indexPath.row];
    
    UIView *viewFrame = (UIView *)[cell viewWithTag:10000];
    [UICommon setBorder:viewFrame];

    UIView *viewTitleFrame = (UIView *)[cell viewWithTag:15000];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1000];
    [imageView setImageFromImagePathPhone:obj[@"project_image_phone_path"] andImagePathTablet:obj[@"project_image_tablet_path"]];
    
    UILabel *labelTitle = (UILabel *)[cell viewWithTag:2000];
    labelTitle.text = obj[[@"project_title" languageField]];
    [labelTitle sizeToFit2];

    UILabel *labelDesc = (UILabel *)[cell viewWithTag:3000];
    labelDesc.text = obj[[@"project_desc" languageField]];
    [labelDesc sizeToFit2];
    [labelDesc setFramePosY:labelTitle.posOfEndY + 0];
    
//    [viewTitleFrame setFrameHeight:labelDesc.posOfEndY + 10];
    viewTitleFrame.frame = CGRectMake(viewTitleFrame.frame.origin.x, viewTitleFrame.frame.origin.y, viewTitleFrame.frame.size.width, labelDesc.posOfEndY + 10);
//    [viewFrame setFrameHeight:viewTitleFrame.posOfEndY];
    viewFrame.frame = CGRectMake(viewFrame.frame.origin.x, viewFrame.frame.origin.y, viewFrame.frame.size.width, viewTitleFrame.posOfEndY);
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *obj = [listContent objectAtIndex:indexPath.row];
    return [obj[@"row_height"] floatValue];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *obj = [listContent objectAtIndex:indexPath.row];
    
    ProjectRefDetailViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProjectRefDetailViewController"];
    vc.obj_id = obj[@"id"];
    vc.title = obj[[@"project_title" languageField]];
    [self.navigationController pushViewController:vc animated:YES];
}

//check position of scroll of tableview to load more data
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSInteger currentOffset = scrollView.contentOffset.y;
    NSInteger maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    
    if (maximumOffset - currentOffset <= 40)
    {
        if (!isLoading && isMoreData)
        {
            //offset += NUM_ROW_LOAD;
            [self loadData];
            NSLog(@"load more news");
        }
    }
}

@end
