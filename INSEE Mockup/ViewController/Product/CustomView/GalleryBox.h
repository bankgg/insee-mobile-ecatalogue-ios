//
//  GalleryBox.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/12/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"

@interface GalleryBox : UIView

@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

-(void)initial;
-(void)setImage:(UIImage *)image;

@end
