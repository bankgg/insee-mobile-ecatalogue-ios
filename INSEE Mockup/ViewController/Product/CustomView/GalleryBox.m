//
//  GalleryBox.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/12/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "GalleryBox.h"
#import "UICommon.h"

@implementation GalleryBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initial
{
    [UICommon setBorder:self.button];
}

-(void)setImage:(UIImage *)image
{
    [self.imageView setImage:image];
    [self.imageView setClipsToBounds:YES];
}

@end
