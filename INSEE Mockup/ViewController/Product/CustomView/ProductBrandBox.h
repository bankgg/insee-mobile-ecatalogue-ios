//
//  ProductBrandBox.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/10/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "ProductCat.h"
#import "UIImageView+WebCache.h"

@interface ProductBrandBox : UIView
{
    
}
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (nonatomic, retain) id userData;

-(void)initial;

+(float)viewHeight;
+(float)viewWidth;

-(void)setCat:(NSDictionary *)dict;

@end
