//
//  ProductBrandBox.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/10/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ProductBrandBox.h"
#import "UICommon.h"
#import "UIImageView+Ext.h"

@implementation ProductBrandBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initial
{
    [UICommon setBorder:self.button];
}

-(void)setCat:(NSDictionary *)dict
{
    [[self.button imageView] setContentMode:UIViewContentModeScaleAspectFit];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.button.frame.size.width, self.button.frame.size.height)];
    [imgView setImageFromImagePathPhone:dict[@"product_cat_image_phone_path"] andImagePathTablet:dict[@"product_cat_image_tablet_path"]];
    
    [self.button addSubview:imgView];
}

+(float)viewHeight
{
    float height = 150;
    if (IDIOM != IPAD)
    {
        height = 360;
    }
    return height;
}
+(float)viewWidth
{
    float width = 150;
    if (IDIOM != IPAD)
    {
        width = 360;
    }
    return width;
}

@end
