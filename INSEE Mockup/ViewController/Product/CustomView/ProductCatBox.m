//
//  ProductCatBox.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/11/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ProductCatBox.h"
#import "UICommon.h"

@implementation ProductCatBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)initial
{
    self.labelMain.font = [UIFont fontWithName:@"INSEE" size:23.0f];
    self.labelDetail.font = [UIFont fontWithName:@"INSEE" size:16.0f];
    
    [UICommon setBorder:self];
}

-(void)setCat:(ProductCat *)cat
{
    self.labelMain.text = cat.cat_name;
    self.labelDetail.text = cat.cat_desc;
}

@end
