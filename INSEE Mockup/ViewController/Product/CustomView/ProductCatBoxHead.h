//
//  ProductCatBoxHead.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/12/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductCat.h"
#import "Global.h"

@interface ProductCatBoxHead : UIView

@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UILabel *labDesc;

-(void)initial;
-(void)setCat:(NSDictionary *)catDict;
+(float)viewHeight;

@end
