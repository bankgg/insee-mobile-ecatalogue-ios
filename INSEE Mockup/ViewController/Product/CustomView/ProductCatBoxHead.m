//
//  ProductCatBoxHead.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/12/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ProductCatBoxHead.h"
#import "UICommon.h"
#import "NSString+Language.h"
#import "UIView+Extension.h"

@implementation ProductCatBoxHead

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initial
{
    [UICommon setBorder:self.viewFrame];
    
    if (IDIOM != IPAD)
    {
        self.labName.font = [UIFont fontWithName:@"INSEE" size:23.0f];
        self.labDesc.font = [UIFont fontWithName:@"INSEE" size:16.0f];
        [self setFrame:CGRectMake(0, 0, 320, 60)];
    }
    else
    {
        self.labName.font = [UIFont fontWithName:@"INSEE" size:30.0f];
        self.labDesc.font = [UIFont fontWithName:@"INSEE" size:20.0f];
        [self setFrame:CGRectMake(0, 0, 768, 80)];
    }
}

-(void)setCat:(NSDictionary *)catDict
{
    self.labName.text = catDict[[@"cat_name" languageField]];
    self.labDesc.text = catDict[[@"cat_desc" languageField]];
}

+(float)viewHeight
{
    float height = 60;
    if (IDIOM == IPAD)
    {
        height = 80;
    }
    return height;
}

@end
