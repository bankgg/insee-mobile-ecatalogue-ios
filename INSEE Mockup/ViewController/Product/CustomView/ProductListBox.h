//
//  ProductListBox.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/12/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "Product.h"
#import "UIImageView+WebCache.h"

@interface ProductListBox : UIView

@property (weak, nonatomic) IBOutlet UIView *viewFrame;
@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UILabel *labDesc;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

-(void)initial;
+(float)viewHeight;

-(void)setProduct:(NSDictionary *)productDict;

@end
