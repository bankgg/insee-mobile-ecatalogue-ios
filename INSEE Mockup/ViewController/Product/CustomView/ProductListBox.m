//
//  ProductListBox.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/12/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ProductListBox.h"
#import "UICommon.h"
#import "ManageData.h"
#import "HexColor.h"
#import "UIImageView+Ext.h"
#import "NSString+Language.h"

@implementation ProductListBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)initial
{
    //[UICommon setBorder:self.viewFrame];
    
    if (IDIOM != IPAD)
    {
        self.labName.font = [UIFont fontWithName:@"INSEE" size:23.0f];
        self.labDesc.font = [UIFont fontWithName:@"INSEE" size:16.0f];
        [self setFrame:CGRectMake(0, 0, 320, 78)];
    }
    else
    {
        self.labName.font = [UIFont fontWithName:@"INSEE" size:30.0f];
        self.labDesc.font = [UIFont fontWithName:@"INSEE" size:22.0f];
        [self setFrame:CGRectMake(0, 0, 768, 110)];
    }
}

+(float)viewHeight
{
    float height = 78;
    if (IDIOM == IPAD)
    {
        height = 110;
    }
    return height;
}

-(void)setProduct:(NSDictionary *)productDict
{
    self.labName.text = productDict[[@"product_name" languageField]];
    self.labDesc.text = productDict[[@"product_desc" languageField]];
    
    if ([self.labDesc.text isEqualToString:@""])
    {
        self.labName.numberOfLines = 2;
        [self.labName sizeToFit];
        
        self.labName.center = CGPointMake(self.labName.center.x, self.imageView.center.y);
    }
    
    
    if (![productDict[@"product_rgb"] isEqualToString:@""] && productDict[@"product_rgb"] != nil)
    {
        self.labName.textColor = [HexColor colorWithHexString:productDict[@"product_rgb"]];
    }
    
    [self.imageView setImageFromImagePathPhone:productDict[@"product_image_sm_phone_path"] andImagePathTablet:productDict[@"product_image_sm_tablet_path"]];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
}

@end
