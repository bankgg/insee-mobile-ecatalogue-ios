//
//  ProductDetail2ViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/21/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "ProductDetail2ViewController.h"
#import "LoginViewController.h"

#import "User.h"
#import "Stat.h"

#import "FMDatabase.h"
#import "ManageData.h"
#import "MBProgressHUD.h"
#import "UIImageView+Ext.h"
#import "UIView+Extension.h"
#import "MWPhotoBrowser.h"
#import "NSString+Ext.h"
#import "Reachability.h"
#import "DocViewerViewController.h"
#import "SIAlertView.h"
#import "HexColor.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

@interface ProductDetail2ViewController () <UIWebViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, MWPhotoBrowserDelegate>
{
    NSMutableDictionary *prodDict;
    NSMutableArray *imageArray;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *frameView;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewMain;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubTitle;
@property (weak, nonatomic) IBOutlet UIWebView *webViewContent;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *buttonDoc;

- (IBAction)buttonDocPressed:(id)sender;

@end

@implementation ProductDetail2ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.labelTitle.text = @"";
    self.labelSubTitle.text = @"";
    
    [self loadData];
    
    //stat
    [Stat saveStatOfPageType:pageTypeProductDetail pageName:prodDict[[@"product_name" languageField]] withAction:@"view"];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setFileButtonFromUserLogin];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)loadData
{
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        //Product Data
        FMResultSet *s = [db executeQueryWithFormat:@"SELECT * FROM product WHERE id = %d", self.product_id];
        if ([s next]) {
            prodDict = [[s resultDictionary] mutableCopy];
        }
        
        //Product Images
        imageArray = [NSMutableArray array];
        FMResultSet *si = [db executeQueryWithFormat:@"SELECT * FROM product_detail_image WHERE product_id = %d", self.product_id];
        while ([si next])
        {
            NSMutableDictionary *pd = [[si resultDictionary] mutableCopy];
            
            MWPhoto *photo = [MWPhoto photoWithURL:[Global imageURLFromPath:pd[@"product_image_lg_path"]]];
            [pd setObject:photo forKey:@"mwphoto"];
            
            [imageArray addObject:pd];
        }
        [self.collectionView reloadData];

        [db close];
    }
    
    if (prodDict)
    {
        [self.imageViewMain setImageFromImagePathPhone:prodDict[@"product_image_lg_phone_path"] andImagePathTablet:prodDict[@"product_image_lg_tablet_path"]];
        
        self.labelTitle.text = prodDict[[@"product_name" languageField]];
        [self.labelTitle sizeToFit2];
        
        UIColor *prodColor = [UIColor redColor];
        if (![NSString isNullOrEmpty:prodDict[@"product_rgb"]])
        {
            prodColor = [HexColor colorWithHexString:prodDict[@"product_rgb"]];
        }
        self.labelTitle.textColor = prodColor;
        
        self.labelSubTitle.text = prodDict[[@"product_desc" languageField]];
        [self.labelSubTitle sizeToFit2];
        [self.labelSubTitle setFramePosY:self.labelTitle.posOfEndY + 5];
        
        
        NSString *headerPath = [[NSBundle mainBundle] pathForResource:@"ContentHead" ofType:@"html"];
        NSString *headerContent = [NSString stringWithContentsOfFile:headerPath encoding:NSUTF8StringEncoding error:NULL];
        NSString *contentHtml = [headerContent stringByAppendingString:prodDict[[@"product_full_content" languageField]]];
        
        [self.webViewContent loadHTMLString:contentHtml baseURL:nil];
        self.webViewContent.delegate = self;
        [self.webViewContent setFramePosY:self.labelSubTitle.posOfEndY + 10];
        
        if (![NSString isNullOrEmpty:prodDict[[@"product_pdf_button" languageField]]]) {
            [self.buttonDoc setTitle:prodDict[[@"product_pdf_button" languageField]] forState:UIControlStateNormal];
        }
        else {
            [self.buttonDoc setTitle:LocalizedString(@"product_doc_btn_default") forState:UIControlStateNormal];
        }
    }
}

#pragma mark - WebView Delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    CGRect frame = webView.frame;
    NSString *output = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
    frame.size.height = [output floatValue];
    webView.frame = frame;
    
    //CollectionView Size
    [self.collectionView setFramePosY:webView.posOfEndY + 10];
    
    int collectionNumColumns = 3;
    int collectionNumRows = ((int)imageArray.count + collectionNumColumns - 1)/collectionNumColumns;
    
    UICollectionViewFlowLayout *layout = (id)self.collectionView.collectionViewLayout;
    float collectionViewHeight = layout.sectionInset.top + (layout.itemSize.height * collectionNumRows) + layout.sectionInset.bottom + (layout.minimumLineSpacing * (collectionNumRows - 1));
//    [self.collectionView setFrameHeight:collectionViewHeight];
    self.collectionView.frame = CGRectMake(self.collectionView.frame.origin.x, self.collectionView.frame.origin.y, self.collectionView.frame.size.width, collectionViewHeight);
    
    float frameViewHeight = 300;
    if ([NSString isNullOrEmpty:prodDict[@"product_pdf_path"]])
    {
        self.buttonDoc.hidden = YES;
        frameViewHeight = MAX(self.collectionView.posOfEndY, self.imageViewMain.posOfEndY) + 20;
//        [self.frameView setFrameHeight:MAX(self.collectionView.posOfEndY, self.imageViewMain.posOfEndY) + 20];
    }
    else
    {
        [self setFileButtonFromUserLogin];
        [self.buttonDoc setFramePosY:self.collectionView.posOfEndY + 20];
        frameViewHeight = MAX(self.buttonDoc.posOfEndY, self.imageViewMain.posOfEndY) + 20;
//        [self.frameView setFrameHeight:MAX(self.buttonDoc.posOfEndY, self.imageViewMain.posOfEndY) + 20];
        [self.buttonDoc makeItShadow];
        [self.frameView makeBorder];
    }
    
//    [self.frameView setFrameHeight:frameViewHeight];
    self.frameView.frame = CGRectMake(self.frameView.frame.origin.x, self.frameView.frame.origin.y, self.frameView.frame.size.width, frameViewHeight);
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width, self.frameView.posOfEndY + 10)];
}

#pragma mark - CollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return imageArray.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    NSDictionary *dict = [imageArray objectAtIndex:indexPath.item];
    
    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1000];
    [imageView setImageFromImagePathPhone:dict[@"product_image_sm_path"] andImagePathTablet:dict[@"product_image_sm_path"]];
    [imageView makeBorder];
    
    [cell makeBorder];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayActionButton = YES;
    [browser setCurrentPhotoIndex:indexPath.item];
    
    [self.navigationController pushViewController:browser animated:YES];
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return imageArray.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < imageArray.count)
    {
        NSDictionary *dict = [imageArray objectAtIndex:index];
        return dict[@"mwphoto"];
    }
    return nil;
}

#pragma mark - Document

- (IBAction)buttonDocPressed:(id)sender
{
    if ([User isUserLogin])
    {
        Reachability *_reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
        if (remoteHostStatus == NotReachable) {
            // not reachable
            SIAlertView *alert = [[SIAlertView alloc] initWithTitle:nil andMessage:LocalizedString(@"product_doc_warn_internet")];
            [alert show];
        }
        else {
            DocViewerViewController *docVC = [[DocViewerViewController alloc] initWithNibName:@"DocViewerViewController" bundle:nil];
            docVC.docURLPath = [NSString stringWithFormat:[Global PATH_OF:S_PATH_IMAGE], prodDict[@"product_pdf_path"]];
            
            docVC.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:docVC animated:YES completion:nil];
        }
    }
    else
    {
        SIAlertView *alert = [[SIAlertView alloc] initWithTitle:nil andMessage:LocalizedString(@"product_doc_warn_login")];
        [alert addButtonWithTitle:LocalizedString(@"cancel") type:SIAlertViewButtonTypeCancel handler:nil];
        [alert addButtonWithTitle:LocalizedString(@"ok") type:SIAlertViewButtonTypeDestructive handler:^(SIAlertView *alertView) {
            
            LoginViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LoginViewController"];
            [self presentViewController:vc animated:YES completion:nil];
        }];
        
        [alert show];
    }
}

-(void)setFileButtonFromUserLogin
{
    if (![User isUserLogin]) {
        self.buttonDoc.alpha = 0.5;
        [self.buttonDoc setBackgroundImage:[UIImage imageNamed:@"buttonGray"] forState:UIControlStateNormal];
    }
    else {
        self.buttonDoc.alpha = 1.0;
        [self.buttonDoc setBackgroundImage:[UIImage imageNamed:@"buttonRed"] forState:UIControlStateNormal];
    }
}


@end
