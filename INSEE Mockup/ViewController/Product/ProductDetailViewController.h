//
//  ProductDetailViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/11/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "Product.h"
#import "ProductDetail.h"
#import "ProductDetailImage.h"
#import "UIImageView+WebCache.h"
#import "MWPhotoBrowser.h"
#import "Stat.h"
#import "MBProgressHUD.h"
#import "Reachability.h"
#import "JustAlert.h"

#define TAG_CLEAR 101010

@interface ProductDetailViewController : UIViewController <UIActionSheetDelegate, MWPhotoBrowserDelegate, UIAlertViewDelegate>
{
    __weak IBOutlet UIScrollView *scroll;
    __weak IBOutlet UIView *viewBox;
    __weak IBOutlet UILabel *labName;
    __weak IBOutlet UILabel *labNameSub;
    __weak IBOutlet UIImageView *imageViewMain;
    
    __weak IBOutlet UIView *viewPicDummy1;
    
    IBOutlet UILabel *labHeadDummy;
    IBOutlet UILabel *labDetailDummy;
    
    IBOutlet UILabel *labHeadPicture;
    
    UIBarButtonItem *actButton;
    
    float diffY;
    
    __weak IBOutlet UIButton *tisButton;
    
    NSMutableArray *photos;
}

@property (nonatomic, retain) NSMutableArray *allProductArray;

- (void)callBackFromLoginView;

//NEW
@property int product_id;

@end
