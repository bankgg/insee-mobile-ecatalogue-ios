//
//  ProductDetailViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/11/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ProductDetailViewController.h"
#import "UICommon.h"
#import "GalleryBox.h"
#import "ImagesViewerViewController.h"
#import "DocViewerViewController.h"
#import "FMDatabase.h"
#import "ManageData.h"
#import "HexColor.h"
#import "LoginViewController.h"

#import "UIImageView+Ext.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

@interface ProductDetailViewController ()
{
    NSMutableDictionary *prodDict;
}
@end

@implementation ProductDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
	
    [UICommon setBorder:viewBox];
    [scroll setContentSize:CGSizeMake(320, 800)];
    
    [self putData];
    
    actButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonPressed:)];
    //self.navigationItem.rightBarButtonItem = actButton;
    
    //stat
    [Stat saveStatOfPageType:pageTypeProductDetail pageName:prodDict[@"product_name_en"] withAction:@"view"];
    
    /*
    //swipe left
    UISwipeGestureRecognizer *swipeGestureLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedLeft:)];
    swipeGestureLeft.numberOfTouchesRequired = 1;
    swipeGestureLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeGestureLeft];
    
    //swipe left
    UISwipeGestureRecognizer *swipeGestureRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipedRight:)];
    swipeGestureRight.numberOfTouchesRequired = 1;
    swipeGestureRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeGestureRight];
    */
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"scroll y = %f", scroll.frame.origin.y);
    NSLog(@"scroll content y = %f", scroll.contentOffset.y);
    
    //self.navigationController.navigationBar.translucent = NO;
    
    [self setFileButtonFromUserLogin];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)putData
{
    // Load Product Data
    FMDatabase *dbProd = [ManageData database];
    if ([dbProd open])
    {
        FMResultSet *sProd = [dbProd executeQuery:[NSString stringWithFormat:@" SELECT p.*, \
                                                                                    c.cat_name_en, c.cat_name_th \
                                                                                FROM product p \
                                                                                LEFT JOIN product_cat c ON c.id = p.product_cat_id \
                                                                                WHERE p.id = %d", self.product_id]];
        while ([sProd next])
        {
            prodDict = [[sProd resultDictionary] mutableCopy];
        }
    }
    
    
    NSMutableArray *prodDetails = [[NSMutableArray alloc] init];
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *sDet = [db executeQuery:[NSString stringWithFormat:@"SELECT * FROM product_detail WHERE product_id = %d", self.product_id]];
        while ([sDet next])
        {
            ProductDetail *det = [[ProductDetail alloc] init];
            det.header = [sDet stringForColumn:@"header"];
            det.detail = [sDet stringForColumn:@"detail"];
            [prodDetails addObject:det];
        }
        prodDict[@"prodDetails"] = [NSArray arrayWithArray:prodDetails];
    }
    else
    {
        NSLog(@"cannot open database");
    }
    [db close];
    
    
    //Main Image
    [imageViewMain setImageFromImagePathPhone:prodDict[@"product_image_lg_phone_path"] andImagePathTablet:prodDict[@"product_image_lg_tablet_path"]];
    
    self.title = prodDict[@"cat_name_th"];
    
    UIFont *h1 = [UIFont fontWithName:@"INSEE" size:34.0f];
    UIFont *h2 = [UIFont fontWithName:@"INSEE" size:22.0f];
    UIFont *p1 = [UIFont fontWithName:@"INSEEText" size:16.0f];
    
    if (IDIOM == IPAD)
    {
        h1 = [UIFont fontWithName:h1.fontName size:40.0f];
        h2 = [UIFont fontWithName:h2.fontName size:26.0f];
        p1 = [UIFont fontWithName:p1.fontName size:20.0f];
        
        tisButton.titleLabel.font = h2;
    }
    
    labName.font = h1;
    labNameSub.font = h2;
    labHeadDummy.font = h2;
    labDetailDummy.font = h2;
    labDetailDummy.font = p1;
    labHeadPicture.font = h2;
    
    UIColor *prodColor = [UIColor redColor];
    if (prodDict[@"product_rgb"] != nil && ![prodDict[@"product_rgb"] isEqualToString:@""])
    {
        prodColor = [HexColor colorWithHexString:prodDict[@"product_rgb"]];
    }
    labName.textColor = labHeadDummy.textColor = labHeadPicture.textColor = prodColor;
    
    
    diffY = 0;
    
    //NAME
    
    diffY += [UICommon diffHeightFromAdjustSizeLabel:labName byString:prodDict[@"product_name_en"]];
    [UICommon adjustPositionOfControl:labNameSub byDiffHeight:diffY];
    diffY += [UICommon diffHeightFromAdjustSizeLabel:labNameSub byString:prodDict[@"product_desc_en"]];
    
    //CONTENT
    
    labHeadDummy.hidden = YES;
    labDetailDummy.hidden = YES;
    
    float yBottom = labNameSub.frame.origin.y + labNameSub.frame.size.height;
    float space = IS_PAD?20.0:10.0;
    float spaceDetail = IS_PAD?7.0:5.0;
    float labX = labHeadDummy.frame.origin.x;
    float labWidth = labHeadDummy.frame.size.width;
    
    for (int i=0; i<[prodDict[@"prodDetails"] count]; i++)
    {
        ProductDetail *det = [prodDict[@"prodDetails"] objectAtIndex:i];
        
        UILabel *labHead = [[UILabel alloc] init];
        [labHead setFrame:CGRectMake(labX, yBottom + space, labWidth, labHeadDummy.frame.size.height)];
        labHead.font = labHeadDummy.font;
        labHead.textColor = labHeadDummy.textColor;
        [viewBox addSubview:labHead];
        
        [UICommon diffHeightFromAdjustSizeLabel:labHead byString:det.header];
        yBottom = labHead.frame.origin.y + labHead.frame.size.height;

        
        UILabel *labDetail = [[UILabel alloc] init];
        [labDetail setFrame:CGRectMake(labX, yBottom + spaceDetail, labWidth, labDetailDummy.frame.size.height)];
        labDetail.font = labDetailDummy.font;
        labDetail.textColor = labDetailDummy.textColor;
        labDetail.lineBreakMode = NSLineBreakByWordWrapping;
        labDetail.numberOfLines = 0;
        [viewBox addSubview:labDetail];
        
        [UICommon diffHeightFromAdjustSizeLabel:labDetail byString:det.detail];
        yBottom = labDetail.frame.origin.y + labDetail.frame.size.height;
        
        labHead.tag = TAG_CLEAR;
        labDetail.tag = TAG_CLEAR;
    }
    
    //PICTURES =====================================================================
    
    //LOAD PRODUCT_DETAIL_IMAGE
    NSMutableArray *images = [[NSMutableArray alloc] init];
    FMDatabase *dbp = [ManageData database];
    if ([dbp open])
    {
        FMResultSet *sDet = [dbp executeQuery:[NSString stringWithFormat:@"SELECT * FROM product_detail_image WHERE product_id = %d", self.product_id]];
        while ([sDet next])
        {
            ProductDetailImage *pdImg = [[ProductDetailImage alloc] init];
            pdImg.product_image_sm_path = [sDet stringForColumn:@"product_image_sm_path"];
            pdImg.product_image_lg_path = [sDet stringForColumn:@"product_image_lg_path"];
            
            [images addObject:pdImg];
        }
        prodDict[@"prodImages"] = [NSArray arrayWithArray:images];
    }
    else
    {
        NSLog(@"cannot open database");
    }
    [dbp close];
    
    if ([images count] > 0)
    {
        labHeadPicture.hidden = NO;
        CGRect labPicFrame = labHeadPicture.frame;
        labPicFrame.origin.y = yBottom + space;
        labHeadPicture.frame = labPicFrame;
        yBottom = labPicFrame.origin.y + labPicFrame.size.height;
        
        CGRect viewPicDummyFrame = viewPicDummy1.frame;
        viewPicDummyFrame.origin.y = yBottom + space;
        viewPicDummy1.frame = viewPicDummyFrame;
        viewPicDummy1.hidden = YES;
        
        int column = IS_PAD?4:3;
        float picSpace = 15.0f;
        for (int i=0;i<[prodDict[@"prodImages"] count];i++)
        {
            ProductDetailImage *pdImg = [prodDict[@"prodImages"] objectAtIndex:i];
            
            NSString *imgPath = pdImg.product_image_sm_path;
            NSString *imgURLString = [NSString stringWithFormat:[Global PATH_OF:S_PATH_IMAGE], imgPath];
            NSURL *imgURL = [NSURL URLWithString:imgURLString];
            
            GalleryBox *galBox = [[[NSBundle mainBundle] loadNibNamed:@"GalleryBox" owner:self options:nil] objectAtIndex:0];
            [galBox initial];
            [galBox.imageView sd_setImageWithURL:imgURL placeholderImage:[UIImage imageNamed:@"placeholder"]];
            [galBox.imageView setClipsToBounds:YES];
            galBox.tag = TAG_CLEAR;
            
            float xPos = viewPicDummy1.frame.origin.x + ((i%column) * (viewPicDummy1.frame.size.width + picSpace)); //+ (picSpace * i);
            float yPos = viewPicDummy1.frame.origin.y + ((int)floor((float)i/(float)column) * (viewPicDummy1.frame.size.height + picSpace));// + (picSpace * i);
            
            galBox.frame = CGRectMake(xPos, yPos, viewPicDummy1.frame.size.width, viewPicDummy1.frame.size.height);
            [viewBox addSubview:galBox];
            
            //[UICommon setFakeShadow:galBox];
            
            galBox.button.tag = i;
            [galBox.button addTarget:self action:@selector(galPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            yBottom = galBox.frame.origin.y + galBox.frame.size.height;
        }
        
        //Full size photo set for MWPhotoBrowser
        photos = [NSMutableArray array];
        for (int i=0;i<[prodDict[@"prodImages"] count];i++)
        {
            ProductDetailImage *pdImg = [prodDict[@"prodImages"] objectAtIndex:i];
            
            NSString *imgPath = pdImg.product_image_lg_path;
            NSString *imgURLString = [NSString stringWithFormat:[Global PATH_OF:S_PATH_IMAGE], imgPath];
            NSURL *imgURL = [NSURL URLWithString:imgURLString];
            
            [photos addObject:[MWPhoto photoWithURL:imgURL]];
        }
    }
    else
    {
        labHeadPicture.hidden = YES;
        viewPicDummy1.hidden = YES;
    }
    
    //FILE BUTTON
    if (prodDict[@"product_pdf_path"] != nil && ![prodDict[@"product_pdf_path"] isEqualToString:@""])
    {
        [self setFileButtonFromUserLogin];
        
        [tisButton addTarget:self action:@selector(openDocument) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect tisFrame = tisButton.frame;
        tisFrame.origin.y = yBottom + space*2;
        tisButton.frame = tisFrame;
        
        [UICommon setShadow:tisButton];
        
        yBottom = tisFrame.origin.y + tisFrame.size.height;
        
        if (prodDict[@"product_pdf_button_en"] != nil && ![prodDict[@"product_pdf_button_en"] isEqualToString:@""])
        {
            tisButton.hidden = NO;
            tisButton.titleLabel.textAlignment = NSTextAlignmentCenter;
            //tisButton.titleLabel.text = self.prod.product_pdf_button;
            [tisButton setTitle:prodDict[@"product_pdf_button_en"] forState:UIControlStateNormal];
        }
    }
    else
    {
        tisButton.hidden = YES;
    }
    
    [viewBox setFrame:CGRectMake(viewBox.frame.origin.x, viewBox.frame.origin.y, viewBox.frame.size.width, yBottom + space *2 )];
    [scroll setContentSize:CGSizeMake(self.view.frame.size.width, viewBox.frame.size.height + viewBox.frame.origin.y * 2)];
    
    NSLog(@"frame y = %f", self.view.frame.origin.y);
    NSLog(@"scroll y = %f", scroll.frame.origin.y);
    NSLog(@"scroll content y = %f", scroll.contentOffset.y);
}

-(void)setFileButtonFromUserLogin
{
    if (![User isUserLogin]) {
        tisButton.alpha = 0.5;
        [tisButton setBackgroundImage:[UIImage imageNamed:@"buttonGray"] forState:UIControlStateNormal];
    }
    else {
        tisButton.alpha = 1.0;
        [tisButton setBackgroundImage:[UIImage imageNamed:@"buttonRed"] forState:UIControlStateNormal];
    }
}

-(void)galPressed:(id)sender
{
    UIButton *bu = (UIButton *)sender;
    
    // Create & present browser
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    [browser setCurrentPhotoIndex:bu.tag]; // Example: allows second image to be presented first
    //browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
    // Present
    //self.navigationController.navigationBar.translucent = YES;
    [self.navigationController pushViewController:browser animated:YES];
    
    // Manipulate!
    //[browser showPreviousPhotoAnimated:YES];
    //[browser showNextPhotoAnimated:YES];
    
    /*
    ImagesViewerViewController *imageVC = [[ImagesViewerViewController alloc] initWithNibName:@"ImagesViewerViewController" bundle:nil];
    
    NSMutableArray *imgURLs = [[NSMutableArray alloc] init];
    for (int i=0;i<[self.prod.prodImages count];i++)
    {
        ProductDetailImage *pdImg = [self.prod.prodImages objectAtIndex:i];
        
        NSString *imgPath = pdImg.product_image_sm_path;
        NSString *imgURLString = [NSString stringWithFormat:PATH_IMAGE, imgPath];
        NSURL *imgURL = [NSURL URLWithString:imgURLString];
    
        [imgURLs addObject:imgURL];
    }
    
    imageVC.arrayImages = [NSArray arrayWithArray:imgURLs];
    imageVC.selectedIndex = bu.tag;
    imageVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:imageVC animated:YES completion:nil];
    */
}

-(void)actionButtonPressed:(id)sender
{
    UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Open มอก. file", nil];
    [actSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
    [actSheet showFromBarButtonItem:actButton animated:YES];
    //[actSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self openDocument];
        
        //Stat
        [Stat saveStatOfPageType:pageTypeProductDetail pageName:prodDict[@"product_name_en"] withAction:@"action document"];
    }
}

- (void)openDocument
{
    if ([User isUserLogin])
    {
        Reachability *_reachability = [Reachability reachabilityForInternetConnection];
        NetworkStatus remoteHostStatus = [_reachability currentReachabilityStatus];
        if (remoteHostStatus == NotReachable) {
            // not reachable
            [JustAlert alertWithMessage:@"Please connect to the Internet to download the document."];
        }
        else {
            DocViewerViewController *docVC = [[DocViewerViewController alloc] initWithNibName:@"DocViewerViewController" bundle:nil];
            docVC.docURLPath = [NSString stringWithFormat:[Global PATH_OF:S_PATH_IMAGE], prodDict[@"product_pdf_path"]];
            
            docVC.modalPresentationStyle = UIModalPresentationFullScreen;
            [self presentViewController:docVC animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertView *alertV = [[UIAlertView alloc] initWithTitle:@"Login Required" message:@"Please login to download the document." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alertV show];
    }
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;
}

#pragma mark - SwipeGesture

- (void)swipedLeft:(id)sender
{
    //go to next product
    NSLog(@"swiped left");
    
    for (int i=0; i<[self.allProductArray count]; i++)
    {
        NSMutableDictionary *cProd = [self.allProductArray objectAtIndex:i];
        if ([prodDict[@"id"] isEqualToString:cProd[@"id"]])
        {
            int nextIndex = i+1;
            if (nextIndex < [self.allProductArray count])
            {
                [self removeControlTagClear];
                prodDict = [[self.allProductArray objectAtIndex:nextIndex] mutableCopy];
                [self putData];
                [scroll setContentOffset:CGPointZero animated:YES];
            }
            break;
        }
    }
}

- (void)swipedRight:(id)sender
{
    //go to next product
    NSLog(@"swiped right");
    
    for (int i=(int)self.allProductArray.count-1; i>=0; i--)
    {
        NSMutableDictionary *cProd = [self.allProductArray objectAtIndex:i];
        if ([prodDict[@"id"] isEqualToString:cProd[@"id"]])
        {
            int prevIndex = i-1;
            if (prevIndex < [self.allProductArray count])
            {
                [self removeControlTagClear];
                prodDict = [[self.allProductArray objectAtIndex:prevIndex] mutableCopy];
                [self putData];
                [scroll setContentOffset:CGPointZero animated:YES];
            }
            break;
        }
    }
}

- (void)removeControlTagClear
{
    for (UIView *view in viewBox.subviews)
    {
        if (view.tag == TAG_CLEAR) {
            [view removeFromSuperview];
        }
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        LoginViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}


@end