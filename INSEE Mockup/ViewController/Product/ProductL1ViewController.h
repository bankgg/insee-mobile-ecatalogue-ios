//
//  ProductL1ViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/9/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "News.h"
#import "LanguageUITableViewController.h"

typedef enum
{
    productListModeNormal = 1,
    productListModePickList = 2
}ProductListMode;

@interface ProductL1ViewController : LanguageUITableViewController
{
    NSArray *listContent;
    NSMutableArray *filteredListContent;
}

-(void)reloadData;

@property ProductListMode listMode;
@property (nonatomic, retain) id calculationVC;

@end
