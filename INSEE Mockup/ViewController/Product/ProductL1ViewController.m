//
//  ProductL1ViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/9/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ProductL1ViewController.h"

#import "UICommon.h"
#import "CJSONDeserializer.h"
#import <QuartzCore/QuartzCore.h>

#import "AlignButton.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

#import "User.h"
#import "ManageData.h"
#import "ProductCat.h"
#import "ProductBrandBox.h"
#import "ProductCatBoxHead.h"
#import "ProductListBox.h"
#import "NSString+Language.h"
#import "UIImageView+Ext.h"
#import "UIView+Extension.h"
#import "HexColor.h"

#import "LoginViewController.h"
#import "ProductL2ViewController.h"
#import "ProductLMidViewController.h"
#import "ProductDetailViewController.h"
#import "OptionViewController.h"
#import "CalculationViewController.h"

@interface ProductL1ViewController () <UISearchBarDelegate, UISearchDisplayDelegate>
{
    UIRefreshControl *refreshControl;
}
@end

@implementation ProductL1ViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Because this is first view of the app
    /*[[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(runFirstViewPart)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];*/
    
    if (self.listMode != productListModePickList) {
        [self runFirstViewPart];
    }
    
    //load data
    [self loadData];
    
    //filteredListContent
    if (filteredListContent == nil) {
        filteredListContent = [NSMutableArray array];
    }
    
    //RefreshControl
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(reloadData) forControlEvents:UIControlEventValueChanged];
    [self setRefreshControl:refreshControl];
}

- (void)languageLoad
{
    self.title = LocalizedString(@"product_cat_title");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadData];
    
    //search tableview
    self.searchDisplayController.searchResultsTableView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:1.0];
    self.searchDisplayController.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    float searchTableHeaderHeight = IS_PAD?20.0:10.0;
    self.searchDisplayController.searchResultsTableView.tableHeaderView = [UICommon blankHeaderViewAtHeight:searchTableHeaderHeight];
}

#pragma mark - FIRST VIEW PART
- (void)runFirstViewPart
{
    if (![User isUserLogin])
    {
        //Open login page
        LoginViewController *loginVC = [[Global storyboard] instantiateViewControllerWithIdentifier:@"LoginViewController"];
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:loginVC animated:NO completion:nil];
    }
    
    //fuck ios7 bug
    self.tabBarController.selectedIndex = 4;
    self.tabBarController.selectedIndex = 0;
    
    //SYNC DATA
    //-->moved to AppDelegate
    
    //Check News
    //-->moved to AppDelegate
}

#pragma mark - DATA
-(void)loadData
{
    NSMutableArray *cats = [[NSMutableArray alloc] init];
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        NSString *sql = @"  SELECT c.*, CASE WHEN c3.id > 0 THEN 4 ELSE 3 END AS num_level \
                            FROM product_cat c \
                            LEFT JOIN product_cat c2 ON c2.product_cat_parent_id = c.id \
                            LEFT JOIN product_cat c3 ON c3.product_cat_parent_id = c2.id \
                            WHERE c.product_cat_parent_id = 0 \
                            GROUP BY c.id \
                            ORDER BY c.cat_code ASC";
        FMResultSet *s = [db executeQuery:sql];
        
        while ([s next])
        {
            [cats addObject:[s resultDictionary]];
        }
        
        listContent = [NSArray arrayWithArray:cats];
        
        [db close];
        
        [refreshControl endRefreshing];
    }
    else
    {
        [refreshControl endRefreshing];
    }
}

-(void)reloadData
{
    [self loadData];
    [self.tableView reloadData];
    
    NSLog(@"reload");
    
//    self.tableView.tableHeaderView = nil;
//    self.tableView.tableHeaderView = self.searchDisplayController.searchBar;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    int numSec = 0;
    
    if (tableView == self.tableView)
    {
        numSec = 1;
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        numSec = (int)[filteredListContent count];
    }
    
    return numSec;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int numRow = 0;
    
    if (tableView == self.tableView)
    {
        numRow = (int)[listContent count]/2;
        
        if ([listContent count]%2 > 0) {
            numRow += 1;
        }
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSDictionary *catDict = [filteredListContent objectAtIndex:section];
        numRow = (int)[catDict[@"prodArray"] count];
    }
    
    return numRow;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    if (tableView == self.tableView)
    {
        static NSString *CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                [subview removeFromSuperview];
            }
        }
        
        for (int i=0; i<2; i++)
        {
            int pdIndex = ((int)indexPath.row * 2) + i;
            if (pdIndex >= [listContent count]) {
                break;
            }
            
            //NSLog(@"pdINdex = %d", pdIndex);
            
            NSDictionary *catDict = (id)[listContent objectAtIndex:pdIndex];
            ProductBrandBox *box = [[[NSBundle mainBundle] loadNibNamed:@"ProductBrandBox" owner:self options:nil] objectAtIndex:0];

            if (IDIOM != IPAD)
            {
                float box_x = 8;
                if (i == 1) {
                    box_x = 8 + 150 + 4;
                }
                [box setFrame:CGRectMake(box_x, 0, 150, 150)];
                [UICommon setFakeShadow:box.button];
            }
            else if (IDIOM == IPAD)
            {
                float box_x = 20;
                if (i == 1) {
                    box_x += 360 + 4*2;
                }
                [box setFrame:CGRectMake(box_x, 0, 360, 360)];
                //[UICommon setShadow:box];
            }
            
            [box initial];
            [box setCat:catDict];
            box.button.tag = pdIndex;
            [box setAutoresizingMask:UIViewAutoresizingNone];
            [box.button addTarget:self action:@selector(boxPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.contentView addSubview:box];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSDictionary *catDict = [filteredListContent objectAtIndex:indexPath.section];
        NSDictionary *prodDict = [catDict[@"prodArray"] objectAtIndex:indexPath.row];
        
//        ProductListBox *box = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"ProductListBox"] owner:self options:nil] objectAtIndex:0];
//        [box initial];
//        [box setProduct:prodDict];
        
        float row_height = [prodDict[@"row_height"] floatValue];
        
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell_search"];
        
        UIView *frameView = (id)[cell viewWithTag:10000];
        UIImageView *imageView = (id)[cell viewWithTag:1000];
        UILabel *labelName = (id)[cell viewWithTag:2000];
        UILabel *labelDesc = (id)[cell viewWithTag:3000];
        UIImageView *imageViewArrow = (id)[cell viewWithTag:8000];
        
        [imageView setImageFromImagePathPhone:prodDict[@"product_image_sm_phone_path"] andImagePathTablet:prodDict[@"product_image_sm_tablet_path"]];
        [imageView setContentMode:UIViewContentModeScaleAspectFit];
        imageView.center = CGPointMake(imageView.center.x, row_height/2);
        
        if (![prodDict[@"product_rgb"] isEqualToString:@""] && prodDict[@"product_rgb"] != nil)
        {
            labelName.textColor = [HexColor colorWithHexString:prodDict[@"product_rgb"]];
        }
        
        labelName.text = prodDict[[@"product_name" languageField]];
        [labelName sizeToFit2];
        labelDesc.text = prodDict[[@"product_desc" languageField]];
        [labelDesc sizeToFit2];
        [labelDesc setFramePosY:labelName.posOfEndY + 0];
        [imageViewArrow setFrameCenterY:row_height/2];
        
//        [frameView setFrameHeight:row_height];
        frameView.frame = CGRectMake(frameView.frame.origin.x, frameView.frame.origin.y, frameView.frame.size.width, row_height);
        [frameView makeBorder];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.clipsToBounds = YES;
        
        if (self.listMode == productListModePickList) {
            imageViewArrow.hidden = YES;
        }
        
//        if (self.listMode == productListModePickList) {
//            UIImageView *imageViewArrow = (id)[box viewWithTag:8000];
//            imageViewArrow.hidden = YES;
//        }
        
//        [cell.contentView addSubview:box];
    }
    
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.tableView) {
        return [UICommon blankHeaderViewAtHeight:10];
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSDictionary *catDict = [filteredListContent objectAtIndex:section];
        
        ProductCatBoxHead *catHead = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"ProductCatBoxHead"] owner:self options:nil] objectAtIndex:0];
        [catHead initial];
        [catHead setCat:catDict];
        
        return catHead;
    }
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 10)];
        footerView.backgroundColor = [UIColor clearColor];
        return footerView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    float secHeight = 0;
    
    if (tableView == self.tableView)
    {
        secHeight = IS_PAD?20.0:10.0;
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView) {
        secHeight = [ProductCatBoxHead viewHeight];
    }
    
    return secHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    float secHeight = 0;
    
    if (tableView == self.tableView) {
        secHeight = 0;
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView) {
        secHeight = IS_PAD?20.0:10.0;;
    }
    
    return secHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float rowHeight = 0.0f;
    
    if (tableView == self.tableView)
    {
        rowHeight = IS_PAD?370:155;
    }
    else if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSDictionary *catDict = [filteredListContent objectAtIndex:indexPath.section];
        NSDictionary *prodDict = [catDict[@"prodArray"] objectAtIndex:indexPath.row];
        
        rowHeight = [prodDict[@"row_height"] floatValue]; //[ProductListBox viewHeight];
    }
    
    return rowHeight;
}

- (void)boxPressed:(id)sender;
{
    UIButton *btn = (UIButton *)sender;
    NSDictionary *catDict = (id)[listContent objectAtIndex:btn.tag];
    
    //Get Level
    int maxLevel = 0;
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        NSString *sql = [NSString stringWithFormat:@" SELECT c.id, CASE WHEN c3.id > 0 THEN 4 ELSE 3 END AS num_level \
        FROM product_cat c \
        LEFT JOIN product_cat c2 ON c2.product_cat_parent_id = c.id \
        LEFT JOIN product_cat c3 ON c3.product_cat_parent_id = c2.id \
        WHERE c.id =  %@", catDict[@"id"]];
        FMResultSet *s = [db executeQuery:sql];
        
        while ([s next])
        {
            int num_level = [s intForColumn:@"num_level"];
            if (num_level > maxLevel) {
                maxLevel = num_level;
            }
        }
    }
    
    if (maxLevel == 3)
    {
        ProductL2ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductL2ViewController"];
        vc.parentId = [catDict[@"id"] intValue];
        vc.title = catDict[[@"cat_name" languageField]];
        vc.listMode = self.listMode;
        vc.calculationVC = self.calculationVC;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (maxLevel == 4)
    {
        ProductLMidViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductLMidViewController"];
        vc.parent_id = [catDict[@"id"] intValue];
        vc.title = catDict[[@"cat_name" languageField]];
        vc.listMode = self.listMode;
        vc.calculationVC = self.calculationVC;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        if (self.listMode == productListModePickList)
        {
            UITableViewCell *cell = [self.searchDisplayController.searchResultsTableView cellForRowAtIndexPath:indexPath];
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            
            [self.navigationController popToViewController:self.calculationVC animated:YES];
            
            NSDictionary *catDict = [filteredListContent objectAtIndex:indexPath.section];
            NSDictionary *prodDict = [catDict[@"prodArray"] objectAtIndex:indexPath.row];
            
            CalculationViewController *calVC = self.calculationVC;
            [calVC updateProduct:[prodDict[@"id"] intValue]];
        }
        else
        {
            ProductDetailViewController *prodVC = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
            
            NSDictionary *catDict = [filteredListContent objectAtIndex:indexPath.section];
            NSDictionary *prodDict = [catDict[@"prodArray"] objectAtIndex:indexPath.row];
            prodVC.product_id = [prodDict[@"id"] intValue];
            prodVC.title = catDict[[@"cat_name" languageField]];
            
            [self.navigationController pushViewController:prodVC animated:YES];
        }
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}

#pragma mark - Option Button

- (void)optionPressed
{
    OptionViewController *optionVC = [[Global storyboardIphone] instantiateViewControllerWithIdentifier:@"OptionViewController"];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:optionVC];
    
    [self presentViewController:navVC animated:YES completion:nil];
}

#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSLog(@"search string = %@", searchString);
    
    NSString *searchText = [searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if (searchText.length >= 2) {
        [self filterContentForSearchText:searchText];
    }
    else
    {
        [filteredListContent removeAllObjects];
    }
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    //search tableview
    tableView.backgroundColor = [UIColor colorWithWhite:0.3 alpha:1.0];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    float searchTableHeaderHeight = IS_PAD?20.0:10.0;
    tableView.tableHeaderView = [UICommon blankHeaderViewAtHeight:searchTableHeaderHeight];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    
}

#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText
{
    if (filteredListContent == nil) {
        filteredListContent = [NSMutableArray array];
    }
	[filteredListContent removeAllObjects]; // First clear the filtered array.
	
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        NSString *sql = [NSString stringWithFormat:@"\
                         SELECT p.id, p.product_name_en, p.product_name_th, p.product_desc_en, p.product_desc_th, \
                                c.cat_name_en, c.cat_name_th, c.cat_desc_en, c.cat_desc_th, p.product_cat_id, \
                                p.product_image_sm_phone_path, p.product_image_sm_tablet_path, p.product_image_lg_phone_path, p.product_image_lg_tablet_path, \
                                p.product_rgb, p.product_pdf_path \
                         FROM product p \
                         JOIN product_cat c ON p.product_cat_id = c.id \
                         WHERE p.product_name_en LIKE '%%%@%%' OR p.product_name_th LIKE '%%%@%%' OR p.product_desc_en LIKE '%%%@%%' OR p.product_desc_th LIKE '%%%@%%' \
                         ORDER BY c.id ASC", searchText, searchText, searchText, searchText];
        FMResultSet *s = [db executeQuery:sql];
        
        NSDictionary *lastRowCat = nil;
        while ([s next])
        {
            NSMutableDictionary *prodDict = [[s resultDictionary] mutableCopy];
            
            // row height
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell_search"];
            UILabel *labelName = (id)[cell viewWithTag:2000];
            UILabel *labelDesc = (id)[cell viewWithTag:3000];
            UIImageView *imageView = (id)[cell viewWithTag:1000];
            labelName.text = prodDict[[@"product_name" languageField]];
            [labelName sizeToFit2];
            labelDesc.text = prodDict[[@"product_desc" languageField]];
            [labelDesc sizeToFit2];
            [labelDesc setFramePosY:labelName.posOfEndY + 0];
            
            float y_imageView = (float)imageView.posOfEndY;
            y_imageView += IS_PAD?10.0:8.0;
            float y_labelDesc = (float)labelDesc.posOfEndY + 10.0;
            
            float row_height = MAX(y_imageView, y_labelDesc);
            NSNumber *rowHeight = [NSNumber numberWithFloat:row_height];
            [prodDict setObject:rowHeight forKey:@"row_height"];
            
            
            if (![lastRowCat[@"id"] isEqualToString:[prodDict[@"product_cat_id"] stringValue]])
            {
                //start new cat
                NSMutableDictionary *currentRowCat = [NSMutableDictionary dictionary];
                currentRowCat[@"id"] = [s stringForColumn:@"product_cat_id"];
                currentRowCat[@"cat_name_th"] = [s stringForColumn:@"cat_name_th"];
                currentRowCat[@"cat_name_en"] = [s stringForColumn:@"cat_name_en"];
                currentRowCat[@"cat_desc_th"] = [s stringForColumn:@"cat_desc_th"];
                currentRowCat[@"cat_desc_en"] = [s stringForColumn:@"cat_desc_en"];
                currentRowCat[@"prodArray"] = [NSMutableArray array];
                [currentRowCat[@"prodArray"] addObject:prodDict];
                
                [filteredListContent addObject:currentRowCat];
                
                lastRowCat = currentRowCat;
            }
            else
            {
                [lastRowCat[@"prodArray"] addObject:prodDict];
            }
            
            NSLog(@"search found = %@", prodDict[@"product_name_en"]);
        }
        
        [db close];
    }
}

@end
