//
//  ProductL2ViewController.h
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/11/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Global.h"
#import "ProductCat.h"
#import "Product.h"
#import "ManageData.h"
#import "ProductL1ViewController.h"
#import "LanguageUITableViewController.h"

@interface ProductL2ViewController : LanguageUITableViewController
{
    NSMutableArray *catArray;
    
    NSMutableArray *allProductArray; //hold data of all products in this lv1 catalog, for send list to product detail page
}

@property int parentId;

@property ProductListMode listMode;
@property (nonatomic, retain) id calculationVC;

@end
