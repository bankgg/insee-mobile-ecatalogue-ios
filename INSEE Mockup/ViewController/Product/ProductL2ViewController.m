//
//  ProductL2ViewController.m
//  INSEE Mockup
//
//  Created by Tanut Chantarajiraporn on 10/11/56 BE.
//  Copyright (c) 2556 iAppGarage. All rights reserved.
//

#import "ProductL2ViewController.h"

#import "UICommon.h"
#import "FMDatabase.h"
#import "Stat.h"

#import "ProductCatBox.h"
#import "ProductCatBoxHead.h"
#import "ProductListBox.h"
#import "NSString+Language.h"

#import "CalculationViewController.h"
#import "ProductDetailViewController.h"
#import "ProductDetail2ViewController.h"

#import "HexColor.h"
#import "UIView+Extension.h"
#import "UIImageView+Ext.h"

@interface ProductL2ViewController ()

@end

@implementation ProductL2ViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.title = @"อินทรี MORTAR MAX";
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    //[self setData];
    [self loadData];
    
    float tableHeaderHeight = 10;
    if (IDIOM == IPAD) {
        tableHeaderHeight = 20;
    }
    [self.tableView setTableHeaderView:[UICommon blankHeaderViewAtHeight:tableHeaderHeight]];
    [self.tableView setTableFooterView:[UICommon blankHeaderViewAtHeight:tableHeaderHeight]];
    
    [self.tableView reloadData];
}

- (void)languageLoad
{
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadData
{
    catArray = [[NSMutableArray alloc] init];
    allProductArray = [[NSMutableArray alloc] init];
    
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *sCat = [db executeQuery:[NSString stringWithFormat:@"\
                                              SELECT * FROM product_cat \
                                              WHERE product_cat_parent_id = %d \
                                              ORDER BY cat_code ASC", self.parentId]];
        while ([sCat next])
        {
            NSMutableDictionary *catDict = [[sCat resultDictionary] mutableCopy];
            
            NSMutableArray *prodArray = [[NSMutableArray alloc] init];
            
            NSString *queryString = [NSString stringWithFormat:@"SELECT p.* FROM product p \
                                    WHERE p.product_cat_id = %@ \
                                    ORDER BY p.product_code ASC", catDict[@"id"]];
            
            if (self.listMode == productListModePickList)
            {
                queryString = [NSString stringWithFormat:@"SELECT p.* FROM product p \
                               JOIN formula f ON f.product_id = p.id \
                               WHERE p.product_cat_id = %@ \
                               GROUP BY p.id \
                               ORDER BY p.product_code ASC", catDict[@"id"]];
            }
            
            FMResultSet *sProd = [db executeQuery:queryString];
            while ([sProd next])
            {
                NSMutableDictionary *prodDict = [[sProd resultDictionary] mutableCopy];
                
                // row height
                UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
                UILabel *labelName = (id)[cell viewWithTag:2000];
                UILabel *labelDesc = (id)[cell viewWithTag:3000];
                UIImageView *imageView = (id)[cell viewWithTag:1000];
                labelName.text = prodDict[[@"product_name" languageField]];
                [labelName sizeToFit2];
                labelDesc.text = prodDict[[@"product_desc" languageField]];
                [labelDesc sizeToFit2];
                [labelDesc setFramePosY:labelName.posOfEndY + 0];
                
                float y_imageView = (float)imageView.posOfEndY;
                y_imageView += IS_PAD?10.0:8.0;
                float y_labelDesc = (float)labelDesc.posOfEndY + 10.0;
                
                float row_height = MAX(y_imageView, y_labelDesc);
                NSNumber *rowHeight = [NSNumber numberWithFloat:row_height];
                [prodDict setObject:rowHeight forKey:@"row_height"];
                
                [prodArray addObject:prodDict];
                [allProductArray addObject:prodDict];
            }
            catDict[@"prodArray"] = prodArray;
            
            if (prodArray.count > 0)
            {
                [catArray addObject:catDict];
            }
        }
        [db close];
    }
    else
    {
        NSLog(@"cannot open database");
    }
}

-(void)reloadData
{
    [self loadData];
    [self.tableView reloadData];
    
    NSLog(@"reload");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [catArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *cat = [catArray objectAtIndex:section];
    NSArray *prodArray = (id)cat[@"prodArray"];
    
    return [prodArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [ProductCatBoxHead viewHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return IS_PAD?20.0:10.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *catDict = [catArray objectAtIndex:indexPath.section];
    NSDictionary *prodDict = [catDict[@"prodArray"] objectAtIndex:indexPath.row];
    
    return (CGFloat)[prodDict[@"row_height"] floatValue];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *catDict = [catArray objectAtIndex:section];
    
    ProductCatBoxHead *catHead = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"ProductCatBoxHead"] owner:self options:nil] objectAtIndex:0];
    [catHead initial];
    [catHead setCat:catDict];
    
    return catHead;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 10)];
    footerView.backgroundColor = [UIColor clearColor];
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *catDict = [catArray objectAtIndex:indexPath.section];
    NSDictionary *prodDict = [catDict[@"prodArray"] objectAtIndex:indexPath.row];
    float row_height = [prodDict[@"row_height"] floatValue];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UIView *frameView = (id)[cell viewWithTag:10000];
    UIImageView *imageView = (id)[cell viewWithTag:1000];
    UILabel *labelName = (id)[cell viewWithTag:2000];
    UILabel *labelDesc = (id)[cell viewWithTag:3000];
    UIImageView *imageViewArrow = (id)[cell viewWithTag:8000];
    
    [imageView setImageFromImagePathPhone:prodDict[@"product_image_sm_phone_path"] andImagePathTablet:prodDict[@"product_image_sm_tablet_path"]];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    imageView.center = CGPointMake(imageView.center.x, row_height/2);
    
    if (![prodDict[@"product_rgb"] isEqualToString:@""] && prodDict[@"product_rgb"] != nil)
    {
        labelName.textColor = [HexColor colorWithHexString:prodDict[@"product_rgb"]];
    }
    
    labelName.text = prodDict[[@"product_name" languageField]];
    [labelName sizeToFit2];
    labelDesc.text = prodDict[[@"product_desc" languageField]];
    [labelDesc sizeToFit2];
    [labelDesc setFramePosY:labelName.posOfEndY + 0];
    [UIView makeView:labelName andView:labelDesc toVerticalCenterOfHeight:row_height];
    
    [imageViewArrow setFrameCenterY:row_height/2];
    
//    [frameView setFrameHeight:row_height];
    frameView.frame = CGRectMake(frameView.frame.origin.x, frameView.frame.origin.y, frameView.frame.size.width, row_height);
    [frameView makeBorder];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.clipsToBounds = YES;
    
    if (self.listMode == productListModePickList) {
        imageViewArrow.hidden = YES;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.listMode == productListModePickList)
    {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        [self.navigationController popToViewController:self.calculationVC animated:YES];
        
        NSDictionary *catDict = [catArray objectAtIndex:indexPath.section];
        NSDictionary *prodDict = [catDict[@"prodArray"] objectAtIndex:indexPath.row];
        
        CalculationViewController *calVC = self.calculationVC;
        [calVC updateProduct:[prodDict[@"id"] intValue]];
    }
    else
    {
        NSDictionary *catDict = [catArray objectAtIndex:indexPath.section];
        NSDictionary *prodDict = [catDict[@"prodArray"] objectAtIndex:indexPath.row];
        
//        ProductDetailViewController *prodVC = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
//        prodVC.product_id = [prodDict[@"id"] intValue];
//        prodVC.title = catDict[[@"cat_name" languageField]];
        
        ProductDetail2ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductDetail2ViewController"];
        vc.product_id = [prodDict[@"id"] intValue];
        vc.title = catDict[[@"cat_name" languageField]];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
