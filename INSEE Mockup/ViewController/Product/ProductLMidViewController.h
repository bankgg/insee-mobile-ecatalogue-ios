//
//  ProductLMidViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/6/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductL1ViewController.h"
#import "LanguageUITableViewController.h"

@interface ProductLMidViewController : LanguageUITableViewController

@property int parent_id;

@property ProductListMode listMode;
@property (nonatomic, retain) id calculationVC;

@end
