//
//  ProductLMidViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/6/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "ProductLMidViewController.h"

#import "Global.h"
#import "UICommon.h"
#import "FMDatabase.h"
#import "ManageData.h"
#import "Stat.h"

#import "UIImageView+Ext.h"
#import "UIView+Extension.h"
#import "NSString+Language.h"

#import "ProductL2ViewController.h"

@interface ProductLMidViewController ()
{
    NSMutableArray *listContent;
}
@end

@implementation ProductLMidViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)languageLoad
{
    [self.tableView reloadData];
}

- (void)loadData
{
    listContent = [NSMutableArray array];
    
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *s = [db executeQuery:[NSString stringWithFormat:@" \
                                           SELECT * \
                                           FROM product_cat \
                                           WHERE product_cat_parent_id = %d", self.parent_id]];
        
        while ([s next])
        {
            NSMutableDictionary *dict = [[s resultDictionary] mutableCopy];
            
            //row_height
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
            
            UIView *frameView = (id)[cell viewWithTag:10000];
            UIImageView *imageView = (id)[cell viewWithTag:1000];
            UILabel *labelTitle = (id)[cell viewWithTag:2000];
            UILabel *labelDesc = (id)[cell viewWithTag:3000];
            
            labelTitle.text = dict[[@"cat_name" languageField]];
            labelDesc.text = dict[[@"cat_desc" languageField]];
            
            [labelTitle sizeToFit2];
            [labelDesc sizeToFit2];
            [labelDesc setFramePosY:labelTitle.posOfEndY + 3];
//            [frameView setFrameHeight:MAX(labelDesc.posOfEndY + 15, imageView.posOfEndY + 5)];
            frameView.frame = CGRectMake(frameView.frame.origin.x, frameView.frame.origin.y, frameView.frame.size.width, MAX(labelDesc.posOfEndY + 15, imageView.posOfEndY + 5));
            
            float row_height = frameView.posOfEndY + 5;
            [dict setObject:[NSNumber numberWithFloat:row_height] forKeyedSubscript:@"row_height"];
            
            [listContent addObject:dict];
            
//            [listContent addObject:[s resultDictionary]];
        }
    }
    [db close];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listContent.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (IDIOM == IPAD) {
        return tableView.rowHeight;
    }
    else {
        NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
        return [dict[@"row_height"] floatValue];
    }
    
    return 90;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UIView *frameView = (id)[cell viewWithTag:10000];
    UIImageView *imageView = (id)[cell viewWithTag:1000];
    UILabel *labelTitle = (id)[cell viewWithTag:2000];
    UILabel *labelDesc = (id)[cell viewWithTag:3000];
    
    [frameView makeBorder];
    [imageView setImageFromImagePathPhone:dict[@"product_cat_image_phone_path"] andImagePathTablet:dict[@"product_cat_image_tablet_path"]];
    
    labelTitle.text = dict[[@"cat_name" languageField]];
    labelDesc.text = dict[[@"cat_desc" languageField]];
    
    [labelTitle sizeToFit2];
    [labelDesc sizeToFit2];
    [labelDesc setFramePosY:labelTitle.posOfEndY + 0];
//    [frameView setFrameHeight:MAX(labelDesc.posOfEndY + 15, imageView.posOfEndY + 5)];
    frameView.frame = CGRectMake(frameView.frame.origin.x, frameView.frame.origin.y, frameView.frame.size.width, MAX(labelDesc.posOfEndY + 15, imageView.posOfEndY + 5));
    
    [imageView setFrameCenterY:frameView.frame.size.height / 2];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *catDict = [listContent objectAtIndex:indexPath.row];
    
    ProductL2ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductL2ViewController"];
    vc.parentId = [catDict[@"id"] intValue];
    vc.title = catDict[[@"cat_name" languageField]];
    vc.listMode = self.listMode;
    vc.calculationVC = self.calculationVC;
    [self.navigationController pushViewController:vc animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
