//
//  SimulatorPortraitViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/22/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ACT_TYPE_INFO           1000
#define ACT_TYPE_PRODUCT        2000
#define ACT_TYPE_VIDEO          3000
#define ACT_TYPE_MULTI_PRODUCT  4000

@interface SimulatorPortraitViewController : UIViewController

@property int sim_id;

@end
