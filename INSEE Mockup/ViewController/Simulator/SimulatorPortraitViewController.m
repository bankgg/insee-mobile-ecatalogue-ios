//
//  SimulatorPortraitViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/22/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "SimulatorPortraitViewController.h"
#import "UIScrollView+ZoomToPoint.h"
#import "SimulatorPortraitMarkerButton.h"
#import "ProductDetail2ViewController.h"
#import "SimulatorProductListViewController.h"

#import "UICommon.h"
#import "UIView+Extension.h"
#import "LocalizeHelper.h"
#import "NSString+Language.h"
#import "UIImageExtension.h"
#import "UIImageView+Ext.h"
#import "YTPlayerView.h"

#import "Global.h"
#import "FMDatabase.h"
#import "ManageData.h"
#import "Stat.h"

@interface SimulatorPortraitViewController () <UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSDictionary *theBox;
    NSMutableArray *boxArray;
    NSMutableArray *zoomViewArray;
    
    BOOL isSetUI;
    
    UIView *currentInfoBoxTypeView;
    
    NSMutableArray *multiProductArray;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewZoom;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIView *viewInfo;
@property (weak, nonatomic) IBOutlet UIView *viewInfoBox;
@property (weak, nonatomic) IBOutlet UILabel *infoBoxTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *infoBoxTopButton;
@property (weak, nonatomic) IBOutlet UIButton *infoBoxMoreButton;

//Type Info : 1000
@property (weak, nonatomic) IBOutlet UIScrollView *infoBoxTypeInfoScrollView;
@property (weak, nonatomic) IBOutlet UILabel *infoBoxTypeInfoLabelContent;

//Type Product : 2000
@property (weak, nonatomic) IBOutlet UIScrollView *infoBoxTypeProductScrollView;
@property (weak, nonatomic) IBOutlet UILabel *infoBoxTypeProductLabelContent;
@property (weak, nonatomic) IBOutlet UIImageView *infoBoxTypeProductImageView;

//Type Video : 3000
@property (weak, nonatomic) IBOutlet UIScrollView *infoBoxTypeVideoScrollView;
@property (weak, nonatomic) IBOutlet YTPlayerView *infoBoxTypeVideoYoutubePlayerView;
@property (weak, nonatomic) IBOutlet UILabel *infoBoxTypeVideoLabelContent;

//Type Multiple Products : 4000
@property (weak, nonatomic) IBOutlet UITableView *infoBoxTypeMultiProductsTableView;


- (IBAction)infoBoxTopButtonPressed:(id)sender;
- (IBAction)infoBoxMoreButtonPressed:(UIButton *)button;
- (IBAction)dimBackgroundButtonPressed:(id)sender;

@end

@implementation SimulatorPortraitViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewInfo.alpha = 0;
    self.viewInfo.userInteractionEnabled = NO;
    [self.viewInfoBox makeRoundCorner:15];
    
    [self.imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"sim_%d.jpg", self.sim_id]]];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapRecognized:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.scrollView addGestureRecognizer:doubleTap];
    
    //Hide all types
    self.infoBoxTypeInfoScrollView.hidden = YES;
    self.infoBoxTypeProductScrollView.hidden = YES;
    self.infoBoxTypeVideoScrollView.hidden = YES;
    self.infoBoxTypeMultiProductsTableView.hidden = YES;
    
    
    [self loadData];
    
    [self.infoBoxTypeInfoLabelContent sizeToFit2];
    self.infoBoxTypeInfoScrollView.contentSize = self.infoBoxTypeInfoLabelContent.frame.size;
    
    //stat
    [Stat saveStatOfPageType:pageTypeSimulationPortrait pageName:self.title withAction:@"view"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLayoutSubviews
{
    NSLog(@"-SIM --viewDidLayoutSubviews self.viewZoom.bounds.size.height = %f", self.scrollView.frame.size.height);
    
    if (!isSetUI)
    {
        [self setUI];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    /*
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionReveal;
    transition.subtype = kCATransitionFromBottom;
    transition.delegate = self;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
     */
}

- (void)loadData
{
    boxArray = [NSMutableArray array];
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *s = [db executeQueryWithFormat:@"SELECT * FROM sim_act WHERE sim_id = %d", self.sim_id];
        while ([s next])
        {
            [boxArray addObject:[[s resultDictionary] mutableCopy]];
        }
    }
    
    zoomViewArray = [NSMutableArray array];
    
    for (NSMutableDictionary *box in boxArray)
    {
        SimulatorPortraitMarkerButton *button = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"SimulatorPortraitMarkerButton"] owner:self options:nil] objectAtIndex:0];
        button.alpha = 0;
        button.stringTag = [box[@"id"] stringValue];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImage *imageButton;
        switch ([box[@"sim_act_type_id"] intValue]) {
            case ACT_TYPE_INFO:
                imageButton = [UIImage imageNamed:@"sim_marker_info"];
                break;
            case ACT_TYPE_PRODUCT:
                imageButton = [UIImage imageNamed:@"sim_marker_prod"];
                break;
            case ACT_TYPE_VIDEO:
                imageButton = [UIImage imageNamed:@"sim_marker_video"];
                break;
            case ACT_TYPE_MULTI_PRODUCT:
                imageButton = [UIImage imageNamed:@"sim_marker_prod"];
                break;
                
            default:
                break;
        }
        if (!imageButton) {
            imageButton = [UIImage imageNamed:@"sim_marker_info"];
        }
        
//        [button setImage:imageButton forState:UIControlStateNormal];
        [button setBackgroundImage:imageButton forState:UIControlStateNormal];
        [button.imageView setContentMode:UIViewContentModeScaleAspectFit];
        
        [self.viewZoom addSubview:button];
        [box setObject:button forKey:@"button"];
        
        [zoomViewArray addObject:button];
    }
}

- (void)setUI
{
    [self.scrollView setContentSize:self.scrollView.frame.size];
    
    self.infoBoxMoreButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.infoBoxMoreButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    
    for (NSMutableDictionary *box in boxArray)
    {
        UIButton *button = box[@"button"];
        [self setCenterPosX:[box[@"pos_x"] floatValue] andPosY:[box[@"pos_y"] floatValue] ofView:button];
        
        float moveY = 5.0;
        button.center = CGPointMake(button.center.x, button.center.y + moveY);
        
        int delayInt = arc4random_uniform(100);
        float delayFloat = (float)delayInt/100.0;
        
        [UIView animateWithDuration:0.5 delay:delayFloat options:UIViewAnimationOptionCurveEaseInOut animations:^{
            button.alpha = 0.6;
            button.center = CGPointMake(button.center.x, button.center.y - moveY);
        } completion:^(BOOL finished) {
            
        }];
    }
    
    isSetUI = YES;
}

//posX and posY is from 0 to 1
- (void)setCenterPosX:(float)posX andPosY:(float)posY ofView:(UIView *)view
{
    float W = self.viewZoom.frame.size.width;
    float H = self.viewZoom.frame.size.height;
    
    CGSize imgSize = [UIImageExtension sizeOfImage:self.imageView.image afterScaledFitToFrameSize:self.viewZoom.frame.size];
    float w = imgSize.width;
    float h = imgSize.height;
    
    float x = posX * w;
    float y = posY * h;
    
    float x_ = (W-w)/2 + x;
    float y_ = (H-h)/2 + y;
    
    view.center = CGPointMake(x_, y_);
}

#pragma mark - Buttons

- (void)buttonPressed:(UIButton *)button
{
    for (NSDictionary *box in boxArray)
    {
        UIButton *v_button = box[@"button"];
        
        if ([[box[@"id"] stringValue] isEqualToString:button.stringTag])
        {
            theBox = box;
            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                v_button.alpha = 1.0;
            } completion:nil];
        }
        else
        {
            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                v_button.alpha = 0.6;
            } completion:nil];
        }
    }
    
    [self.scrollView zoomToPoint:button.center withScale:self.scrollView.zoomScale animated:YES];
    
    
    self.infoBoxTitleLabel.text = theBox[[@"title" languageField]];
    currentInfoBoxTypeView.hidden = YES;
    switch ([theBox[@"sim_act_type_id"] intValue])
    {
        case ACT_TYPE_INFO: //INFO
        {
            currentInfoBoxTypeView = self.infoBoxTypeInfoScrollView;
            
            self.infoBoxTypeInfoLabelContent.text = theBox[[@"content" languageField]];
            
            [self.infoBoxTypeInfoLabelContent sizeToFit2];
            self.infoBoxTypeInfoScrollView.contentSize = self.infoBoxTypeInfoLabelContent.frame.size;
            
            [self.infoBoxTopButton setImage:[UIImage imageNamed:@"sim_marker_close"] forState:UIControlStateNormal];
            
            self.infoBoxMoreButton.tag = [theBox[@"id"] intValue];
            self.infoBoxMoreButton.hidden = NO;
            [self.infoBoxMoreButton setTitle:LocalizedString(@"sim_t_info_button") forState:UIControlStateNormal];
        }
            break;
            
        case ACT_TYPE_PRODUCT: //PRODUCT
        {
            currentInfoBoxTypeView = self.infoBoxTypeProductScrollView;
            
            FMDatabase *db = [ManageData database];
            if ([db open])
            {
                FMResultSet *s = [db executeQueryWithFormat:@"SELECT p.* \
                                  FROM sim_act_prod sap \
                                  JOIN product p ON sap.product_id = p.id \
                                  WHERE sap.sim_act_id = %d", [theBox[@"id"] intValue]];
                [s next];
                NSDictionary *dict = s.resultDictionary;
                
                [self.infoBoxTypeProductImageView setImageFromImagePathPhone:dict[@"product_image_lg_phone_path"] andImagePathTablet:dict[@"product_image_lg_tablet_path"]];
                
                self.infoBoxTypeProductLabelContent.text = dict[[@"product_desc" languageField]];
                [self.infoBoxTypeProductLabelContent sizeToFit2];
                
                self.infoBoxTypeProductScrollView.contentSize = CGSizeMake(self.infoBoxTypeProductScrollView.frame.size.width, self.infoBoxTypeProductLabelContent.posOfEndY + 10);
                
                self.infoBoxMoreButton.tag = [dict[@"id"] intValue];
            }
            
            [self.infoBoxTopButton setImage:[UIImage imageNamed:@"sim_marker_close"] forState:UIControlStateNormal];
            
            self.infoBoxMoreButton.hidden = NO;
            [self.infoBoxMoreButton setTitle:LocalizedString(@"sim_t_product_button") forState:UIControlStateNormal];
        }
            break;
            
        case ACT_TYPE_VIDEO: //VIDEO
        {
            currentInfoBoxTypeView = self.infoBoxTypeVideoScrollView;
            
            self.infoBoxTypeVideoLabelContent.text = theBox[[@"content" languageField]];
            
            [self.infoBoxTypeVideoLabelContent sizeToFit2];
            [self.infoBoxTypeVideoYoutubePlayerView setFramePosY:self.infoBoxTypeVideoLabelContent.posOfEndY + 5];
            
            [self.infoBoxTypeVideoYoutubePlayerView loadWithVideoId:theBox[@"video_url"]];
            
            self.infoBoxTypeVideoScrollView.contentSize = CGSizeMake(self.infoBoxTypeVideoScrollView.frame.size.width, self.infoBoxTypeVideoYoutubePlayerView.posOfEndY + 5);
            
            [self.infoBoxTopButton setImage:[UIImage imageNamed:@"sim_marker_close"] forState:UIControlStateNormal];
            
            self.infoBoxMoreButton.hidden = YES;
        }
            break;
            
        case ACT_TYPE_MULTI_PRODUCT: //MULTIPLE PRODUCTS
        {
            currentInfoBoxTypeView = self.infoBoxTypeMultiProductsTableView;
            
            [self multiProductLoadData:[theBox[@"id"] intValue]];
            
            [self.infoBoxTopButton setImage:[UIImage imageNamed:@"sim_marker_close"] forState:UIControlStateNormal];
            
            self.infoBoxMoreButton.hidden = YES;
        }
            break;
            
        default:
            break;
    }
    currentInfoBoxTypeView.hidden = NO;
    
    //Show info view
    self.viewInfoBox.center = CGPointMake(self.viewInfoBox.center.x, self.viewInfoBox.center.y + 5);
    self.viewInfoBox.alpha = 0;
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.viewInfo.alpha = 1.0;
        self.viewInfo.userInteractionEnabled = YES;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.viewInfoBox.center = CGPointMake(self.viewInfoBox.center.x, self.viewInfoBox.center.y - 5);
            self.viewInfoBox.alpha = 1.0;
        } completion:nil];
    }];
}

- (IBAction)infoBoxTopButtonPressed:(id)sender
{
    [self dismissInfoBox];
}

- (IBAction)infoBoxMoreButtonPressed:(UIButton *)button
{
    switch ([theBox[@"sim_act_type_id"] intValue])
    {
        case ACT_TYPE_INFO: //INFO
        {
            SimulatorProductListViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"SimulatorProductListViewController"];
            vc.parent_id = (int)button.tag;
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
            
        case ACT_TYPE_PRODUCT: //PRODUCT
        {
            ProductDetail2ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductDetail2ViewController"];
            vc.product_id = (int)button.tag;
            vc.title = LocalizedString(@"product_default_title");
            [self.navigationController pushViewController:vc animated:YES];
            
        }
            break;
            
        case ACT_TYPE_VIDEO: //VIDEO
        {
            
        }
            break;
            
        case ACT_TYPE_MULTI_PRODUCT: //MULTIPLE PRODUCTS
        {
            
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)dimBackgroundButtonPressed:(id)sender
{
    [self dismissInfoBox];
}

- (void)dismissInfoBox
{
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.viewInfo.alpha = 0;
    } completion:^(BOOL finished) {
        self.viewInfo.userInteractionEnabled = NO;
    }];
}

#pragma mark - ZOOM

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.viewZoom;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    NSLog(@"scale = %f", scale);
    self.viewZoom.contentScaleFactor = scale;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    for (UIView *v in zoomViewArray)
    {
        CGPoint center = v.center;
        v.transform = CGAffineTransformMakeScale(1.0/scrollView.zoomScale, 1.0/scrollView.zoomScale);
        v.center = center;
    }
}

- (void)doubleTapRecognized:(UITapGestureRecognizer*)recognizer
{
    if (self.scrollView.zoomScale == 2.0)
    {
        [self.scrollView zoomToPoint:CGPointZero withScale:1.0 animated:YES];
    }
    else
    {
        CGPoint point = [recognizer locationInView:self.viewZoom];
        [self.scrollView zoomToPoint:point withScale:2.0 animated:YES];
    }
}

#pragma mark - Multiple Product TableView

- (void)multiProductLoadData:(int)sim_act_id
{
    multiProductArray = [NSMutableArray array];
    
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *s = [db executeQueryWithFormat:@"SELECT p.id, \
                            p.product_name_th, p.product_name_en, \
                            p.product_desc_th, p.product_desc_en, \
                            p.product_image_sm_phone_path, p.product_image_sm_tablet_path\
                          FROM sim_act_prod sap \
                          JOIN product p ON sap.product_id = p.id \
                          WHERE sap.sim_act_id = %d", sim_act_id];
        
        while ([s next])
        {
            [multiProductArray addObject:[s resultDictionary]];
        }
        
        [db close];
        [self.infoBoxTypeMultiProductsTableView reloadData];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return multiProductArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return IS_PAD?100:70;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *v = [[UIView alloc] init];
    v.backgroundColor = [UIColor clearColor];
    
    return v;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [multiProductArray objectAtIndex:indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    UIView *frameView = (id)[cell viewWithTag:10000];
    [frameView makeBorder];
    
    UIImageView *imageView = (id)[cell viewWithTag:1000];
    [imageView setImageFromImagePathPhone:dict[@"product_image_sm_phone_path"] andImagePathTablet:dict[@"product_image_sm_tablet_path"]];
    
    UILabel *labelTitle = (id)[cell viewWithTag:2000];
    labelTitle.text = dict[[@"product_name" languageField]];
    
    UILabel *labelDesc = (id)[cell viewWithTag:3000];
    labelDesc.text = dict[[@"product_desc" languageField]];
    
    if ([labelDesc.text isEqualToString:@""])
    {
        labelTitle.numberOfLines = 2;
        [labelTitle sizeToFit];
        
        labelTitle.center = CGPointMake(labelTitle.center.x, imageView.center.y);
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = [multiProductArray objectAtIndex:indexPath.row];
    
    ProductDetail2ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductDetail2ViewController"];
    vc.product_id = [dict[@"id"] intValue];
    vc.title = dict[[@"product_name" languageField]];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
