//
//  SimulatorProductListViewController.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 6/2/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimulatorProductListViewController : UITableViewController

@property int parent_id; //sim_act_id

@end
