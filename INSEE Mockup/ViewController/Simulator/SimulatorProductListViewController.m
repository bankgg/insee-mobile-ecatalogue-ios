//
//  SimulatorProductListViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 6/2/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "SimulatorProductListViewController.h"
#import "ProductDetail2ViewController.h"

#import "Global.h"
#import "UICommon.h"
#import "FMDatabase.h"
#import "ManageData.h"

#import "UIImageView+Ext.h"
#import "UIView+Extension.h"
#import "NSString+Language.h"
#import "LocalizeHelper.h"
#import "HexColor.h"

@interface SimulatorProductListViewController ()
{
    NSMutableArray *listContent;
}
@end

@implementation SimulatorProductListViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = LocalizedString(@"sim_product_list_title");
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData
{
    listContent = [NSMutableArray array];
    
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *s = [db executeQuery:[NSString stringWithFormat:@" \
                                           SELECT sap.id AS sap_id, p.* \
                                           FROM sim_act_prod sap \
                                           JOIN product p ON sap.product_id = p.id \
                                           WHERE sap.sim_act_id = %d", self.parent_id]];
        
        while ([s next])
        {
            NSMutableDictionary *dict = [[s resultDictionary] mutableCopy];
            

            //Row Height
            
            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cell"];
            
            UIImageView *imageView = (id)[cell viewWithTag:1000];
            
            UILabel *labelTitle = (id)[cell viewWithTag:2000];
            labelTitle.text = dict[[@"product_name" languageField]];
            [labelTitle sizeToFit2];
            
            UILabel *labelDesc = (id)[cell viewWithTag:3000];
            labelDesc.text = dict[[@"product_desc" languageField]];
            [labelDesc sizeToFit2];
            [labelDesc setFramePosY:labelTitle.posOfEndY + 0];
            
            float row_height = MAX(imageView.posOfEndY + 15, labelDesc.posOfEndY + 10);
            [dict setObject:[NSNumber numberWithFloat:row_height] forKey:@"row_height"];
            
            
            
            [listContent addObject:dict];
        }
    }
    [db close];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listContent.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (IS_PAD)
    {
        return 100;
    }
    else
    {
        NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
        return [dict[@"row_height"] floatValue];
    }
    
//    return IS_PAD?100:70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    UIView *frameView = (id)[cell viewWithTag:10000];
    [frameView makeBorder];
    
    UIImageView *imageView = (id)[cell viewWithTag:1000];
    [imageView setImageFromImagePathPhone:dict[@"product_image_sm_phone_path"] andImagePathTablet:dict[@"product_image_sm_tablet_path"]];
    
    UILabel *labelTitle = (id)[cell viewWithTag:2000];
    labelTitle.text = dict[[@"product_name" languageField]];
    [labelTitle sizeToFit2];
    
    if (![dict[@"product_rgb"] isEqualToString:@""] && dict[@"product_rgb"] != nil)
    {
        labelTitle.textColor = [HexColor colorWithHexString:dict[@"product_rgb"]];
    }
    
    UILabel *labelDesc = (id)[cell viewWithTag:3000];
    labelDesc.text = dict[[@"product_desc" languageField]];
    [labelDesc sizeToFit2];
    [labelDesc setFramePosY:labelTitle.posOfEndY + 0];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dict = [listContent objectAtIndex:indexPath.row];
    
    ProductDetail2ViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"ProductDetail2ViewController"];
    vc.product_id = [dict[@"id"] intValue];
    vc.title = dict[[@"product_name" languageField]];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
