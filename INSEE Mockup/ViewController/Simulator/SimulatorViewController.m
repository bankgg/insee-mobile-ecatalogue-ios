//
//  SimulatorViewController.m
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/22/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import "SimulatorViewController.h"
#import "SimulatorPortraitViewController.h"

#import "UIScrollView+ZoomToPoint.h"
#import "SimulatorMarkerBox.h"
#import "UICommon.h"
#import "UIView+Extension.h"
#import "UIImageExtension.h"

#import "LocalizeHelper.h"
#import "NSString+Language.h"

#import "Global.h"
#import "FMDatabase.h"
#import "ManageData.h"
#import "Stat.h"

// Transition
#import "RZTransitionsManager.h"
#import "RZZoomPushAnimationController.h"
#import "RZCirclePushAnimationController.h"
#import "RZRectZoomAnimationController.h"
#import "RZShrinkZoomAnimationController.h"

@interface SimulatorViewController () <UIScrollViewDelegate>
{
    NSMutableArray *boxArray;
    NSMutableArray *zoomViewArray;
    
    BOOL isSetUI;
    
    NSMutableDictionary *boxSelected;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewZoom;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *viewBox;
@property (weak, nonatomic) IBOutlet UIButton *buttonInfo;
@property (weak, nonatomic) IBOutlet UIButton *buttonInfo2;

- (IBAction)buttonInfoPressed:(UIButton *)button;

@end


@implementation SimulatorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = LocalizedString(@"sim_title");
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTapRecognized:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.scrollView addGestureRecognizer:doubleTap];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(singleTapRecognized:)];
    singleTap.numberOfTapsRequired = 1;
    [self.scrollView addGestureRecognizer:singleTap];
    
    
    [self.buttonInfo2 makeItShadowRect];
    self.buttonInfo2.alpha = 0;
    self.buttonInfo2.userInteractionEnabled = NO;
    self.viewBox.alpha = 0;
    
    [self loadData];
    
    //stat
    [Stat saveStatOfPageType:pageTypeSimulationMain pageName:@"" withAction:@"view"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSLog(@"-SIM self.viewZoom.bounds.size.height = %f", self.scrollView.frame.size.height);
    
    if (!isSetUI)
    {
        [self setUI];
    }
}

- (void)loadData
{
    boxArray = [NSMutableArray array];
    FMDatabase *db = [ManageData database];
    if ([db open])
    {
        FMResultSet *s = [db executeQueryWithFormat:@"SELECT * FROM sim"];
        while ([s next])
        {
            [boxArray addObject:[[s resultDictionary] mutableCopy]];
        }
    }
    
    zoomViewArray = [NSMutableArray array];
    [zoomViewArray addObject:self.viewBox];
    [zoomViewArray addObject:self.buttonInfo2];
    
    for (NSMutableDictionary *box in boxArray)
    {
        SimulatorMarkerBox *boxView = [[[NSBundle mainBundle] loadNibNamed:[UICommon nibFileDevice:@"SimulatorMarkerBox"] owner:self options:nil] objectAtIndex:0];
        boxView.alpha = 0;
        
        UIButton *buttonNum = (id)[boxView viewWithTag:1000];
        [buttonNum setTitle:box[@"number"] forState:UIControlStateNormal];
        buttonNum.stringTag = [box[@"id"] stringValue];
        [buttonNum addTarget:self action:@selector(buttonNumPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.viewZoom addSubview:boxView];
        [box setObject:boxView forKey:@"boxView"];
        
        [zoomViewArray addObject:boxView];
    }
}

- (void)setUI
{
    [self.scrollView setContentSize:self.scrollView.frame.size];
    
    for (NSMutableDictionary *box in boxArray)
    {
        UIView *boxView = box[@"boxView"];
        [self setCenterPosX:[box[@"pos_x"] floatValue] andPosY:[box[@"pos_y"] floatValue] ofView:boxView];
        
        float moveY = 5.0;
        boxView.center = CGPointMake(boxView.center.x, boxView.center.y + moveY);
        
        int delayInt = arc4random_uniform(100);
        float delayFloat = (float)delayInt/100.0;
        
        [UIView animateWithDuration:0.5 delay:delayFloat options:UIViewAnimationOptionCurveEaseInOut animations:^{
            boxView.alpha = 0.6;
            boxView.center = CGPointMake(boxView.center.x, boxView.center.y - moveY);
        } completion:^(BOOL finished) {
            
        }];
    }
    
    isSetUI = YES;
}

//posX and posY is from 0 to 1
- (void)setCenterPosX:(float)posX andPosY:(float)posY ofView:(UIView *)view
{
    float W = self.viewZoom.frame.size.width;
    float H = self.viewZoom.frame.size.height;
    
    CGSize imgSize = [UIImageExtension sizeOfImage:self.imageView.image afterScaledFitToFrameSize:self.viewZoom.frame.size];
    float w = imgSize.width;
    float h = imgSize.height;
    
    float x = posX * w;
    float y = posY * h;
    
    float x_ = (W-w)/2 + x;
    float y_ = (H-h)/2 + y;
    
    view.center = CGPointMake(x_, y_);
}

#pragma mark - Buttons

- (void)buttonNumPressed:(UIButton *)button
{
    for (NSMutableDictionary *box in boxArray)
    {
        UIView *boxView = box[@"boxView"];
        
        if ([[box[@"id"] stringValue] isEqualToString:button.stringTag])
        {
            boxSelected = box;
            
            [UIView animateWithDuration:0.2 animations:^{
                
                self.viewBox.center = boxView.center;
                //set to position of viewBox
                [self setButtonInfo2Position];
            }];
            
            self.buttonInfo2.alpha = 0;
            self.buttonInfo2.stringTag = [box[@"id"] stringValue];
            [self.buttonInfo2 setTitle:box[[@"title" languageField]] forState:UIControlStateNormal];
            
            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.buttonInfo2.alpha = 1.0;
                boxView.alpha = 1.0;
            } completion:^(BOOL finished) {
                self.buttonInfo2.userInteractionEnabled = YES;
            }];
        }
        else
        {
            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                boxView.alpha = 0.6;
            } completion:nil];
        }
    }
    
    [self.scrollView zoomToPoint:button.superview.center withScale:self.scrollView.zoomScale animated:YES];
}

- (IBAction)buttonInfoPressed:(UIButton *)button
{
    [UIView animateWithDuration:0.4
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.scrollView zoomToPoint:self.viewBox.center withScale:3.0 animated:NO];
                     }
                     completion:^(BOOL finished) {
                         
                         
                         CATransition *transition = [CATransition animation];
                         transition.duration = 0.5;
                         transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                         transition.type = kCATransitionReveal;
                         transition.subtype = kCATransitionFromTop;
                         transition.delegate = self;
                         [self.navigationController.view.layer addAnimation:transition forKey:nil];
                         
                         
                         
                         SimulatorPortraitViewController *vc = [[Global storyboard] instantiateViewControllerWithIdentifier:@"SimulatorPortraitViewController"];
                         vc.sim_id = [button.stringTag intValue];
                         vc.title = boxSelected[[@"title" languageField]];
                         
                         
                         //2015.10.12
                         //Cancel this effect cuz iOS9 frame size issue
                         //
                         //zoom effect
//                         [[RZTransitionsManager shared] setAnimationController:[[RZZoomPushAnimationController alloc] init]
//                                                            fromViewController:[self class]
//                                                              toViewController:[SimulatorPortraitViewController class]
//                                                                     forAction:RZTransitionAction_PushPop];
//                         [self.navigationController setDelegate:[RZTransitionsManager shared]];
                         
                         
                         [self.navigationController pushViewController:vc animated:YES];
                         
                     }];
}

#pragma mark - ZOOM

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.viewZoom;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    NSLog(@"scale = %f", scale);
    self.viewZoom.contentScaleFactor = scale;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    for (UIView *v in zoomViewArray)
    {
        CGPoint center = v.center;
        v.transform = CGAffineTransformMakeScale(1.0/scrollView.zoomScale, 1.0/scrollView.zoomScale);
        v.center = center;
        
        // Set to position of viewBox when zoom
        if (v == self.buttonInfo2)
        {
            [self setButtonInfo2Position];
        }
    }
}

- (void)doubleTapRecognized:(UITapGestureRecognizer*)recognizer
{
    if (self.scrollView.zoomScale == 2.0)
    {
        [self.scrollView zoomToPoint:CGPointZero withScale:1.0 animated:YES];
    }
    else
    {
        CGPoint point = [recognizer locationInView:self.viewZoom];
        [self.scrollView zoomToPoint:point withScale:2.0 animated:YES];
    }
}

- (void)setButtonInfo2Position
{
    CGPoint pointInSuperView = self.buttonInfo.center; // CGPointMake(60, 20);
    CGPoint pointInSuperView2 = [self.viewZoom convertPoint:pointInSuperView fromView:self.viewBox];
    self.buttonInfo2.center = pointInSuperView2;
    [self.viewZoom bringSubviewToFront:self.buttonInfo2];
}

#pragma mark - 

- (void)singleTapRecognized:(UITapGestureRecognizer*)recognizer
{
    for (NSMutableDictionary *box in boxArray)
    {
        UIView *boxView = box[@"boxView"];
        
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            boxView.alpha = 0.6;
        } completion:^(BOOL finished) {
        }];
    }
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.buttonInfo2.alpha = 0;
    } completion:^(BOOL finished) {
        self.buttonInfo2.userInteractionEnabled = NO;
    }];
}

@end
