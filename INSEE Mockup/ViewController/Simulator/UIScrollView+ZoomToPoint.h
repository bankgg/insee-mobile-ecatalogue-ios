//
//  UIScrollView+ZoomToPoint.h
//  INSEE
//
//  Created by Tanut Chantarajiraporn on 5/22/2558 BE.
//  Copyright (c) 2558 iAppGarage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (ZoomToPoint)

- (void)zoomToPoint:(CGPoint)zoomPoint withScale: (CGFloat)scale animated: (BOOL)animated;

@end
